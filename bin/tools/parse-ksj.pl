use strict;
use warnings;
use Web::DOM::Document;
use Web::XML::Parser;
use Encode;

my $file_name = shift;
my $charset = 'utf-8';
my $doc = new Web::DOM::Document;

{
  open my $file, '<', $file_name or die "$0: $file_name: $1";
  local $/ = undef;
  my $parser = Web::XML::Parser->new;
  $parser->parse_char_string ((decode $charset, scalar <$file>) => $doc);
}

sub GML () { q<http://www.opengis.net/gml/3.2> }
sub KSJ () { q<http://nlftp.mlit.go.jp/ksj/schemas/ksj-app> }
sub XLINK () { q<http://www.w3.org/1999/xlink> }

my $de = $doc->document_element;

my $Points = {};
my $Data = {};

for my $el (@{$de->children}) {
  if ($el->manakai_element_type_match (GML, 'Point')) {
    $Points->{$el->get_attribute_ns (GML, 'id')} = [grep { length } split /\s+/, $el->text_content];
  } elsif ($el->namespace_uri eq KSJ) {
    my $data = $Data->{$el->get_attribute_ns (GML, 'id')} = {};
    $data->{_type} = $el->local_name;
    for my $cel (@{$el->children}) {
      if ($cel->namespace_uri eq KSJ) {
        my $ln = $cel->local_name;
        if ($ln eq 'position') {
          my $point = $cel->get_attribute_ns (XLINK, 'href');
          $point =~ s/^\#//;
          $data->{_lat} = $Points->{$point}->[0];
          $data->{_lon} = $Points->{$point}->[1];
        } elsif ($ln eq 'yearOfCompletion') {
          $data->{$ln} = $cel->first_element_child->first_element_child->text_content;
          $data->{$ln} =~ s/-04-01$//;
        } else {
          $data->{$ln} = $cel->text_content;
        }
      }
    }
  }
}

use Data::Dumper;
print Dumper $Data;
