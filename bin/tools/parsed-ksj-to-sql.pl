use strict;
use warnings;
use Encode qw(encode);
use JSON::Functions::XS qw(perl2json_bytes);
use Char::Normalize::FullwidthHalfwidth qw(normalize_width);

my $file_name = shift;
my $Data = do $file_name;

my $ObjectTypeID = {
  Dam => 1,
};
my $IDKey = {
  Dam => 'damCode',
};
my $NameKey = {
  Dam => 'damName',
};

sub _quote ($) {
  my $s = shift;
  $s =~ s/([\\'])/\\$1/g;
  return "'" . $s . "'";
}

sub _string ($) {
  return encode 'utf-8', $_[0];
}

sub _json ($) {
  return perl2json_bytes $_[0];
}

my @data;
my $time = time;
for (sort { $a cmp $b } keys %$Data) {
  my $data = $Data->{$_};
  normalize_width \($data->{$_})
      for qw(damName riverName waterSystemName address);
  my $json = {%$data};
  delete $json->{$_} for qw(_type _lat _lon);
  push @data, sprintf "(%s, %s, %s, %s, %s, %s, %s)",
      $ObjectTypeID->{$data->{_type}},
      $data->{$IDKey->{$data->{_type}}},
      $data->{_lat},
      $data->{_lon},
      _quote _string $data->{$NameKey->{$data->{_type}}},
      _quote _json $json,
      time;
}

while (@data) {
  my @d = splice @data, 0, 1000;
  print 'INSERT INTO external_data (type, code, lat, lon, name, data, created) VALUES '."\n";
  print join ",\n", @d;
  print ";\n";
}

