#!/usr/bin/perl
use strict;
use warnings;
use World::MySQL;
use World::Web;
use Karasuma::Config::JSON;
use Path::Class;

$SIG{PIPE} = 'IGNORE';

my $config = Karasuma::Config::JSON->new_from_env;
my $dbreg = World::MySQL->load_by_env;

return World::Web->psgi_app ($dbreg, $config);
