<!DOCTYPE html>
<html t:params="$loader $go_def $go $is_public">
<t:call x="use URL::PercentEncode">
<t:my as=$go_name x="$go->get_prop_value ('name')">
<t:include path=_macro.html.tm />
<t:include path=_wrapper.html.tm m:is_public=$is_public>
<t:field name=title><t:text value="$go_name">
<t:field name=body-attrs>
  <t:if x="$go->has_latlon">
    <t:attr name="'data-spot-lat'" value="$go->lat">
    <t:attr name="'data-spot-lon'" value="$go->lon">
  </t:if>
  <t:attr name="'data-spot-name'" value="$go_name">
<t:field name=edit>
<t:field>

<hgroup>
  <t:my as=$go_en_name x="$go->get_prop_value ('en_name')">
  <t:my as=$go_kana_name x="$go->get_prop_value ('kana_name')">
  <h1>
    <t:if x="defined $go_en_name and not $go_name eq $go_en_name">
      <ruby><t:text value="$go_name"><rt><t:if x="defined $go_kana_name and not $go_kana_name eq $go_name"><t:text value=$go_kana_name></t:if><rt><t:text value="$go_en_name"></ruby>
    <t:else>
      <t:text value="$go_name">
    </t:if>
  </h1>
</hgroup>

<div class=content>
  <nav class=content-links>
    <span class=lsf-icon data-icon=memo><a pl:href="$go->sw_url">ノート</a></span>
  </nav>

  <div class=main>
    <nav class=breadcrumbs>
      <m:go-breadcrumbs m:go_def=$go_def m:go=$go m:go_loader=$loader />
    </nav>

    <section class="summary wp-abstract" hidden>
      <blockquote>
        <p>{abstract}
      </blockquote>
      <cite><a href=https://ja.wikipedia.org/wiki/>フリー百科事典 ウィキペディア日本語版</a></cite>
    </section>

    <section class=go-data>
      <m:go-data m:go_def=$go_def m:go=$go m:go_loader=$loader />
      <script>
        var wpA = document.querySelector ('.go-data a[href^="https://ja.wikipedia.org/wiki/"]');
        if (wpA) {
          var pageName = wpA.pathname.replace (/^\/wiki\//, '');
          var xhr = new XMLHttpRequest;
          xhr.open ('GET', '/wp-abstract/' + pageName, true);
          xhr.onreadystatechange = (function (pageName) { return function () {
            if (this.readyState === 4) {
              if (this.status === 200) {
                var abstract = document.querySelector ('.summary.wp-abstract');
                abstract.hidden = false;
                abstract.querySelector ('blockquote p').textContent = this.responseText;
                abstract.querySelector ('cite a[href]').href += pageName;
              }
            }
          } }) (pageName);
          xhr.send (null);
        }
      </script>
    </section>

    <div class=child-spots>
      <m:go-children m:go_def=$go_def m:go=$go m:go_loader=$loader />
    </div>
  </div><!-- main -->

  <div class=side>
    <ul class=additional-names>
      <m:go-names m:go_def=$go_def m:go=$go m:go_loader=$loader />
    </ul>

    <m:go-images m:go_def=$go_def m:go=$go m:go_loader=$loader />

    <div id=map class=map pl:data-map-zoom="$go_def->{map_zoom}"></div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmPxsvoS9L-Ixcwex0XjMReZCg7dE4kv4&sensor=false&libraries=places"></script>
    <script>
      createMapFromSpotList ('map', '[data-spot-lat][data-spot-lon]');
    </script>

    <m:ads-rectangle/>
  </div>
</div>

</t:include>
