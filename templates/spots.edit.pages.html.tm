<!DOCTYPE html>
<html t:params="$spot $loader">
<t:include path=_macro.html.tm />
<t:include path=_wrapper.html.tm>
<t:field name=title>編集 - <t:text value="$spot->name">
<t:field name=edit>
  <a pl:href="$spot->path">スポット</a>
  <a pl:href="$spot->edit_path">編集</a>
<t:field>

<hgroup>
<h1><t:text value="$spot->name"> - 編集</h1>
</hgroup>

<nav class=breadcrumbs>
  <p><a pl:href="$spot->path"><t:text value="$spot->name"></a> > 編集
  <p><a href=/edit>編集</a> >> <t:text value="$spot->name">
</nav>

<nav class=tabs>
  <a pl:href="$spot->edit_path">基本</a>
  <a pl:href="$spot->edit_pages_path" class=selected>ページ</a>
</nav>

<section>
<h2>Wikipedia</h2>

<form pl:action="$spot->edit_wikipedia_ja_path" method=post>
  <p><input name=name pl:value="$spot->wikipedia_ja_page">
    <button type=button onclick="
      this.form.elements.name.value = this.getAttribute('data-spot-name');
    " pl:data-spot-name="$spot->name">スポット名と同じ</button>
  <p><button type=submit>Update</button>
  <p><button type=button onclick="
    var name = this.form.elements.name.value;
    if (!name) return;
    var form = document.createElement ('form');
    form.action = '/pages/add';
    form.method = 'POST';
    form.target = '_blank';
    form.innerHTML = '<input type=hidden name=wikipedia_page>';
    form.elements.wikipedia_page.value = name;
    form.submit ();
  ">Wikipedia 再取得</button>
  <button type=button onclick="
    var name = this.form.elements.name.value;
    if (!name) return;
    name = name.replace (/ /g, '_');
    var form = document.createElement ('form');
    form.action = 'https://ja.wikipedia.org/wiki/' + encodeURIComponent (name);
    form.target = '_blank';
    form.submit ();
  ">Wikipedia で開く</button>
</form>
</section>

<section>
  <h1>国土数値情報</h1>
  <form pl:action="$spot->edit_ksj_path" method=post id=ksj-form>
    <input type=hidden name=ksj_type pl:value="$spot->ksj_type">
    <input type=hidden name=ksj_code pl:value="$spot->ksj_code">
    <p><input type=search name=ksj_search oninput=" updateKSJUpdate () " data-url="/pages/ksj">
      <button type=button onclick="
        this.form.elements.ksj_search.value = this.getAttribute('data-spot-name');
        updateKSJUpdate ();
      " pl:data-spot-name="my $name = $spot->name;
                           $name =~ s/ダム$//;
                           $name ">スポット名と同じ</button>
    <p>
      <select multiple name=ksj_select onchange="
        var value = this.value.split (/,/);
        this.form.ksj_type.value = value[0];
        this.form.ksj_code.value = value[1];
      ">
        <t:if x="$spot->ksj_item">
          <option value="0,0">(なし)</option>
          <t:my as=$item x="$spot->ksj_item">
          <optgroup label="現在">
            <option pl:value="$item->type . ',' . $item->code">
              <t:text value="$item->name">
        <t:else>
          <option value="0,0" selected>(なし)
        </t:if>
        <optgroup label="検索結果">
      </select>
    <p><button type=submit>変更</button>
    <script>
      var updateKSJTimer;
      function updateKSJUpdate () {
        clearTimeout (updateKSJTimer);
        updateKSJTimer = setTimeout (function () {
          var form = document.forms['ksj-form'];
          var q = form.ksj_search.value;
          if (!q) return;
          var xhr = new XMLHttpRequest;
          xhr.open ('GET', form.ksj_search.getAttribute ('data-url') + '?q=' + encodeURIComponent (form.ksj_search.value), true);
          xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
              if (xhr.status < 400) {
                var json = JSON.parse (xhr.responseText).items;
                var optgroup = form.ksj_select.lastChild;
                optgroup.textContent = '';
                for (var i = 0; i < json.length; i++) {
                  var data = json[i];
                  var option = document.createElement ('option');
                  option.textContent = data.name;
                  option.value = data.type + ',' + data.code;
                  optgroup.appendChild (option);
                }
              }
            }
          };
          xhr.send (null);
        }, 500);
      }
    </script>
  </form>
</section>

<section>
  <h1>所属ページ</h1>

  <table>
    <tbody>
      <t:for as=$page x="$loader->spot_pages">
        <tr>
          <th>
            <a pl:href="$page->edit_path" pl:title="substr $page->summary // '', 0, 100"><t:text value="$page->any_title"></a>
          <td>
            <m:edit_spot_page_weight m:spot=$spot m:page=$page
                m:weight="$loader->get_spot_page_weight ($page)"/>
      </t:for>
  </table>
</section>

</t:include>
