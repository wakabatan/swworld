<!DOCTYPE html>
<html t:params="$loader" class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title>クロール設定
<t:field>

<h1>クロール設定</h1>

<nav class=breadcrumbs>
  <p><a href=/edit>編集</a> > クロール設定
</nav>

<form action=/pages/crawling/admin method=post>
  <dl>
    <dt>禁止 URL パターン
    <dd><textarea name=disallow_patterns t:parse><t:text value="$loader->disallow_patterns"></textarea>
  </dl>

  <p><button type=submit>保存</button>
</form>
