<!DOCTYPE html>
<html class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title>
  編集
<t:field>

<hgroup>
<h1>編集</h1>
</hgroup>

<div>
  <p id=current-edit-mode>
    <span class=edit-mode>編集モード</span>
    <span class=non-edit-mode>非・編集モード</span>

  <p>
    <button type=button onclick="
      document.cookie = 'edit=1';
      location.reload ();
    ">編集モード</button>
    <button type=button onclick="
      document.cookie = 'edit=0';
      location.reload ();
    ">非・編集モード</button>
</div>

<ul>
  <li><a href="/pages/add">頁の追加</a>
  <li><a href="/spots/create">スポットの追加</a>
  <li><a href="/pages/crawling">クロール予定</a>
  <li><a href="/pages/crawling/admin">クロール設定</a>
  <li><a href="/words/dict">辞書</a>
</ul>
