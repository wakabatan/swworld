<!DOCTYPE html>
<html t:params="$loader" class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title>クロール予定
<t:field>

<h1>クロール予定</h1>

<nav class=breadcrumbs>
  <p><a href=/edit>編集</a> > クロール予定
</nav>

<table>
  <thead>
    <tr>
      <th><code>ctid</code>
      <th>URL
      <th>Next
  <tbody>
    <tr>
      <th>
      <td>
        <form method=post action=/pages/crawling>
          <input type=hidden name=action value=add>
          <p>
            <input type=url name=url>
            <button type=submit>Add</button>
        </form>
      <td>
  <tbody>
    <t:for as=$target x="$loader->crawling_targets">
      <tr>
        <th>
          <p><code><t:text value="$target->ctid"></code>
          <p><t:text value="$target->status">
        <td>
          <p><code class=url>&lt;<a pl:href="$target->url" rel=noreferrer target=_blank><t:text value="$target->url"></a>></code></p>
          <t:my as=$page x="$loader->get_page_by_url ($target->url)">
          <t:if x=$page>
            <p>[<a pl:href="$page->path">Page</a>]
          </t:if>
        <td>
          <t:text value="scalar localtime $target->crawl_after">
    </t:for>
</table>

<form action=/pages/crawling/jobs method=post>
  <button type=submit>Run next job</button>
</form>
