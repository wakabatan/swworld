<!DOCTYPE html>
<html class=edit t:params=$loader>
<t:include path=_wrapper.html.tm>
<t:field name=title>辞書
<t:field>

  <h1>辞書</h1>

  <nav class=breadcrumbs>
    <p><a href=/edit>編集</a> > 辞書
  </nav>

  <form action=/words/dict method=get>
    <p>
      <input type=search name=q pl:value="$loader->search_word">
      <button type=submit>検索</button>
  </form>

  <form action=/words/dict method=post>
    <input type=hidden name=action value=rebuild>
    <button type=submit>辞書再構築</button>
  </form>

  <script>
    function startEdit (self) {
      var tr = self.form.parentNode.parentNode;
      var formId = self.form.id;
      var cells = tr.cells;
      for (var i = 0; i < cells.length; i++) {
        var td = cells[i];
        var name = td.getAttribute ('data-name');
        if (!name) continue;
        if (name === 'type') {
          var input = document.querySelector ('tfoot select').cloneNode (true);
          input.name = name;
          input.value = td.getAttribute ('data-value');
        } else {
          var input = document.createElement ('input');
          input.name = name;
          input.value = td.textContent;
          if (name === 'text') input.required = true;
          if (name === 'reading' || name === 'canon_reading') {
            input.pattern = document.querySelector ('input[name=reading][pattern]').pattern;
          }
        }
        input.setAttribute ('form', formId);
        td.textContent = '';
        td.appendChild (input);
      }
      self.type = 'submit';
      self.textContent = self.getAttribute ('data-submit-label');
      self.onclick = null;
      return false;
    } // startEdit
  </script>

  <table>
    <thead>
      <tr>
        <th>語句
        <th>読み
        <th>正規形
        <th>発音
        <th>種別
        <th>

    <tbody>
      <t:for as=$dw x="$loader->dict_words">
        <tr>
          <td data-name=text><t:text value="$dw->text">
          <td data-name=reading><t:text value="$dw->reading">
          <td data-name=canon_text><t:text value="$dw->canon_text">
          <td data-name=canon_reading><t:text value="$dw->canon_reading">
          <td data-name=type pl:data-value="$dw->type">
            <t:text value="$dw->type_as_label">
          <td>
            <form pl:id="'dw-' . $dw->dwid" pl:action="$dw->edit_path" method=post>
              <button type=button onclick=" return startEdit (this) " data-submit-label=変更>編集</button>
            </form>
      </t:for>
    
    <tfoot>
      <tr>
        <td><input name=text placeholder=語句 form=add-word required pl:value="$loader->search_word">
        <td><input name=reading placeholder=読み form=add-word
                pattern="[アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヰヱヲンガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォッャュョー]*"
                title="カタカナ">
        <td><input name=canon_text placeholder=正規形 form=add-word>
        <td><input name=canon_reading placeholder=発音 form=add-word
                pattern="[アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヰヱヲンガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォッャュョー]*"
                title="カタカナ">
        <td>
          <select name=type form=add-word>
            <t:for as=$t x="$loader->avail_word_types">
              <option pl:value="$t->{value}"><t:text value="$t->{label}">
            </t:for>
          </select>
        <td>
          <form action=/words/dict method=post id=add-word>
            <input type=hidden name=action value=add>
            <button type=submit>追加</button>
          </form>
  </table>