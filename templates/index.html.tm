<!DOCTYPE html>
<html t:params="$loader $is_public?">
<head>
<t:include path=_macro.html.tm />
<meta name="google-site-verification" content="tE5pbEtqbJu0UKbNCIsW2gUzW5bWGhvCwpwynqEIBRs">
<t:include path=_wrapper.html.tm m:is_public=$is_public>

<h1>最近の更新</h1>

<div class=content>
  <div class="main spot-list">
    <t:for as=$spot x="$loader->spots->grep (sub { $_->has_wikipedia and length $_->wikipedia_summary and $_->wikipedia_image_thumbnail_url })->slice (0, 2)">
      <article class=spot>
        <a pl:href="$spot->path">
          <t:if x="defined $spot->reading">
            <h1 draggable=true>
              <ruby>
                <t:text value="$spot->name">
              <rt>
                <t:text value="$spot->reading">
              </ruby>
            </h1>
          <t:else>
            <h1 draggable=true><t:text value="$spot->name"></h1>
          </t:if>

          <t:if x="$spot->wikipedia_image_thumbnail_url">
            <figure>
              <img pl:src="$spot->wikipedia_image_thumbnail_url">
            </figure>
          </t:if>

          <t:if x="$spot->has_wikipedia and length $spot->wikipedia_summary">
            <blockquote class=summary title="出典: フリー百科事典 ウィキペディア日本語版">
              <p><t:text value="$spot->wikipedia_summary"></p>
            </blockquote>
          </t:if>
          <footer/>
        </a>
      </article>
    </t:for>

    <ul class=additional-list>
      <t:for as=$spot x="$loader->spots">
        <li class=spot><a pl:href="$spot->path">
          <t:if x="defined $spot->reading">
            <ruby>
              <t:text value="$spot->name">
            <rt>
              <t:text value="$spot->reading">
            </ruby>
          <t:else>
            <t:text value="$spot->name">
          </t:if>
        </a>
      </t:for>
    </ul>
  </div>

  <div class=side>
    <m:ads-rectangle/>

    <form action="/spots/search" class=search>
      <p><input type=search placeholder=スポットを探す name=q oninput="
        var form = this.form;
        clearTimeout (form.searchTimer);
        form.searchTimer = setTimeout (function () {
          var xhr = new XMLHttpRequest;
          xhr.open ('GET', '/spots/search.json?q=' + encodeURIComponent (form.q.value), true);
          xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
              if (xhr.status < 400) {
                var json = JSON.parse (xhr.responseText);
                var list = form.getElementsByTagName ('ul')[0];
                list.innerHTML = '';
                for (var i = 0; i < json.items.length; i++) {
                  var item = json.items[i];
                  var li = document.createElement ('li');
                  li.innerHTML = '<a></a>';
                  li.firstChild.href = '/spots/' + item.spot_id;
                  li.firstChild.textContent = item.name;
                  list.appendChild (li);
                }
              }
            }
          };
          xhr.send (null);
        }, 100);
      "><button type=submit data-icon=search class=lsf-icon>検索</button>
      <ul>
      </ul>
    </form>
  </div>
</div>

</t:include>
