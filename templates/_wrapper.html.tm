<!DOCTYPE html>
<html t:params=$is_public?>

<t:if x="not $is_public">
  <meta name=referrer content=origin><!-- Chrome sends |Origin: null| if |never|... -->
  <meta name=robots content="noindex,nofollow">
  <meta name=viewport content="width=device-width,initial-scale=1">
</t:if>

<title t:parse><t:content name=title> - SuikaWiki World</title>

<link rel=stylesheet href=/css/common.css?7>
<style id=edit-mode>
  .edit-mode {
    display: none ! important;
  }
</style>
<script src="/js/world.js?4"></script>

<link rel=license href="https://suika.suikawiki.org/c/sw/swl" title="SuikaWiki License (GFDL / CC-BY-SA)">

<body>
<t:content name=body-attrs>

<header id=site-header>
  <span><a href="/">World</a>.</span>
  <a href="https://suikawiki.org/"><img src="https://suika.suikawiki.org/admin/logo/suika.png" alt=SuikaWiki.org></a>
</header>

<t:content>

<nav class="edit edit-mode">
  <t:content name=edit>
</nav>

<footer>
  <a href="/" rel=top>World.SuikaWiki.org</a>
  <a href="https://suika.suikawiki.org/c/sw/swl" rel=license>&copy;</a>
  <a href="/about">このサイトについて</a>
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-39820773-2', 'suikawiki.org');
  ga('send', 'pageview');
</script>
