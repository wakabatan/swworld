<!DOCTYPE html>
<html t:params="$spot $loader $go_loader" class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title>編集 - <t:text value="$spot->name">
<t:field name=edit>
  <a pl:href="$spot->path">スポット</a>
  <a pl:href="$spot->edit_path">編集</a>
<t:field>

<hgroup>
<h1><t:text value="$spot->name"> - 編集</h1>
</hgroup>

<nav class=breadcrumbs>
  <p><a pl:href="$spot->path"><t:text value="$spot->name"></a> > 編集
  <p><a href=/edit>編集</a> >> <t:text value="$spot->name">
</nav>

<nav class=tabs>
  <a pl:href="$spot->edit_path" class=selected>基本</a>
  <a pl:href="$spot->edit_pages_path">ページ</a>
</nav>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmPxsvoS9L-Ixcwex0XjMReZCg7dE4kv4&sensor=false&libraries=places"></script>

<t:wait cv="$loader->load_linked_go_data_as_cv">
<t:my as=$go_links x="$spot->go_links">
<t:for as=$go_type x="[keys %$go_links]">
  <t:wait cv="$go_loader->load_by_type_as_cv ($go_type)">
</t:for>

<section>
<h2>Basic data</h2>

<form method=post>
  <dl>
    <dt>Name
    <dd><input type=text name=name pl:value="$spot->name" oninput=" updateEditMap () ">

    <dt>読み
    <dd><input type=text name=reading pl:value="$spot->reading // ''">

    <dt>ローマ字表記
    <dd><input type=text name=roman pl:value="$spot->roman // ''">

    <dt>Location
    <dd><input type=text name=location pl:value="$spot->location">

    <dt>Map
    <dd>
      <div id=map-edit style="width: 60%; height: 20em"></div>
      <input type=search name=mapsearch>
    <dd>
      <input type=number name=lat step=any pl:value="$spot->lat" oninput=" updateEditMap () ">
      <input type=number name=lon step=any pl:value="$spot->lon" oninput=" updateEditMap () ">
      <t:if x="$spot->ksj_item">
        <button type=button onclick="
          this.form.lat.value = this.getAttribute ('data-lat');
          this.form.lon.value = this.getAttribute ('data-lon');
          updateEditMap ();
        " pl:data-lat="$spot->ksj_item->lat" pl:data-lon="$spot->ksj_item->lon">
          国土数値情報から
        </button>
      </t:if>
      <t:if x="$spot->has_wikipedia_latlon">
        <button type=button onclick="
          this.form.lat.value = this.getAttribute ('data-lat');
          this.form.lon.value = this.getAttribute ('data-lon');
          updateEditMap ();
        " pl:data-lat="$spot->wikipedia_lat" pl:data-lon="$spot->wikipedia_lon">
          Wikipedia から
        </button>
      </t:if>
      <t:for as=$go_type x="[keys %$go_links]">
        <t:my as=$go_def x="$go_loader->get_go_def_by_type ($go_type)">
        <t:my as=$go_id x="$go_links->{$go_type}">
        <t:my as=$go x="$go_loader->get_go_by_type_and_id ($go_type, $go_id)">
        <t:if x="$go->has_latlon">
          <button type=button onclick="
            this.form.lat.value = this.getAttribute ('data-lat');
            this.form.lon.value = this.getAttribute ('data-lon');
            updateEditMap ();
          " pl:data-lat="$go->lat" pl:data-lon="$go->lon">
            <t:text value="$go_def->{name}">から
          </button>
        </t:if>
      </t:for>
      <script>
        var form = document.forms[0];
        var lat = form.lat.value;
        var lon = form.lon.value;
        var pos = new google.maps.LatLng (lat, lon);
        var map = new google.maps.Map
            (document.getElementById ("map-edit"), {
               center: pos,
               zoom: 10,
               mapTypeId: google.maps.MapTypeId.HYBRID
             });
        var marker = new google.maps.Marker ({
          position: pos,
          map: map,
          title: form.name.value,
          draggable: true
        });
        google.maps.event.addListener (marker, 'position_changed', function () {
          var pos = marker.getPosition ();
          form.lat.value = pos.lat ();
          form.lon.value = pos.lng ();
        });

        var search = new google.maps.places.PlacesService (map);
        var ac = new google.maps.places.Autocomplete (form.mapsearch, {});
        ac.bindTo ('bounds', map);
        /*
        form.mapsearch.onchange = function () {
          search.textSearch ({
            query: this.value,
            location: map.getCenter (),
            radius: 50000
          }, function (results, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK &&
                results.length > 0) {
              var place = results[0];
              map.setCenter (place.geometry.location);
            }
          });
        };
        */
        google.maps.event.addListener (ac, 'place_changed', function () {
          var place = ac.getPlace ();
          map.setCenter (place.geometry.location);
        });

        google.maps.event.addListener (map, 'click', function (ev) {
          marker.setPosition (ev.latLng);
        });

        function updateEditMap () {
          var lat = form.lat.value;
          var lon = form.lon.value;
          var pos = new google.maps.LatLng (lat, lon);
          marker.setTitle (form.name.value);
          marker.setPosition (pos);
          map.setCenter (pos);
        }
      </script>

    <dt>Description
    <dd><textarea name=desc t:parse><t:text value="$spot->desc"></textarea>
  </dl>
  <p><button type=submit>Update</button>
</form>
</section>

<section>
<h2>他の名前</h2>

  <table>
    <thead>
      <tr>
        <th>名前
        <th>読み
        <th>ローマ字表記
        <th>
    <tbody>
      <t:for as=$n x="$spot->additional_names">
        <tr>
          <td><t:text value="$n->{name}">
          <td><t:text value="$n->{reading} // ''">
          <td><t:text value="$n->{roman} // ''">
          <td>
            <form pl:action="$spot->edit_names_path" method=post
                onsubmit="return confirm (this.getAttribute ('data-confirm-text'))"
                data-confirm-text="本当に削除しますか?">
              <input type=hidden name=spot_name_sha1 pl:value="$n->{spot_name_sha1}">
              <input type=hidden name=action value=delete>
              <button type=submit>削除</button>
            </form>
      </t:for>
    <tfoot>
      <tr>
        <td><input name=name placeholder=名前 required form=edit-names>
        <td><input name=reading placeholder=読み form=edit-names>
        <td><input name=roman placeholder=ローマ字表記 form=edit-names>
        <td>
          <form pl:action="$spot->edit_names_path" method=post id=edit-names>
            <input type=hidden name=action value=add>
            <button type=submit>追加</button>
          </form>
  </table>
</section>

<section>
<h2>Asscoaited words</h2>

<form pl:action="$spot->edit_words_path" method=post>
  <textarea name=words t:parse t:space=trim>
    <t:for as=$word x="$spot->words">
      <t:text value="$word->text . qq{\n}">
    </t:for>
  </textarea>
  <p><button type=submit>Update</button>
</form>
</section>

<section>
  <h1>親スポット</h1>

  <ul>
    <t:for as=$sp x="$loader->parent_spots">
      <li><a pl:href="$sp->path"><t:text value="$sp->name"></a>
    </t:for>
    <li>
      <p><input id=parent-spots-input>
      <form pl:action="$spot->edit_parents_path" method=post>
        <p><select size=10 name=spot_id id=parent-spots-list></select>
        <div id=parent-spots-map class=map></div>
        <p><button type=submit>追加</button>
        <script>
          createSpotSelection ({input: '#parent-spots-input',
                                map: '#parent-spots-map',
                                select: '#parent-spots-list'});
        </script>
      </form>
  </ul>

</section>

<section id=gos>
  <h1>関連付け</h1>

  <table>
    <tbody>
      <t:for as=$go_type x="[keys %$go_links]">
        <tr>
          <t:my as=$go_def x="$go_loader->get_go_def_by_type ($go_type)">
          <t:my as=$go_id x="$go_links->{$go_type}">
          <t:my as=$go x="$go_loader->get_go_by_type_and_id ($go_type, $go_id)">
          <th><t:text value="$go_def->{name}">
          <td><t:text value="$go->get_prop_value ('name')">
      </t:for>
  </table>

  <form pl:action="$spot->edit_gos_path" method=post>
    <select name=go_type size=20 required></select>
    <input type=search name=go_q><button type=button pl:data-value="$spot->name" onclick="
      elements.go_q.value = this.getAttribute ('data-value');
      elements.go_q.oninput ();
    ">スポット名</button>
    <select name=go_id size=20 required>
      <option value="">(なし)
    </select>
    <button type=submit>保存</button>
    <script>
      (function () {
        var form = document.forms[document.forms.length-1];
        var selectGoType = form.elements.go_type;
        var selectGoID = form.elements.go_id;
        var inputQ = form.elements.go_q;

        getJSON ('/spots/gos.json').then (function (data) {
          data.gos.forEach (function (goType) {
            var option = document.createElement ('option');
            option.label = goType.name;
            option.value = goType.id;
            option.setAttribute ('data-search-json-path', goType.search_json_path);
            option.onclick = function () {
              selectGoID.setAttribute
                ('data-search-json-path', this.getAttribute ('data-search-json-path'));
              updateSelectGoID ();
            };
            selectGoType.appendChild (option);
          });
        });

        selectGoType.onchange = function () {
          var option = this.selectedOptions[0];
          if (option) option.onclick ();
        };
        inputQ.oninput = updateSelectGoID;

        function updateSelectGoID () {
          var url = selectGoID.getAttribute ('data-search-json-path');
          if (!url) return;
          url += '?q=' + encodeURIComponent (inputQ.value);
          getJSON (url).then (function (data) {
            var options = selectGoID.options;
            for (var i = options.length - 1; i > 0; i--) {
              options[i].parentNode.removeChild (options[i]);
            }
            data.items.forEach (function (item) {
              var option = document.createElement ('option');
              option.value = item.id;
              option.label = item.name;
              option.title = item.matched;
              selectGoID.appendChild (option);
            });
          });
        } // updateSelectGoID
      }) ();
    </script>
  </form>
</section>

</t:include>
