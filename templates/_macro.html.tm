<t:call x="use URL::PercentEncode">
<t:call x="use Encode q(encode)">
<t:call x="use Digest::MD5 qw(md5_hex)">
<t:macro name=wikipedia-image t:params="$file_name $alt?">
  <t:my as=$url x="
    $file_name =~ s/^[^:]+://;
    $file_name =~ s/ /_/g;
    my $hash = md5_hex encode 'utf-8', $file_name;
    sprintf q<https://upload.wikimedia.org/wikipedia/commons/%s/%s/%s>,
        (substr $hash, 0, 1),
        (substr $hash, 0, 2),
        percent_encode_c $file_name;
  ">
  <a pl:href=$url><img pl:src=$url pl:alt=$alt onerror="
    if (/\/commons\//.test (this.src)) {
      this.src = this.src.replace (/\/commons\//, '/ja/');
    }
  "></a>
</t:macro>

<t:macro name=timestamp-date t:params=$value>
  <time pl:datetime="my @t = gmtime $value;
                                  sprintf '%04d-%02d-%02dT%02d:%02d:%02dZ',
                                      $t[5]+1900, $t[4]+1, $t[3],
                                      $t[2], $t[1], $t[0]">
              <t:text value="my @t = localtime $value;
                             sprintf '%04d/%02d/%02d',
                                 $t[5]+1900, $t[4]+1, $t[3]">
  </time>
</t:macro>

<t:macro name=page-seq-pages t:params="$pages">
  <t:if x="@$pages > 1" t:space=preserve>
    <span class=seq-pages>(<t:for as=$p x="$pages" t:space=trim>
       <a pl:href="$p->[1]->url" pl:title="$p->[1]->any_title . qq{\n} . substr $p->[1]->summary // '', 0, 50" class=seq-page><t:text value="$p->[0]"></a>
       <t:sep t:space=preserve>, 
     </t:for>)</span>
  </t:if>
</t:macro>

<t:macro name=ads-rectangle>
  <aside class=ads-rectangle>
    <script>
      google_ad_client = "ca-pub-6943204637055835";
      google_ad_slot = "4060165115";
      google_ad_width = 300;
      google_ad_height = 250;
    </script>
    <script src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
  </aside>
</t:macro>

<t:macro name=edit_spot_page_weight t:params="$spot $page $weight">
  <form pl:action="$spot->edit_pages_path" method=post>
    <input type=hidden name=page_id pl:value="$page->page_id">
    <m:edit_spot_page_weight-select m:weight=$weight />
    <input type=submit value="変更">
  </form>
</t:macro>

<t:macro name=edit_spot_page_weight-select t:params="$weight">
  <select name=weight>
    <option value=-100 pl:selected="$weight == -100?'':undef">非表示
    <option value= -10 pl:selected="$weight ==  -10?'':undef">追加
    <option value=   0 pl:selected="$weight ==    0?'':undef">標準
    <option value= 100 pl:selected="$weight ==  100?'':undef">重要
  </select>
</t:macro>

<t:macro name=go-data t:params="$go_def $go $go_loader">
    <table class=props>
      <tbody>
        <t:for as=$prop x="$go_def->{main_props}">
          <t:my as=$value x="$go->get_prop_value ($prop->{key})">
          <t:if x="defined $value">
            <tr>
              <th><t:text value="$prop->{name}">
              <td><code><t:text value="$value"></code>
          </t:if>
        </t:for>
      </tbody>

      <t:if x="$go_def->{id} eq 'jp-regions'">
        <t:my as=$office x="$go->get_prop_value ('office')">
        <t:my as=$name_suffix x="substr $go->get_prop_value ('name'), -1">
        <t:if x="defined $office">
          <tbody>
            <t:my as=$label x="{
              'city' => '市役所',
              'ward' => '区役所',
              'town' => '町役場',
              'village' => '村役場',
            }->{$go->get_prop_value ('type')} // ($name_suffix . '庁')">
            <tr pl:data-spot-lat="($office->{position} || [])->[0]"
                pl:data-spot-lon="($office->{position} || [])->[1]"
                pl:data-spot-name=$label>
              <th><t:text value=$label>所在地
              <td><t:text value="$office->{address} // ''">
        </t:if>

        <t:my as=$symbols x="$go->get_prop_value ('symbols') || []">
        <t:if x="@$symbols">
          <tbody>
            <t:for as=$symbol x=$symbols>
              <t:if x="not ($symbol->{type} eq 'flag' or $symbol->{type} eq 'mark')">
                <tr>
                  <th><t:text value="$symbol->{label} // {
                    tree => $name_suffix . '木',
                    flower => $name_suffix . '花',
                    bird => $name_suffix . '鳥',
                    fish => $name_suffix . '魚',
                    song => $name_suffix . '歌',
                  }->{$symbol->{type}} // $symbol->{type}">
                  <td>
                    <t:if x="defined $symbol->{wref}">
                      <figure class=flag title=旗>
                        <m:wikipedia-image m:file_name="$symbol->{wref}" />
                        <t:if x="defined $symbol->{name}">
                          <figcaption><t:text value="$symbol->{name}"></>
                        </t:if>
                      </figure>
                    <t:else>
                      <t:text value="$symbol->{name}">
                    </t:if>
              </t:if>
            </t:for>
        </t:if>
      </t:if>
    </table>

    <ul class=links>
      <t:for as=$prop x="$go_def->{link_props} or []">
        <t:my as=$value x="$go->get_prop_value ($prop->{key})">
        <t:if x="defined $value">
          <li>
            <a>
              <t:if x="defined $prop->{wikipedia}">
                <t:attr name="'href'" value="'https://' . $prop->{wikipedia} . '.wikipedia.org/wiki/' . percent_encode_c $value">
              <t:else>
                <t:attr name="'href'" value="$value">
              </t:if>
              <t:text value="$prop->{name}">
            </a>
        </t:if>
      </t:for>
      <li class=source><a pl:href="'/about#'.$go_def->{id}">データ出典</a>
    </ul>
</t:macro>

<t:macro name=go-children t:params="$go_def $go $go_loader">
  <t:my as=$sub_defs x="{
    macroregions => [{key => 'subregions', go_type => 'macroregions', title => '地域'},
                     {key => 'countries', go_type => 'countries', title => '国'}],
    'jp-regions' => [{key => 'descendant_city_ids', go_type => 'jp-regions', title => '市'},
                     {key => 'descendant_district_ids', go_type => 'jp-regions', title => '郡'},
                     {key => 'descendant_ward_ids', go_type => 'jp-regions', title => '区'},
                     {key => 'descendant_town_ids', go_type => 'jp-regions', title => '町'},
                     {key => 'descendant_village_ids', go_type => 'jp-regions', title => '村'}],
  }->{$go_def->{id}}">
  <t:if x="defined $sub_defs">
    <t:for as=$sub_def x=$sub_defs>
      <t:my as=$list x="$go->get_prop_value ($sub_def->{key}) or {}">
      <t:if x="keys %$list">
        <t:wait cv="$go_loader->load_by_type_as_cv ($sub_def->{go_type})">
        <section>
          <h1><t:text value="$sub_def->{title}"></h1>
          <ul>
            <t:for as=$id x="[keys %$list]">
              <t:my as=$g x="$go_loader->get_go_by_type_and_id ($sub_def->{go_type}, $id)">
              <li>
                <t:if x="$g->has_latlon">
                  <t:attr name="'data-spot-lat'" value="$g->lat">
                  <t:attr name="'data-spot-lon'" value="$g->lon">
                </t:if>
                <a pl:href="$g->path"><t:text value="$g->get_prop_value ('name')"></a>
            </t:for>
          </ul>
        </section>
      </t:if>
    </t:for>
  </t:if>
</t:macro>

<t:macro name=go-breadcrumbs t:params="$go $go_def $go_loader">
  <p>
    <t:if x="$go_def->{id} eq 'countries' or
             $go_def->{id} eq 'macroregions'">
          <t:wait cv="$go_loader->load_by_type_as_cv ('macroregions')">
          <t:my as=$region x="$go_def->{id} eq 'countries' ? $go_loader->get_go_by_type_and_id ('macroregions', $go->get_prop_value ('submacroregion') || 1) : $go">
          <t:for as=$superregion x="
            my $sups = $region->get_prop_value ('superregions') || {};
            [map { $go_loader->get_go_by_type_and_id ('macroregions', $_) } sort { $sups->{$b} <=> $sups->{$a} } keys %$sups];
          ">
            <a pl:href="$superregion->path"><t:text value="$superregion->get_prop_value ('name')"></a>
            <t:text value="' > '">
          </t:for>
          <t:if x="$go_def->{id} eq 'countries'">
            <a pl:href="$region->path"><t:text value="$region->get_prop_value ('name')"></a>
            <t:text value="' > '">
          </t:if>
      <t:text value="$go->get_prop_value ('name')">
    <t:elsif x="$go_def->{id} eq 'jp-regions'">
      <a href="/spots/countries/57">日本</a> >
      <t:for as=$key x="[qw(pref_id city_id district_id)]">
        <t:my as=$id x="$go->get_prop_value ($key)">
        <t:my as=$a_go x="defined $id ? $go_loader->get_go_by_type_and_id ('jp-regions', $id) : undef">
        <t:if x="defined $a_go">
          <a pl:href="$a_go->path"><t:text value="$a_go->get_prop_value ('name')"></a>
          <t:text value="' > '">
        </t:if>
      </t:for>
      <t:text value="$go->get_prop_value ('name')">
    <t:else>
          <a pl:href="'/spots/' . percent_encode_c $go_def->{id}"><t:text value="$go_def->{name}"></a>
          <t:text value="' > '">
      <t:text value="$go->get_prop_value ('name')">
    </t:if>
</t:macro>

<t:macro name=go-names t:params="$go $go_def $go_loader">
  <t:my as=$go_name x="$go->get_prop_value ('name')">
  <t:my as=$go_kana_name x="$go->get_prop_value ('kana_name')">
  <t:my as=$go_en_name x="$go->get_prop_value ('en_name')">
  <li>
    <t:if x="defined $go_name and defined $go_en_name and
             not $go_name eq $go_en_name">
      <ruby>
        <t:text value="$go_name">
        <rt><t:if x="defined $go_kana_name and not $go_kana_name eq $go_name"><t:text value=$go_kana_name></t:if>
        <rt><t:text value="$go_en_name">
      </ruby>
    <t:else>
      <t:text value="$go_name">
    </t:if>
  </li>

    <t:my as=$go_short_name x="$go->get_prop_value ('short_name')">
    <t:my as=$go_kana_short_name x="$go->get_prop_value ('kana_short_name')">
    <t:my as=$go_en_short_name x="$go->get_prop_value ('en_short_name')">
    <t:if x="defined $go_short_name">
        <li>
          <t:if x="defined $go_short_name and defined $go_en_short_name and
                   not $go_short_name eq $go_en_short_name">
            <ruby>
              <t:text value="$go_short_name">
              <rt><t:if x="defined $go_kana_short_name and not $go_kana_short_name eq $go_short_name"><t:text value=$go_kana_short_name></t:if>
              <rt><t:text value="$go_en_short_name">
            </ruby>
          <t:else>
            <t:text value="$go_short_name">
          </t:if>
    </t:if>
</t:macro>

<t:macro name=go-images t:params="$go $go_def $go_loader">
    <t:my as=$flag_file_name x="$go->get_prop_value ('wikipedia_flag_file_name')">
    <t:if x="defined $flag_file_name">
      <figure class=flag title=旗>
        <m:wikipedia-image m:file_name=$flag_file_name />
      </figure>
    </t:if>

  <t:my as=$wp_image x="$go->get_prop_value ('wikipedia_image')">
  <t:if x="defined $wp_image and ref $wp_image eq 'HASH' and defined $wp_image->{wref}">
    <figure>
      <m:wikipedia-image m:file_name="$wp_image->{wref}" />
      <t:if x="defined $wp_image->{desc} and length $wp_image->{desc}">
        <figcaption>
          <t:text value="$wp_image->{desc}">
        </figcaption>
      </t:if>
    </figure>
  </t:if>
</t:macro>
