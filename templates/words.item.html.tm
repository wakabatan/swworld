<!DOCTYPE html>
<html t:params="$word $pages $spots" class=edit>
<t:call x="use URL::PercentEncode">
<t:include path=_wrapper.html.tm>
<t:field name=title><t:text value="$word->text">
<t:field>

<h1><t:text value="$word->text"></h1>

<section>
  <h1>Spots</h1>

  <ul>
    <t:for as=$spot x=$spots>
      <li><a pl:href="$spot->path"><t:text value="$spot->name"></a>
    </t:for>

    <li><a pl:href="'/spots/create?words=' . percent_encode_c $word->text">Add...</A>
  </ul>
</section>

<section>
  <h1>Pages</h1>

  <ul>
    <t:for as=$page x=$pages>
      <li>
        <a pl:href="'/pages/' . $page->page_id"><t:text value="$page->title // $page->url"></a>
        <code><t:text value="$page->url"></code>  
    </t:for>
  </ul>
</section>

</t:include>
