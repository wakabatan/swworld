<!DOCTYPE html>
<html t:params="$q? $spots? $is_public?">
<t:include path=_wrapper.html.tm m:is_public=$is_public>
<t:field name=title>
  <t:if x="defined $q and length $q">
    <t:text value="$q . ' - '">
  </t:if>
  検索
<t:field>

<h1>
  検索
  <t:if x="defined $q and length $q">
    <t:text value="' - '">
    <q><t:text value="$q"></q>
  </t:if>
</h1>

<div class=content>
<div class=main>

<form action=/spots/search method=get>
  <input type=search name=q pl:value="$q // ''">
  <input type=submit value=検索>
</form>

<t:if x="defined $q and length $q">
  <ul class=search-results>
    <t:for as=$spot x=$spots>
      <li>
        <t:if x="$spot->has_latlon">
          <t:attr name="'data-spot-lat'" value="$spot->lat">
          <t:attr name="'data-spot-lon'" value="$spot->lon">
        </t:if>
        <a pl:href="$spot->path"><t:text value="$spot->name"></a>
    </t:for>
  </ul>
</t:if>
</div>

<div class=side>
  <div id=map class=map></div>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmPxsvoS9L-Ixcwex0XjMReZCg7dE4kv4&sensor=false"></script>
  <script>
    createMapFromSpotList ('map', '.search-results [data-spot-lat][data-spot-lon]');
  </script>
</div>
</div>

</t:include>
