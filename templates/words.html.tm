<!DOCTYPE html>
<html t:params=$words class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title>語句
<t:field>

<h1>語句</h1>

<ul>
  <t:for as=$word x=$words>
    <li><a pl:href="'/words/' . $word->word_id"><t:text value="$word->text"></a>
  </t:for>
</ul>

</t:include>