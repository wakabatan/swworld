<!DOCTYPE html>
<html t:params="$page $loader" class=edit>
<t:include path=_macro.html.tm />
<t:include path=_wrapper.html.tm>
<t:field name=title>編集 - <t:text value="$page->any_title">
<t:field name=edit>
  <a pl:href="$page->path">ページ</a>
  <a pl:href="$page->edit_path">編集</a>
<t:field>

<hgroup>
  <h1><t:text value="$page->any_title"></h1>
  <h2 class=url><code class=url>&lt;<a pl:href="$page->url"><t:text value="$page->url"></a>></code></h2>
</hgroup>

<nav class=breadcrumbs>
  <p><a href=/edit>編集</a> >> <t:text value="$page->any_title">
</nav>

  <dl>

  <dt>URL
  <dd><code class=url>&lt;<a pl:href="$page->url"><t:text value="$page->url"></a>></code>
  <dd><form action=/pages/add method=post>
    <input type=hidden name=url pl:value="$page->url">
    <button type=submit>再取得</button>
  </form>
  <dd><form method=post action=/pages/crawling>
    <input type=hidden name=action value=add>
    <input type=hidden name=url pl:value="$page->url">
    <button type=submit>後でクロール</button>
  </form>


  <dt>状態
  <dd><code><t:text value="$page->status"></code></dd>

  <t:if x="$page->is_shown">

    <dt>タイトル
    <dd><cite><t:text value="$page->title"></cite></dd>

    <t:if x="defined $page->timestamp">
      <dt>日時
      <dd><t:text value="scalar localtime $page->timestamp">
    </t:if>

    <dt>抽出した語句
    <dd>
      <t:for as=$v x="[sort {
        $b->{count} <=> $a->{count};
      } @{$loader->page_words}]" t:space=preserve>
        <a pl:href="$v->{word}->path"><t:text value="$v->{word}->text"></a>
        (<t:text value="$v->{count}">)
      </t:for>

    <dt>媒体
    <dd class=lsf-icon data-icon=camera>写真 <t:text value="$page->photo_count">
    <dd class=lsf-icon data-icon=earth>地図 <t:text value="$page->map_count">
    <dd class=lsf-icon data-icon=video>動画 <t:text value="$page->video_count">

    <dt>要約
    <dd><blockquote><t:text value="$page->summary"></blockquote>

    <dt>サイト内リンク
    <dd>
      <ul>
        <t:my as=$urls x="$page->site_links || {}">
        <t:for as=$url x="[sort { $urls->{$b} <=> $urls->{$a} } keys %$urls]">
          <t:my as=$url_page x="$loader->get_page_by_url ($url)">
          <li>
            <t:if x="$url_page and defined $url_page->title">
              <a pl:href="$url_page->path"><cite><t:text value="$url_page->title"></cite></a>
            <t:else>
              <form method=post action=/pages/crawling>
                <input type=hidden name=action value=add>
                <input type=hidden name=url pl:value="$url">
                <button type=submit>後で</button>
              </form>
              <form action=/pages/add method=post>
                <input type=hidden name=url pl:value="$url">
                <button type=submit>取得</button>
              </form>
            </t:if>
            <code class=url>&lt;<a pl:href="$url" rel=noreferrer target=_blank><t:text value="$url"></a>></code>
            (<t:text value="$urls->{$url}">)
        </t:for>
      </ul>

    </t:if>
  </dl>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmPxsvoS9L-Ixcwex0XjMReZCg7dE4kv4&sensor=false&libraries=places"></script>

<t:if x="$page->is_shown">
  <section>
    <h1>スポットへの登録</h1>

    <p><input id=page-spots-input>
    <form data-action="/spots/{spot_id}/edit/pages" method=post onsubmit="
      if (!this.spot_id.value) return false;
      this.action = this.getAttribute ('data-action').replace
          (/\{spot_id\}/, this.spot_id.value);
    ">
      <p><select size=10 name=spot_id id=page-spots-list></select>
      <div id=page-spots-map class=map></div>
      <input type=hidden name=page_id pl:value="$page->page_id">
      <p><m:edit_spot_page_weight-select m:weight=0 />
      <p><button type=submit>追加</button>
    </form>
    <script>
      createSpotSelection ({input: '#page-spots-input',
                            map: '#page-spots-map',
                            select: '#page-spots-list'});
    </script>
  </section>
</t:if>
