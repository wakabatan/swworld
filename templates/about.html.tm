<!DOCTYPE html>
<t:include path=_wrapper.html.tm>
<t:field name=title>
  このサイトについて
<t:field>

<hgroup>
  <h1>このサイトについて</h1>
</hgroup>

<section class=text>
<p>このサイト『<a href="/">World.SuikaWiki.org</a>』はウィキサイト 
『<a href="https://suikawiki.org/">SuikaWiki</a>』の一部で、
世界中の名所旧跡、建造物、土木構造物、自然地形その他の情報を集めています。

<p>このウィキは編集者が手動で追加した情報だけでなく、
機械的に構成された内容も含んでいます。
このウィキは専用のシステムにより運用しています。

<p>このウィキを編集できるのは、編集権を持っている人だけです。
</section>

<section class=text id=license>
<h1>ライセンス</h1>

<p>このウィキの内容は、<a
href="https://suika.suikawiki.org/c/sw/swl">SuikaWiki License</a> 
に基づき利用することができます。
編集者は本ライセンスに基づき利用されることについて、予め同意することとします。
なお、このウィキの内容は外部のサイトの引用を含んでいたり、
Wikipedia 等の外部サイトの画像を埋め込んでいたりすることがあります。
本ライセンスはそうした内容自体に適用されるものではありません。

<p id=ksj>このウィキの内容の一部は、<a href="http://nlftp.mlit.go.jp/ksj/index.html">国土数値情報</a>を利用しています。

  <figure>
    <p><a href="http://nlftp.mlit.go.jp/ksj/gml/datalist/KsjTmplt-W01.html">国土数値情報 (ダムデータ) 国土交通省 平成17年度</a>
  </figure>

<p>このウィキの内容の一部は、 <a href="https://geocol.github.io/">Geocol data
files</a> を利用しています。その出典とライセンスは、次の各ドキュメントを参照してください。

  <ul>
    <li id=macroregions><a href="https://github.com/geocol/data-countries/blob/master/doc/macroregions.txt">macroregions</a>
    <li id=countries><a href="https://github.com/geocol/data-countries/blob/master/doc/countries.txt">countries</a>
    <li id=jp-regions><a href="https://github.com/geocol/data-jp-areas/blob/master/doc/jp-regions.txt">jp-regions</a>
  </ul>

</section>

<section class=text id=robots>
<h1>掲載拒否の表明</h1>

<p>このウィキへの掲載 (引用やリンク) は、当該 HTML 文書に <a
href="http://www.robotstxt.org/meta.html"><code>&lt;meta name=robots content=noindex></code></a> を含めることにより、または 
<code>User-Agent: world.suikawiki.org</code> について <a
href="http://www.robotstxt.org/robotstxt.html"><code>/robots.txt</code></a> 
に記述することにより拒否できます。

<p>また、このウィキはクリエイティブ・コモンズ・ライセンスにより公開されている画像を埋め込んでいることがあります。
このウィキへの掲載を希望されない場合は、画像共有サイト等の設定により適切なライセンスをご指定ください。

<p>既に掲載されているページや画像を削除したい場合は、
これらいずれかの方法で掲載の拒否を表明した上で、当該ページ・画像の URL
を <code>admin@suikawiki.org</code> までご連絡ください。
(URL が明記されてないと削除できないのでご了承ください。)

</section>

<section id=contact>
<h1>お問い合わせ</h1>

<p><a href=https://wiki.suikawiki.org/n/ContactPage>ContactPage</a>
をご覧ください。
</section>

