<!DOCTYPE html>
<html t:params="$url? $parsed? $words?" class=edit>
<t:call x="use URL::PercentEncode qw(percent_encode_c)">
<t:include path=_wrapper.html.tm>
<t:field name=title>
  頁の追加
<t:field>

<h1>頁の追加</h1>

<nav class=breadcrumbs>
  <p><a href=/edit>編集</a> > 頁の追加
</nav>

<section class=page-add>
<h1>頁の追加</h1>

<form method=post action=/pages/add>
  <dl>
    <dt>URL
    <dd><input type=url name=url pl:value=$url autofocus>
    <dt>Wikipedia page name
    <dd><input type=text name=wikipedia_page>
  </dl>

  <p><button type=submit>Add</button>
</form>
</section>
