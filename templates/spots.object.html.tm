<!DOCTYPE html>
<html t:params="$loader $go_def $is_public">
<t:call x="use URL::PercentEncode">
<t:include path=_macro.html.tm />
<t:include path=_wrapper.html.tm m:is_public=$is_public>
<t:field name=title><t:text value="$go_def->{name}">
<t:field name=body-attrs>
<t:field name=edit>
<t:field>

<hgroup>
  <h1><t:text value="$go_def->{name}"></h1>
</hgroup>

<div class=content>
  <nav class=content-links>
    <span class=lsf-icon data-icon=memo><a pl:href="'https://wiki.suikawiki.org/n/' . percent_encode_c $go_def->{name}">ノート</a></span>
  </nav>

  <div class=main>
    <ul>
      <t:for as=$id x="$loader->get_member_go_ids_by_type ($go_def->{id})">
        <t:my as=$go x="$loader->get_go_by_type_and_id ($go_def->{id}, $id)">
        <li><a pl:href="$go->path"><t:text value="$go->get_prop_value ('name')"></a>
      </t:for>
    </ul>
  </div><!-- main -->

  <div class=side>
    <m:ads-rectangle/>
  </div>
</div>

</t:include>
