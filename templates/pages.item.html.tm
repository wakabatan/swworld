<!DOCTYPE html>
<html t:params="$page" class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title><t:text value="$page->any_title">
<t:field name=edit>
  <a pl:href="$page->path">ページ</a>
  <a pl:href="$page->edit_path">編集</a>
<t:field>

  <hgroup>
    <h1><t:text value="$page->any_title"></h1>
    <h2 class=url><code class=url>&lt;<a pl:href="$page->url"><t:text value="$page->url"></a>></code></h2>
  </hgroup>

  <t:if x="defined $page->summary">
    <section class=summary>
      <blockquote>
        <p><t:text value="$page->summary">
      </blockquote>
      <p class=more><a pl:href="$page->url">続きを読む</a>
    </section>
  </t:if>