<!DOCTYPE html>
<html t:params=$app class=edit>
<t:include path=_wrapper.html.tm>
<t:field name=title>スポットの追加
<t:field>

<h1>スポットの追加</h1>

<nav class=breadcrumbs>
  <p><a href=/edit>編集</a> > スポットの追加
</nav>

<form method=post>
  <dl>
    <dt>Name
    <dd><input type=text name=name pl:value="$app->text_param ('words')">

    <dt>Words
    <dd><textarea name=words t:parse><t:text value="$app->text_param ('words')"></textarea>
  </dl>
  <p><button type=submit>Create</button>
</form>

</t:include>
