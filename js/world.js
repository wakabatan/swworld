(function () {
  var m = document.cookie.match (/(?:^|; )edit=([^;]+)/);
  if (m && parseInt (m[1])) {
    var style = document.getElementById ('edit-mode');
    style.textContent = '.edit-mode { display: block } span.edit-mode { display: inline } .non-edit-mode { display: none }';
  }
}) ();

var PromiseModoki = function () {
};

PromiseModoki.prototype.then = function (onsuccess) {
  if (this.done) {
    var data = this.data;
    delete this.data;
    onsuccess (data);
  } else {
    this.ondone = onsuccess;
  }
};

PromiseModoki.prototype.success = function (data) {
  this.done = true;
  if (this.ondone) {
    var done = this.ondone;
    delete this.ondone;
    done (data);
  } else {
    this.data = data;
  }
};

function getJSON (url) {
  var promise = new PromiseModoki;
  var xhr = new XMLHttpRequest;
  xhr.open ('GET', url, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status < 400) {
        promise.success (JSON.parse (xhr.responseText));
      } else {
        // XXX
      }
    }
  };
  xhr.send (null);
  return promise;
} // getJSON

function createMapFromSpotList (mapId, selectors, onselect) {
  var spots = document.querySelectorAll (selectors);
  if (spots.length) {
    var pos = new google.maps.LatLng
        (spots[0].getAttribute ('data-spot-lat'),
         spots[0].getAttribute ('data-spot-lon'));
    var mapEl = mapId.nodeType ? mapId : document.getElementById (mapId);
    var map = new google.maps.Map
        (mapEl, {
           center: pos,
           zoom: parseInt (mapEl.getAttribute ('data-map-zoom')) || 6,
           mapTypeId: google.maps.MapTypeId.HYBRID
         });
    for (var i = 0; i < spots.length; i++) (function (el) {
      var pos = new google.maps.LatLng
          (el.getAttribute ('data-spot-lat'),
           el.getAttribute ('data-spot-lon'));
      var marker = new google.maps.Marker ({
        position: pos,
        map: map,
        title: el.getAttribute ('data-spot-name') || el.textContent
      });
      google.maps.event.addListener (marker, 'click', function () {
        if (onselect) {
          onselect (el);
        } else {
          for (var j = 0; j < spots.length; j++) {
            if (spots[j] === el) {
              spots[j].className += ' selected';
            } else {
              spots[j].className = spots[j].className.replace (/\bselected\b/g, '');
            }
          }
        }
      });
    }) (spots[i]);
  }
} // createMapFromSpotList

function createSpotSelection (sels) {
  var controls = {input: document.querySelector (sels.input),
                  map: document.querySelector (sels.map),
                  select: document.querySelector (sels.select)};
  var lastValue;
  var lastTimer;
  var updateParentSpotsList = function (value) {
    lastValue = value;
    clearTimeout (lastTimer);
    lastTimer = setTimeout (function () {
      var xhr = new XMLHttpRequest ();
      xhr.open ('POST', '/spots/search.json', true);
      var fd = new FormData;
      fd.append ('q', lastValue);
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status < 400) {
            var opt = controls.select;
            opt.innerHTML = '';
            var json = JSON.parse (xhr.responseText);
            for (var i = 0; i < json.items.length; i++) {
              var item = json.items[i];
              var o = document.createElement ('option');
              o.value = item.spot_id;
              o.textContent = item.name;
              if (item.lat || item.lon) {
                o.setAttribute ('data-spot-lat', item.lat);
                o.setAttribute ('data-spot-lon', item.lon);
              }
              opt.appendChild (o);
            }
            createMapFromSpotList
              (controls.map,
               sels.select+' [data-spot-lat][data-spot-lon]',
               function (el) { el.selected = true });
          }
        }
      };
      xhr.send (fd);
    }, 1000);
  }; // updateParentSpotsList

  controls.input.oninput = function () {
    updateParentSpotsList (this.value);
  };
} // createSpotSelection

/*

License

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

*/
