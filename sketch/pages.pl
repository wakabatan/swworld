use strict;
use warnings;
use Path::Class;
use World::Loader::PagesByWord;
  use World::MySQL;
  my $dsns_json_f = file (__FILE__)->dir->parent->subdir ('t', 'tmp')->file ('dsns.json');
  my $dbreg = World::MySQL->create_dbreg_by_dsns_json_f ($dsns_json_f);

my $loader = World::Loader::PagesByWord->new_from_dbreg ($dbreg);
binmode STDOUT, qw(:encoding(utf-8));
use Encode;
my $word = decode 'utf-8', shift;

my $pages = $loader->get_by_word ($word);

$pages->each (sub {
  printf "%d\t%s\t%s\n", $_->page_id, $_->url, $_->title;
});
