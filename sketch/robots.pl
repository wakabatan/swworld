use strict;
use warnings;
use Web::UserAgent::Functions;
#use WWW::RobotRules;
use WWW::RobotRules::Extended;
use Web::URL::Canonicalize qw(url_to_canon_url);

sub check ($$) {
  my ($ua, $url) = @_;
#  my $rules = WWW::RobotRules->new ($ua);
  my $rules = WWW::RobotRules::Extended->new ($ua);
  my $robots_txt = url_to_canon_url q</robots.txt>, $url;
  my (undef, $res) = http_get url => $robots_txt;
  return 1 unless $res->is_success;
  $rules->parse ($robots_txt, $res->content);
  return $rules->allowed ($url);
} # check

my $ua = 'world.suikawiki.org';
my $url = url_to_canon_url shift // '', 'http://suika.suikawiki.org/';

my $result = check $ua, $url;

printf "<%s>: %d\n", $url, $result;
