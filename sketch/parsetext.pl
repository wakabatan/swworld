use strict;
use warnings;
use World::Process::ExtractWords;
use Encode;

binmode STDOUT, qw(:encoding(utf-8));
while (1) {
  print "> ";
  my $process = World::Process::ExtractWords->new;
  $process->parse_text (decode 'utf-8', scalar <>);
  for (@{$process->parsed}) {
    next unless defined $_->{text};
    printf "- %s\t[%s]\n", $_->{text}, join ',', @{$_->{info}};
  }
  $process->extract_words;
  my $words = $process->words;
  print join ' ', map { "$_ ($words->{$_})" } sort { $words->{$b} <=> $words->{$a} } keys %$words;
  print "\n";
}
