use strict;
use warnings;
use Path::Class;
use World::MySQL;
my $dsns_json_f = file (__FILE__)->dir->parent->subdir ('t', 'tmp')->file ('dsns.json');
my $dbreg = World::MySQL->create_dbreg_by_dsns_json_f ($dsns_json_f);

$dbreg->load ('spot')->execute ('
-- alter table spot add column roman VARBINARY(1023);
alter table spot_name add column roman VARBINARY(1023);
');

use Data::Dumper;
warn Dumper $dbreg->load ('page')->execute ('SHOW CREATE TABLE page')->all;
