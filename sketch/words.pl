use strict;
use warnings;
use Path::Class;
use World::Loader::Words;
  use World::MySQL;
  my $dsns_json_f = file (__FILE__)->dir->parent->subdir ('t', 'tmp')->file ('dsns.json');
  my $dbreg = World::MySQL->create_dbreg_by_dsns_json_f ($dsns_json_f);

my $loader = World::Loader::Words->new_from_dbreg ($dbreg);
binmode STDOUT, qw(:encoding(utf-8));

my $words = $loader->get_all_words;

$words->each (sub {
  printf "%d\t%s\n", $_->word_id, $_->text;
});
