use strict;
use warnings;
use World::Process::ExtractWords;
use World::Action::BuildDictionary;
use Path::Class;

binmode STDOUT, qw(:encoding(utf-8));

my $dic_f = file (__FILE__)->dir->file ('fuga.dic');
{
  my $input_f = file (__FILE__)->dir->file ('fuga.txt');
  my $csv_f = file (__FILE__)->dir->file ('fuga.csv');
  my $dict_builder = World::Action::BuildDictionary->new_from_csv_f_and_dic_f
      ($csv_f, $dic_f);
  $dict_builder->mecab_root_d (file (__FILE__)->dir->parent->subdir ('local', 'mecab-0.994-utf-8'));
  $dict_builder->add_from_txt_f ($input_f);
  $dict_builder->write_csv;
  $dict_builder->write_user_dict;
}

my $process = World::Process::ExtractWords->new;
my $data = scalar file (shift)->slurp;
my $charset = shift || 'utf-8';

$process->mecab->user_dic_f ($dic_f);
$process->parse_html ($data, $charset);
$process->extract_words;

my $doc = $process->document;
my $title_el = $doc->query_selector ('h1[itemprop=name]');
my $desc_el = $doc->query_selector ('#des_full p[itemprop=description]');
my $tag_els = $doc->query_selector_all ('#PAGEBODY a[rel=tag]');

use Data::Dumper;
{
  my %info;
  my %type;
  for (@{$process->parsed}) {
    next unless defined $_->{text};
    use utf8;
    next if $_->{text} =~ /^[あ-ん、。]$/;
    $info{$_->{text}}++;
    $type{$_->{text}} = join ',', @{$_->{info}}[0..3];
    printf "%s (%s)\n", $_->{text}, join ',', @{$_->{info}}[0..3];
  }
#  for (sort { $info{$b} <=> $info{$a} } keys %info) {
#    printf "%s (%d / %s)\t", $_, $info{$_}, $type{$_};
#  }
}
warn $title_el->text_content if $title_el;
warn join  ' ', map { $_->text_content } @$tag_els;
print "\n";

print "==============================\n";
$process->merge_words;
my $words = $process->merged_words;
print join ' ', map { "$_ ($words->{$_})" } sort { $words->{$b} <=> $words->{$a} } keys %$words;
print "\n";
