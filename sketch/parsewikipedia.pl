use strict;
use warnings;
use World::Wikimedia;
use AnyEvent;
use Encode;
use Data::Dumper;
use World::Process::ParseWikipedia;
use JSON::Functions::XS qw(perl2json_bytes_for_record);

my $wm = World::Wikimedia->new;
my $cv = AE::cv;
my $word = decode 'utf-8', shift;

$wm->get_wikipedia_ja_text_by_word_as_cv ($word)->cb (sub {
  my $parser = World::Process::ParseWikipedia->new_from_text ($_[0]->recv);
  $parser->extract_sections;
  $parser->extract_table_templates;
  $parser->extract_latlon;
  $parser->extract_image;
  $parser->extract_reading;
  
  warn perl2json_bytes_for_record $parser->table_templates;
  warn $parser->reading;
  warn $parser->lat;
  warn $parser->lon;
  warn $parser->remove_inline_markup ($parser->summary);
  warn perl2json_bytes_for_record $parser->headings;
  warn perl2json_bytes_for_record $parser->image;

  if ($parser->image) {
    $wm->get_wikipedia_ja_image_data_by_as_cv ($parser->image)->cb (sub {
      my $data = $_[0]->recv;
      warn $wm->get_image_url_by_size ($data->{url}, $parser->image->{size});
      $cv->send;
    });
  } else {
    $cv->send;
  }
});

$cv->recv;
