use strict;
use warnings;
use Web::HTML::Parser;
use Path::Class;
use Web::DOM::Document;

my $file_name = shift;
my $f = file ($file_name);
my $doc = new Web::DOM::Document;
my $parser = Web::HTML::Parser->new;
$parser->parse_byte_string (undef, scalar $f->slurp => $doc);

warn $doc->title;
warn $doc->charset;
