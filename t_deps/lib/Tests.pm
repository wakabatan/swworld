package Tests;
use strict;
use warnings;
use Path::Tiny;
use lib glob path (__FILE__)->parent->parent->parent->child ('t_deps/modules/*/lib');
use File::Temp;
use AnyEvent;
use Promise;
use Promised::File;
use Promised::Plackup;
use Promised::Mysqld;
#use Promised::Docker::WebDriver;
use MIME::Base64;
use Digest::SHA qw(sha1_hex);
use JSON::PS;
use Web::UserAgent::Functions qw(http_get http_post);
use Wanage::URL qw(percent_encode_c);
use Test::More;
#use Test::Differences;
use Test::X1;

our @EXPORT;

push @EXPORT, grep { not /^\$/ } @Test::More::EXPORT;
#push @EXPORT, @Test::Differences::EXPORT;
push @EXPORT, @Test::X1::EXPORT;
push @EXPORT, @JSON::PS::EXPORT;
push @EXPORT, qw(percent_encode_c);

sub import ($;@) {
  my $from_class = shift;
  my ($to_class, $file, $line) = caller;
  no strict 'refs';
  for (@_ ? @_ : @{$from_class . '::EXPORT'}) {
    my $code = $from_class->can ($_)
        or die qq{"$_" is not exported by the $from_class module at $file line $line};
    *{$to_class . '::' . $_} = $code;
  }
} # import

my $MySQLServer;
my $HTTPServer;
my $AccountServer;
my $Browsers = {};

my $root_path = path (__FILE__)->parent->parent->parent->absolute;

sub db_sqls () {
  my @x;
  for my $name (qw(word page spot)) {
    my $file = Promised::File->new_from_path ($root_path->child ("db/$name.sql"));
    push @x, $file->read_byte_string->then (sub {
      return {$name => [split /;/, $_[0]]};
    });
  }
  return Promise->all (\@x)->then (sub { return {map { %$_ } @{$_[0]}} });
} # db_sqls

sub account_server () {
  $AccountServer = Promised::Plackup->new;
  $AccountServer->plackup ($root_path->child ('plackup'));
  $AccountServer->envs->{API_TOKEN} = rand;
  $AccountServer->set_option ('--server' => 'Twiggy');
  $AccountServer->set_app_code (q{
    use Wanage::HTTP;
    use Wanage::URL;
    use AnyEvent;
    use Web::UserAgent::Functions qw(http_post);
    use JSON::PS;
    use MIME::Base64;
    my $api_token = $ENV{API_TOKEN};
    my $Sessions = {};
    sub {
      my $env = shift;
      my $http = Wanage::HTTP->new_from_psgi_env ($env);
      my $path = $http->url->{path};
      if ($http->request_method ne 'POST') {
        $http->set_status (405);
      } elsif ($path eq '/session') {
        my $json = {};
        my $session = $Sessions->{$http->request_body_params->{sk}->[0] // ''};
        unless (defined $session) {
          $json->{set_sk} = 1;
          $session = {sk => rand, expires => time + 1000};
          $Sessions->{$session->{sk}} = $session;
        }
        $json->{sk} = $session->{sk};
        $json->{sk_expires} = $session->{expires};
        $http->send_response_body_as_ref (\perl2json_bytes $json);
      } elsif ($path eq '/login') {
        my $json = {};
        my $server = $http->request_body_params->{server}->[0];
        $json->{authorization_url} = qq<http://$server/auth>;
        $http->send_response_body_as_ref (\perl2json_bytes $json);
      } elsif ($path eq '/info') {
        my $json = {};
        my $session = $Sessions->{$http->request_body_params->{sk}->[0] // ''};
        if (defined $session) {
          $json->{account_id} = $session->{account_id};
          $json->{name} = $session->{name};
        }
        $http->send_response_body_as_ref (\perl2json_bytes $json);
      } elsif ($path eq '/profiles') {
        my $json = {};
        my %account_id = map { $_ => 1 } @{$http->request_body_params->{account_id}};
        $json->{accounts}->{$_->{account_id}} = $_
            for grep { $account_id{$_->{account_id}} } values %$Sessions;
        $http->send_response_body_as_ref (\perl2json_bytes $json);

      } elsif ($path eq '/-add-session') {
        my $sk = rand;
        $Sessions->{$sk} = {
          sk => $sk,
          expires => time + 1000,
          account_id => (sprintf '%llu', int (2**63 + rand (2**62))),
          name => rand,
        };
        $http->set_status (201);
        $http->send_response_body_as_ref (\perl2json_bytes $Sessions->{$sk});
      } else {
        $http->set_status (404);
      }
      $http->close_response_body;
      return $http->send_response;
    };
  });
  return $AccountServer->start;
} # account_server

push @EXPORT, qw(web_server);
sub web_server (;$) {
  my $web_host = $_[0];
  my $cv = AE::cv;
  $MySQLServer = Promised::Mysqld->new;
  $MySQLServer->{_temp} = my $temp = File::Temp->newdir;
  my $temp_dir_path = path ($temp)->absolute;
  my $temp_path = $temp_dir_path->child ('file');
  my $temp_repos_path = $temp_dir_path->child ('repos');
  my $temp_file = Promised::File->new_from_path ($temp_path);
  Promise->all ([
    $MySQLServer->start,
    #account_server,
  ])->then (sub {
    my $word_dsn = $MySQLServer->get_dsn_string (dbname => 'word_test');
    my $page_dsn = $MySQLServer->get_dsn_string (dbname => 'page_test');
    my $spot_dsn = $MySQLServer->get_dsn_string (dbname => 'spot_test');
    $HTTPServer = Promised::Plackup->new;
    $HTTPServer->envs->{KARASUMA_CONFIG_JSON} = $temp_path;
    $HTTPServer->envs->{KARASUMA_CONFIG_FILE_DIR_NAME} = $temp_dir_path;
    $HTTPServer->envs->{MYSQL_DSNS_JSON} = $temp_path;
    return Promise->all ([
      db_sqls->then (sub {
        return Promise->all ([
          $MySQLServer->create_db_and_execute_sqls (word_test => $_[0]->{word}),
          $MySQLServer->create_db_and_execute_sqls (page_test => $_[0]->{page}),
          $MySQLServer->create_db_and_execute_sqls (spot_test => $_[0]->{spot}),
        ]);
      }),
      $temp_file->write_byte_string (perl2json_bytes +{
        alt_dsns => {master => {word => $word_dsn,
                                page => $page_dsn,
                                spot => $spot_dsn}},
        dsns => {word => $word_dsn,
                 page => $page_dsn,
                 spot => $spot_dsn},

        wpserver_host => undef,
        edit_basic_path => 'XXX',
      }),
    ]);
  })->then (sub {
    $HTTPServer->plackup ($root_path->child ('plackup'));
    $HTTPServer->set_option ('--host' => $web_host) if defined $web_host;
    $HTTPServer->set_option ('--app' => $root_path->child ('bin/server.psgi'));
    $HTTPServer->set_option ('--server' => 'Twiggy::Prefork');
    return $HTTPServer->start;
  })->then (sub {
    $cv->send ({host => $HTTPServer->get_host,
                hostname => $HTTPServer->get_hostname,
                #account_host => $AccountServer->get_host,
               });
  });
  return $cv;
} # web_server

push @EXPORT, qw(stop_servers);
sub stop_servers () {
  my $cv = AE::cv;
  $cv->begin;
  for ($HTTPServer, $MySQLServer, $AccountServer, values %$Browsers) {
    next unless defined $_;
    $cv->begin;
    $_->stop->then (sub { $cv->end });
  }
  $cv->end;
  $cv->recv;
} # stop_servers

push @EXPORT, qw(GET);
sub GET ($$;%) {
  my ($c, $path, %args) = @_;
  my $host = $c->received_data->{host};
  $path = '/' . join '/', map { percent_encode_c $_ } @$path if ref $path;
  return Promise->new (sub {
    my ($ok, $ng) = @_;
    my $cookies = $args{cookies} || {};
    $cookies->{sk} //= $args{account}->{sk}; # or undef
    http_get
        url => qq<http://$host$path>,
        basic_auth => $args{basic_auth},
        header_fields => $args{header_fields},
        params => $args{params},
        cookies => $cookies,
        timeout => 30,
        anyevent => 1,
        max_redirect => 0,
        cb => sub {
          $ok->($_[1]);
        };
  });
} # GET

push @EXPORT, qw(POST);
sub POST ($$;%) {
  my ($c, $path, %args) = @_;
  my $host = $c->received_data->{host};
  $path = '/' . join '/', map { percent_encode_c $_ } @$path if ref $path;
  return Promise->new (sub {
    my ($ok, $ng) = @_;
    my $cookies = $args{cookies} || {};
    $cookies->{sk} //= $args{account}->{sk}; # or undef
    http_post
        url => qq<http://$host$path>,
        basic_auth => $args{basic_auth},
        header_fields => $args{header_fields},
        params => $args{params},
        files => $args{files},
        cookies => $cookies,
        timeout => 30,
        anyevent => 1,
        max_redirect => 0,
        cb => sub {
          $ok->($_[1]);
        };
  });
} # POST

1;
