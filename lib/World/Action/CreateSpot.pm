package World::Action::CreateSpot;
use strict;
use warnings;
use World::Object::Spot;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub create_spot ($%) {
  my ($self, %args) = @_;

  my $db = $self->dbreg->load ('spot');
  my $spot_id = $db->execute
      ('SELECT UUID_SHORT() AS uuid', undef, source_name => 'master')
          ->first->{uuid};
  $db->table ('spot')->create
      ({spot_id => $spot_id,
        name => $args{name},
        created => time,
        updated => time});
  
  $self->{spot_id} = $spot_id;
} # create_spot

sub spot_id ($) {
  return $_[0]->{spot_id};
} # spot_id

sub spot ($) {
  return undef unless defined $_[0]->{spot_id};
  return World::Object::Spot->new_from_spot_id ($_[0]->{spot_id});
} # spot

1;
