package World::Action::EditCrawlingConfig;
use strict;
use warnings;
use AnyEvent;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub update_disallow_patterns_as_cv ($$) {
  my $cv = AE::cv;
  my $db = $_[0]->db;
  $db->table ('crawling_config')->insert
      ([{id => 1,
         disallow_patterns => $_[1] // ''}],
       duplicate => {disallow_patterns => $db->bare_sql_fragment ('VALUES(disallow_patterns)')});
  $cv->send (1);
  return $cv;
} # update_disallow_patterns

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
