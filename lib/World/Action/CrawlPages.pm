package World::Action::CrawlPages;
use strict;
use warnings;
use AnyEvent;
use World::Defs::CrawlingStatuses;
use World::Action::RegisterPage;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub user_dic_f ($;$) {
  if (@_ > 1) {
    $_[0]->{user_dic_f} = $_[1];
  }
  return $_[0]->{user_dic_f};
} # user_dic_f

sub run_next_action_as_cv ($) {
  my $self = $_[0];
  my $cv = AE::cv;
  my $db = $self->db;

  my $row = $db->table ('crawling_target')->find
      ({crawl_after => {'<=', time},
        status => {'!=', CRAWLING_STATUS_DONE}},
       order => ['crawl_after' => 'DESC']);
  unless ($row) {
    $cv->send (0);
    return $cv;
  }
  
  {
    my $action = World::Action::RegisterPage->new_from_dbreg ($self->dbreg);
    $action->user_dic_f ($self->user_dic_f);
    $action->register_page_by_url_as_cv ($row->get ('url'))->cb (sub {
      if ($_[0]->recv) {
        $action->schedule_site_links_crawling_as_cv->cb (sub {
          $row->update ({status => CRAWLING_STATUS_DONE});
          $cv->send (1);
        });
      } else {
        $row->update ({status => CRAWLING_STATUS_FAILED});
        $cv->send (1);
      }
    });
  }

  return $cv;
} # run_next_action_as_cv

1;
