package World::Action::EditWordDict;
use strict;
use warnings;
use AnyEvent;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('word');
} # db

sub add_word_as_cv ($$$%) {
  my ($self, $text, $type, %args) = @_;
  my $cv = AE::cv;
  if (not defined $text or not length $text or not $type) {
    $cv->send (0);
    return $cv;
  }

  my $reading = (defined $args{reading} and length $args{reading})
      ? $args{reading} : '';

  my $db = $self->db;
  my $id = $db->execute ('SELECT UUID_SHORT() AS uuid', undef,
                         source_name => 'master')->first->{uuid};
  $db->table ('dict_word')->insert
      ([{dwid => $id,
         text => $text,
         canon_text => ((defined $args{canon_text} and length $args{canon_text})
                            ? $args{canon_text} : $text),
         reading => $reading,
         canon_reading => ((defined $args{canon_reading} and length $args{canon_reading})
                           ? $args{canon_reading} : $reading),
         type => $type,
         created => time}]);

  $cv->send (1);
  return $cv;
} # add_word_as_cv

sub edit_word_as_cv ($$%) {
  my ($self, $dwid, %args) = @_;
  my $cv = AE::cv;

  my $db = $self->db;
  my $row = $db->table ('dict_word')->find ({dwid => $dwid});
  unless ($row) {
    $cv->send (0);
    return $cv;
  }

  my %param;
  for (qw(text canon_text reading canon_reading type)) {
    if (defined $args{$_} and length $args{$_}) {
      $param{$_} = $args{$_};
    }
  }

  $row->update (\%param) if keys %param;
  
  $cv->send (1);
  return $cv;
} # edit_word_as_cv

sub delete_word_as_cv ($$) {
  my ($self, $dwid) = @_;
  my $cv = AE::cv;

  my $db = $self->db;
  $db->delete ('dict_word', {dwid => $dwid});

  $cv->send (1);
  return $cv;
} # delete_word_as_cv

1;
