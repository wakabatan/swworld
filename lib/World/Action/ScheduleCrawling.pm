package World::Action::ScheduleCrawling;
use strict;
use warnings;
use AnyEvent;
use Web::URL::Canonicalize qw(url_to_canon_url);

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub add_urls_as_cv ($$) {
  my $cv = AE::cv;
  my $page_db = $_[0]->db;
  my $urls = [grep { /^https?:/ } map { url_to_canon_url $_, 'about:blank' } @{$_[1]}];
  unless (@$urls) {
    $cv->send (0);
    return $cv;
  }
  
  my $i = 0;
  my $uuids = $page_db->execute
      ('SELECT '.(join ',', map { 'UUID_SHORT () AS uuid' . $i++ } @$urls), {},
       source_name => 'master')->first;
  $i = 0;
  my $result = $page_db->insert ('crawling_target',
                                 [map { 
                                   +{ctid => $uuids->{'uuid'.$i++},
                                     url => $_,
                                     created => time,
                                     updated => time,
                                     crawl_after => time};
                                 } @$urls],
                                 duplicate => 'ignore');
  $cv->send ($result->row_count);
  return $cv;
} # add_url

1;
