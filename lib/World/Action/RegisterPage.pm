package World::Action::RegisterPage;
use strict;
use warnings;
use utf8;
use AnyEvent;
use Time::Local qw(timegm_nocheck);
use Dongry::Type;
use WWW::RobotRules::Extended;
use Web::UserAgent::Functions qw(http_get http_post);
use Web::URL::Canonicalize qw(url_to_canon_url);
use Wanage::HTTP::MIMEType;
use World::Defs::PageStatuses;
use World::Object::Page;
use World::Action::ScheduleCrawling;

sub new_from_dbreg_and_config ($$$) {
  return bless {dbreg => $_[1], config => $_[2]}, $_[0];
} # new_from_dbreg_and_config

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub user_dic_f ($;$) {
  if (@_ > 1) {
    $_[0]->{user_dic_f} = $_[1];
  }
  return $_[0]->{user_dic_f};
} # user_dic_f

sub parse_html ($$$) {
  my $self = $_[0];
  require World::Process::ExtractWords;
  my $process = World::Process::ExtractWords->new;
  $process->mecab->user_dic_f ($self->user_dic_f);
  $process->parse_html ($_[1], $_[2]);
  my $doc = $self->{document} = $process->document;
  my $robots = {};
  for my $meta ($doc->get_elements_by_tag_name_ns
                    ('http://www.w3.org/1999/xhtml', 'meta')->to_list) {
    my $name = $meta->name;
    next unless $name =~ /\A[Rr][Oo][Bb][Oo][Tt][Ss]\z/;
    for (map { s/\A[\x09\x0A\x0C\x0D\x20]+//; s/[\x09\x0A\x0C\x0D\x20]+\z//; tr/A-Z/a-z/; $_ } split /,/, $meta->content) {
      $robots->{$_} = 1;
    }
  }
  $self->{robots} = $robots;
  return if $robots->{noindex};
  $process->extract_words;
  $self->{parsed} = $process->parsed;
  $self->{words} = $process->words;
  $self->{title} = $process->document->title;
  $self->{document}->manakai_set_url ($self->{url});
  $self->{extract_process} = $process;
} # parse_html

sub parse_text ($$) {
  my $self = $_[0];
  require World::Process::ExtractWords;
  my $process = World::Process::ExtractWords->new;
  $process->mecab->user_dic_f ($self->user_dic_f);
  $process->parse_text ($_[1]);
  $process->extract_words;
  $self->{parsed} = $process->parsed;
  $self->{words} = $process->words;
  $self->{extract_process} = $process;
} # parse_text

sub extract_process ($) {
  return $_[0]->{extract_process};
} # extract_process

sub extract_content ($) {
  my $self = $_[0];
  return if $self->{robots}->{noindex};
  my $body = $self->{document}->body or return;

  require HTML::ExtractContent;
  my $extractor = HTML::ExtractContent->new;
  $extractor->extract ($body->inner_html);
  $self->{extracted_content} = substr $extractor->as_text, 0, 1000;

  {
    my $date_el = $body->query_selector ('.date, .posted');
    if ($date_el) {
      if ($date_el->text_content =~ m{((?:19|20)\d\d)\s*[./年-]\s*([01]?\d)\s*[./月-]\s*([0-3]?\d)}) {
        $self->{last_modified} = timegm_nocheck 0, 0, 0, $3, $2-1, $1;
      }
    }
  }

  if (not defined $self->{last_modified}) {
    require Wanage::HTTP::Date;
    my $res = $self->{http_res};
    if ($res) {
      my $time = Wanage::HTTP::Date::parse_http_date
          ($res->header ('Last-Modified') // '');
      $self->{last_modified} = $time if $time;
    }
  }

  my $photo_count = 0;
  my $map_count = 0;
  my $video_count = 0;
  for ($self->{document}->images->to_list) {
    my $url = $_->src;
    if ($url =~ /map/ or $url =~ /%E5%9C%B0%E5%9B%B3/) {
      $map_count++;
    } elsif ($url =~ /[Jj][Pp][Ee]?[Gg]/) {
      $photo_count++;
    }
  }
  for ($self->{document}->get_elements_by_tag_name_ns
           ('http://www.w3.org/1999/xhtml', 'iframe')->to_list) {
    my $url = $_->get_attribute ('src') // '';
    if ($url =~ m{maps.google}) {
      $map_count++;
    }
  }
  for (@{$self->{extract_process}->{scripts}}) {
    next unless $_->local_name eq 'script';
    my $url = $_->src;
    if ($url =~ m{cyberjapan.jp/ptmap/}) {
      $map_count++;
    }
  }
  for ($self->{document}->links->to_list) {
    my $href = $_->href;
    if ($href =~ /\.(?:avi|wmv|mpeg|mp4|flv)/) {
      $video_count++;
    }
  }
  $video_count += $self->{document}->get_elements_by_tag_name_ns
      ('http://www.w3.org/1999/xhtml', 'video')->length;
  $self->{photo_count} = $photo_count;
  $self->{map_count} = $map_count;
  $self->{video_count} = $video_count;

  my $doc_url = $self->{document}->url;
  if ($doc_url =~ m{^https?://([0-9a-z.-]+)/}) {
    my $doc_host = $1;
    my $links = $self->{site_links} = {};
    for my $el ($self->{document}->links->to_list) {
      my $href = $el->href;
      $href =~ s{\#.*$}{}s;
      next if $href eq $doc_url;
      next unless $href =~ m{^https?://([0-9a-z.-]+)/};
      my $host = $1;
      next unless $doc_host eq $host;
      $links->{$href}++;
    }
  }
} # extract_content

sub words ($) {
  return $_[0]->{words};
} # words

sub register_page_by_youtube_id_as_cv ($$) {
  my ($self, $video_id) = @_;
  my $cv = AE::cv;

  require World::YouTube;
  my $youtube = World::YouTube->new_from_config_and_video_id
      ($self->{config}, $video_id);
  $youtube->get_video_data_as_cv->cb (sub {
    my $data = $_[0]->recv;
    if ($data) {
      $self->{title} = $data->{title};
      $self->{last_modified} = $data->{published};
      $self->{video_count} = 1;
      $self->{extracted_content} = $data->{content};
      $self->{media_duration} = $data->{seconds};
      $self->parse_text ($data->{title} . "\n" . $data->{content});
      $self->_write_page_as_cv->cb (sub {
        $self->_write_words_as_cv->cb (sub { $cv->send (1) });
      });
    } else {
      $cv->send (0);
    }
  });

  return $cv;
} # register_page_by_youtube_id_as_cv

sub register_page_by_data_as_cv ($$$) {
  #my ($self, undef, $charset) = @_;
  my $self = shift;
  $self->parse_html (@_);
  my $cv = AE::cv;

  if ($self->{robots}->{noindex} or $self->{robots}->{none}) {
    my $page_db = $self->dbreg->load ('page');
    $page_db->table ('page')->update
        ({status => PAGE_STATUS_HIDDEN_BY_META},
         where => {url => $self->{url}});
    $self->{error} = 'meta';
    $cv->send;
    return $cv;
  }

  $self->extract_content;
  $self->_write_page_as_cv->cb (sub {
    $self->_write_words_as_cv->cb (sub {
      my $word_to_id = $_[0]->recv;
      my $spot_db = $self->dbreg->load ('spot');
      if (keys %$word_to_id) {
        my $spot_ids = $spot_db->select ('spot_word', {
          word_id => {-in => [values %$word_to_id]},
        }, fields => ['spot_id'])->all->map (sub { $_->{spot_id} });
        if (@$spot_ids) {
          my $spot_id_to_name = {};
          $spot_db->select ('spot', {
            spot_id => {-in => $spot_ids},
          }, fields => ['spot_id', 'name'])->each (sub {
            $spot_id_to_name->{$_->{spot_id}} = Dongry::Type->parse ('text', $_->{name});
          });
          my $feed_url = $self->{config}->get_text ('feed_post_url');
          my $site_url = $self->{config}->get_text ('web_origin');
          for my $spot_id (keys %$spot_id_to_name) {
            my $name = $spot_id_to_name->{$spot_id};
            http_post
                url => $feed_url,
                params => {
                  url => $site_url . q</spots/> . $spot_id,
                  title => $name,
                },
                anyevent => 1;
          }
        }
      }
      $cv->send;
    });
  });
  return $cv;
} # register_page_by_data_as_cv

sub _write_page_as_cv ($) {
  my $self = $_[0];
  my $cv = AE::cv;

  my %page_data = (url => $self->{url},
                   title => $self->{title},
                   timestamp => $self->{last_modified} || 0,
                   photo_count => $self->{photo_count} || 0,
                   map_count => $self->{map_count} || 0,
                   video_count => $self->{video_count} || 0);
  $page_data{summary} = $self->{extracted_content}
      if defined $self->{extracted_content};

  my $page_db = $self->dbreg->load ('page');
  my $page_id = $page_db->execute
      ('SELECT UUID_SHORT() AS uuid', undef, source_name => 'master')
          ->first->{uuid};
  $page_db->table ('page')->insert
      ([{page_id => $page_id,
         %page_data,
         created => time}],
       duplicate => {map { $_ => $page_db->bare_sql_fragment ('VALUES('.$_.')') } grep { exists $page_data{$_} }
                         qw(title summary timestamp photo_count
                            map_count video_count)});
  $page_id = $page_db->table ('page')
      ->find ({url => $self->{url}}, fields => 'page_id')
      ->get ('page_id');
  $self->{page_id} = $page_id;
  $page_db->table ('page')->update
      ({status => PAGE_STATUS_NORMAL},
       where => {page_id => $page_id,
                 status => {'!=', PAGE_STATUS_HIDDEN_BY_OTHER}});

  my $page_data = {};
  $page_data->{site_links} = $self->{site_links} if $self->{site_links};
  $page_data->{media_duration} = $self->{media_duration}
      if defined $self->{media_duration};
  $page_db->table ('page_data')->insert
      ([{page_id => $page_id,
         parsed_data => $page_data,
         created => time,
         updated => time}],
       duplicate => {parsed_data => $page_db->bare_sql_fragment ('VALUES(parsed_data)'),
                     updated => $page_db->bare_sql_fragment ('VALUES(updated)')});

  $cv->send;
  return $cv;
} # _write_page_as_cv

sub _write_words_as_cv ($) {
  my $self = $_[0];
  my $cv = AE::cv;
  my $words = $self->{words} || {};
  unless (keys %$words) {
    $cv->send;
    return $cv;
  }

  my $page_id = $self->{page_id};
  my $page_db = $self->dbreg->load ('page');
  my $word_db = $self->dbreg->load ('word');
  my $ids = $word_db->execute
      ('SELECT ' . (join ',', map { 'UUID_SHORT() AS uuid' . $_ } 1..scalar keys %$words), undef, source_name => 'master')->first;
  my $i = 1;
  $word_db->table ('word')->insert
      ([map { +{
         word_id => $ids->{'uuid'.$i++},
         text => $_,
         created => time,
       } } keys %$words],
       duplicate => 'ignore');

  my $word_to_id = {};
  $word_db->table ('word')->find_all ({text => {-in => [keys %$words]}})
      ->each (sub { $word_to_id->{$_->get ('text')} = $_->get ('word_id') });
  $page_db->table ('page_word')->insert
      ([map { +{
         page_id => $page_id,
         word_id => $word_to_id->{$_},
         frequency => $words->{$_},
         created => time,
       } } keys %$words],
       duplicate => {frequency => $page_db->bare_sql_fragment ('VALUES(frequency)')});
  $cv->send ($word_to_id);
  return $cv;
} # _write_words_as_cv

sub user_agent_string ($) {
  return 'world.suikawiki.org';
} # user_agent_string

sub check_robots_txt_as_cv ($$) {
  my ($self, $url) = @_;
  my $cv = AE::cv;
  my $robots_txt = url_to_canon_url q</robots.txt>, $url;
  http_get url => $robots_txt, anyevent => 1, cb => sub {
    my (undef, $res) = @_;
    unless ($res->is_success) { ## No robots.txt
      $cv->send (1);
      return;
    }
    my $rules = WWW::RobotRules::Extended->new ($self->user_agent_string);
    $rules->parse ($robots_txt, $res->content);
    $cv->send ($rules->allowed ($url));
  };
  return $cv;
} # check_robots_txt_as_cv

sub check_crawling_rules_as_cv ($$) {
  my ($self, $url) = @_;
  my $cv = AE::cv;

  my $page_db = $self->dbreg->load ('page');
  my $row = $page_db->table ('crawling_config')->find ({id => 1});
  my $disallows = [grep { length }
                   split /[\x0D\x0A]/,
                   $row ? $row->get ('disallow_patterns') : ''];
  for my $pattern (@$disallows) {
    next if $pattern =~ /^\s*\#/;
    if ($url =~ /$pattern/) {
      $cv->send (0);
      return $cv;
    }
  }

  $cv->send (1);
  return $cv;
} # check_crawling_rules_as_cv

sub parse_url ($$) {
  my ($self, $url) = @_;
  if ($url =~ m{^https?://www\.youtube\.com/watch\?v=([^&%]+)(?:$|&)}) {
    return {type => 'youtube', video_id => $1};
  } else {
    return {type => 'default'};
  }
} # parse_url

sub register_page_by_url_as_cv ($$) {
  my ($self, $url) = @_;
  $self->{url} = $url;

  my $parsed_url = $self->parse_url ($url);
  if ($parsed_url->{type} eq 'youtube') {
    return $self->register_page_by_youtube_id_as_cv ($parsed_url->{video_id});
  }

  my $cv = AE::cv;

  my $cv1 = AE::cv;
  $self->check_crawling_rules_as_cv ($url)->cb (sub {
    unless ($_[0]->recv) {
      $self->{error} = 'admin';
      my $page_db = $self->dbreg->load ('page');
      $page_db->table ('page')->update
          ({status => PAGE_STATUS_HIDDEN_BY_OTHER},
           where => {url => $url});
      $cv->send (0);
      return;
    }
    $cv1->send;
  });
  my $cv2 = AE::cv;
  $cv1->cb (sub {
    $self->check_robots_txt_as_cv ($url)->cb (sub {
      unless ($_[0]->recv) {
        $self->{error} = 'robots.txt';
        my $page_db = $self->dbreg->load ('page');
        $page_db->table ('page')->update
            ({status => PAGE_STATUS_HIDDEN_BY_ROBOTS},
             where => {url => $url});
        $cv->send (0);
        return;
      }
      $cv2->send;
    });
  });
  $cv2->cb (sub {
    http_get
        url => $url,
        anyevent => 1,
        cb => sub {
          my (undef, $res) = @_;
          $cv->send (0) unless $res->is_success;

          $self->{http_res} = $res;
          my $ct = Wanage::HTTP::MIMEType->new_from_content_type
              ($res->header ('Content-Type'));
          $self->register_page_by_data_as_cv
              ($res->content, $ct->params->{charset})->cb (sub {
            $cv->send (not $self->{error});
          });
        };
  });

  return $cv;
} # register_page_by_url_as_cv

sub document ($) {
  return $_[0]->{document};
} # document

sub parsed_structure ($) {
  return $_[0]->{parsed};
} # parsed_structure

sub page_id ($) {
  return $_[0]->{page_id};
} # page_id

sub page ($) {
  return undef unless defined $_[0]->{page_id};
  return $_[0]->{page} ||= World::Object::Page->new_from_page_id ($_[0]->{page_id});
} # page

sub schedule_site_links_crawling_as_cv ($) {
  my $site_links = $_[0]->{site_links} || {};
  unless (keys %$site_links) {
    my $cv = AE::cv;
    $cv->send (1);
    return $cv;
  }
  my $action = World::Action::ScheduleCrawling->new_from_dbreg ($_[0]->{dbreg});
  return $action->add_urls_as_cv ([keys %$site_links]);
} # schedule_site_links_crawling_as_cv

sub error ($) {
  return $_[0]->{error};
} # error

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
