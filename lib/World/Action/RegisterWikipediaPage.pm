package World::Action::RegisterWikipediaPage;
use strict;
use warnings;
use World::Wikimedia;
use World::Action::RegisterPage;
use World::Process::ParseWikipedia;

sub new_from_dbreg_and_config ($$$) {
  return bless {dbreg => $_[1], config => $_[2]}, $_[0];
} # new_from_dbreg_and_config

sub lang ($;$) {
  if (@_ > 1) {
    $_[0]->{lang} = $_[1];
  }
  return $_[0]->{lang} // 'ja';
} # lang

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub user_dic_f ($;$) {
  if (@_ > 1) {
    $_[0]->{user_dic_f} = $_[1];
  }
  return $_[0]->{user_dic_f};
} # user_dic_f

sub register_by_word_as_cv ($$) {
  my $self = $_[0];
  my $word = $_[1];
  my $cv = AE::cv;
  my $cv0 = AE::cv;
  $cv0->begin;

  my $wm = World::Wikimedia->new;
  my $cv1 = AE::cv;
  $cv1->begin;
  $cv1->begin;
  {
    my $action = World::Action::RegisterPage->new_from_dbreg_and_config
        ($self->{dbreg}, $self->{config});
    my $url = $wm->get_wikipedia_ja_url ($word);
    $action->user_dic_f ($self->user_dic_f);
    $action->register_page_by_url_as_cv ($url)->cb (sub {
      unless ($_[0]->recv) {
        $cv->send (0);
        return;
      }

      $self->{page} = $action->page;
      $cv1->end;
    });
  }

  my $parser;
  my $parsed_data = {};
  my $cv2 = AE::cv;
  $cv1->begin;
  $cv0->begin;
  {
    $wm->get_wikipedia_ja_text_by_word_as_cv ($word)->cb (sub {
      my $text = $_[0]->recv;
      unless (defined $text) {
        $cv0->end;
        $cv->send (0);
        return;
      }

      $parser = World::Process::ParseWikipedia->new_from_text ($text);
      $parser->extract_sections;
      $parser->extract_table_templates;
      $parser->extract_latlon;
      $parser->extract_image;
      $parser->extract_reading;
      $parsed_data = {summary => $parser->remove_inline_markup ($parser->summary),
                      headings => $parser->headings,
                      image => $parser->image};
      if ($parsed_data->{image}) {
        #$cv0->begin;
        #$cv0->end;
        $cv2->send;
      } else {
        $cv0->end;
      }
      $cv1->end;
    });
  }
  $cv1->end;

  my $page_db = $self->db;
  $cv1->cb (sub {
    $page_db->table ('wikipedia')->insert
        ([{page_id => $self->page->page_id,
           lang => $self->lang,
           name => $word,
           reading => $parser->reading // '',
           lat => $parser->lat || 0,
           lon => $parser->lon || 0,
           parsed_data => $parsed_data,
           created => time,
           updated => time}],
         duplicate => {reading => $page_db->bare_sql_fragment ('VALUES(reading)'),
                       lat => $page_db->bare_sql_fragment ('VALUES(lat)'),
                       lon => $page_db->bare_sql_fragment ('VALUES(lon)'),
                       parsed_data => $page_db->bare_sql_fragment ('VALUES(parsed_data)'),
                       updated => time});
    $cv0->cb (sub {
      $cv->send (1);
    });
  });

  $cv2->cb (sub {
    $wm->get_wikipedia_ja_image_data_by_as_cv ($parsed_data->{image})->cb (sub {
      my $data = $_[0]->recv;
      $page_db->table ('wikipedia_image')->insert
          ([{file_name => $parsed_data->{image}->{file_name},
             url => $data->{url},
             created => time}],
           duplicate => 'ignore');
      $cv0->end;
    });
  });

  $cv0->end;
  return $cv;
} # register_by_word_as_cv

sub page ($) {
  return $_[0]->{page};
} # page

sub error ($) {
  return $_[0]->{error};
} # error

1;
