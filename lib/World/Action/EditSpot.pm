package World::Action::EditSpot;
use strict;
use warnings;
use AnyEvent;
use Digest::SHA qw(sha1_hex);
use Encode qw(encode);

sub new_from_dbreg_and_spot_and_config ($$$$) {
  return bless {dbreg => $_[1], spot => $_[2], config => $_[3]}, $_[0];
} # new_from_dbreg_and_spot_and_config

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub spot ($) {
  return $_[0]->{spot};
} # spot

sub spot_id ($) {
  return $_[0]->spot->spot_id;
} # spot_id

sub user_dic_f ($;$) {
  if (@_ > 1) {
    $_[0]->{user_dic_f} = $_[1];
  }
  return $_[0]->{user_dic_f};
} # user_dic_f

sub edit_spot_data ($%) {
  my ($self, %args) = @_;
  my $row = $self->spot->spot_row or die "The spot has no row data loaded";
  my %new;
  for (qw(name reading roman location desc lat lon)) {
    $new{$_} = $args{$_} if defined $args{$_};
  }
  return unless keys %new;
  $new{updated} = time;
  $row->update (\%new);
} # edit_spot_data

sub _sha1 ($) {
  return sha1_hex encode 'utf-8', $_[0];
} # _sha1

sub add_spot_name_as_cv ($$$$) {
  my ($self, $name, $reading, $roman) = @_;
  my $cv = AE::cv;
  if (not defined $name or not length $name) {
    $cv->send (0);
    return $cv;
  }

  my $db = $self->dbreg->load ('spot');
  my $spot_id = $self->spot->spot_id;
  $db->table ('spot_name')->insert
      ([{spot_name_sha1 => _sha1 (join $;, $spot_id, $name, $reading, $roman),
         spot_id => $spot_id,
         name => $name,
         reading => $reading,
         roman => $roman,
         created => time}],
       duplicate => 'ignore');
  $cv->send (1);
  return $cv;
} # add_spot_name_as_cv

sub delete_spot_name_by_sha1_as_cv ($$$) {
  my ($self, $sha1) = @_;
  my $cv = AE::cv;

  my $db = $self->dbreg->load ('spot');
  $db->delete ('spot_name', {spot_name_sha1 => $sha1});

  $cv->send (1);
  return $cv;
} # delete_spot_name_by_sha1_as_cv

sub edit_words ($$) {
  my ($self, $words) = @_;

  my @word_id;
  if (@$words) {
    my %found_word;
    my $word_db = $self->{dbreg}->load ('word');
    $word_db->table ('word')->find_all ({text => {-in => $words}},
                                        fields => ['word_id', 'text'])->each (sub {
      $found_word{$_->get ('text')}++;
      push @word_id, $_->get ('word_id');
    });
    $words = [grep { not $found_word{$_} } @$words];
    if (@$words) {
      my $ids = $word_db->execute
          ('SELECT ' . (join ',', map { 'UUID_SHORT() AS uuid' . $_ } 1..scalar @$words), undef, source_name => 'master')->first;
      my $i = 1;
      $word_db->table ('word')->insert
          ([map { +{
             word_id => $ids->{'uuid'.$i++},
             text => $_,
             created => time,
           } } @$words],
           duplicate => 'ignore');
      push @word_id, $word_db->table ('word')
          ->find_all ({word_id => {-in => [values %$ids]}}, fields => 'word_id')
          ->map (sub { $_->get ('word_id') })
          ->to_list;
    }
  }

  my $spot_db = $self->dbreg->load ('spot');
  if (@word_id) {
    my $spot_id = $self->spot_id;
    my $time = time;
    $spot_db->delete ('spot_word', {spot_id => $spot_id,
                                    word_id => {-not_in => \@word_id}});
    $spot_db->insert ('spot_word', [map { +{spot_id => $spot_id,
                                            word_id => $_,
                                            created => $time} } @word_id],
                      duplicate => 'ignore');
  } else {
    $spot_db->delete ('spot_word', {spot_id => $self->spot_id});
  }
} # edit_words

sub set_wikipedia_ja_as_cv ($$) {
  my ($self, $word) = @_;
  my $cv = AE::cv;
  my $row = $self->spot->spot_row or die "The spot has no row data loaded";
  if ($row->get ('wikipedia_ja') eq $word) {
    $cv->send (1);
    return $cv;
  }
  if (defined $word and length $word) {
    require World::Action::RegisterWikipediaPage;
    my $action = World::Action::RegisterWikipediaPage->new_from_dbreg_and_config
        ($self->dbreg, $self->{config});
    $action->user_dic_f ($self->user_dic_f);
    $action->register_by_word_as_cv ($word)->cb (sub {
      $row->update ({wikipedia_ja => $word, updated => time});
      $cv->send (1);
    });
  } else {
    $row->update ({wikipedia_ja => '', updated => time});
    $cv->send (1);
  }
  return $cv;
} # set_wikipedia_ja_as_cv

sub set_ksj_as_cv ($$$) {
  my ($self, $type, $code) = @_;
  my $cv = AE::cv;
  my $row = $self->spot->spot_row or die "The spot has no row data loaded";
  $row->update ({ksj_type => $type || 0, ksj_code => $code || 0,
                 updated => time});
  $cv->send (1);
  return $cv;
} # set_ksj_as_cv

sub set_go_link_as_cv ($$$) {
  my ($self, $go_type, $go_id) = @_;
  my $cv = AE::cv;

  my $spot_db = $self->dbreg->load ('spot');
  my $spot_id = $self->spot->spot_id;
  if (defined $go_id and length $go_id) {
    $spot_db->insert ('spot_go_link', [{spot_id => $spot_id,
                                        go_type => $go_type,
                                        go_id => $go_id}],
                      duplicate => 'replace');
  } else {
    $spot_db->delete
        ('spot_go_link', {spot_id => $spot_id, go_type => $go_type});
  }
  AE::postpone { $cv->send (1) };

  return $cv;
} # set_go_link_as_cv

sub add_parent_spot_by_id_as_cv ($$) {
  my ($self, $parent_spot_id) = @_;
  my $cv = AE::cv;

  my $spot_db = $self->dbreg->load ('spot');
  unless ($spot_db->execute ('select spot_id from spot where spot_id = ?',
                             {spot_id => $parent_spot_id})->first) {
    $cv->send (0);
    return $cv;
  }

  $spot_db->insert ('spot_link',
                    [{parent_spot_id => $parent_spot_id,
                      child_spot_id => $self->spot->spot_id,
                      created => time}],
                    duplicate => 'ignore');

  $cv->send (1);
  return $cv;
} # add_parent_spot_by_id_as_cv

sub set_page_weight_by_page_id_as_cv ($$$) {
  my ($self, $page_id, $weight) = @_;
  my $cv = AE::cv;

  my $page_db = $self->{dbreg}->load ('page');
  unless ($page_db->select ('page', {page_id => $page_id},
                            fields => 'page_id', limit => 1)->first) {
    $cv->send (0);
    return $cv;
  }

  my $db = $self->{dbreg}->load ('spot');
  $db->insert ('spot_page', [{spot_id => $self->spot_id,
                              page_id => $page_id,
                              weight => $weight,
                              created => time,
                              updated => time}],
               duplicate => {weight => $weight,
                             updated => time});
  $cv->send (1);
  return $cv;
} # set_page_weight_as_cv

sub touch_as_cv ($) {
  my $self = $_[0];
  my $cv = AE::cv;

  $self->dbreg->load ('spot')->update
      ('spot', {updated => time}, where => {spot_id => $self->spot->spot_id});

  $cv->send (1);
  return $cv;
} # touch_as_cv

1;
