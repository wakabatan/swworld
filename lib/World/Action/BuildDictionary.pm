package World::Action::BuildDictionary;
use strict;
use warnings;
use utf8;
use Encode;
use Path::Class;
use World::Defs::WordTypes;

sub new_from_csv_f_and_dic_f ($$$) {
  return bless {csv_f => $_[1], dic_f => $_[2]}, $_[0];
} # new_from_csv_f_and_dic_f

sub csv_f ($) {
  return $_[0]->{csv_f};
} # csv_f

sub dic_f ($) {
  return $_[0]->{dic_f};
} # dic_f

sub add_entry ($%) {
  my ($self, %data) = @_;
  push @{$self->{entries}}, \%data;
} # add_entry

sub add_geographical_proper_noun ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '地域',
         category3 => '一般');
} # add_geographical_proper_noun

sub add_person_name ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '人名',
         category3 => '一般');
} # add_person_name

sub add_organization ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '組織');
} # add_organization

sub add_building ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '地域',
         category3 => '建造物名');
} # add_building

sub add_area ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '地域',
         category3 => '一般');
} # add_area

sub add_geo_natural ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '地域',
         category3 => '自然物名');
} # add_geo_natural

sub add_doable_noun ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => 'サ変接続');
} # add_doable_noun

sub add_misc_proper_noun ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '固有名詞',
         category2 => '一般');
} # add_misc_proper_noun

sub add_misc_prefix ($$$$$) {
  $_[0]->add_entry
      (surface => $_[1],
       yomi => $_[2],
       original => $_[3],
       pronounce => $_[4],
       pos => '接頭辞',
       category1 => '名詞接続');
} # add_misc_prefix

sub add_geographical_suffix ($$$$$) {
  $_[0]->add_entry
        (surface => $_[1],
         yomi => $_[2],
         original => $_[3],
         pronounce => $_[4],
         pos => '名詞',
         category1 => '接尾',
         category2 => '地域');
} # add_geographical_suffix

sub add_misc_suffix ($$$$$) {
  $_[0]->add_entry
      (surface => $_[1],
       yomi => $_[2],
       original => $_[3],
       pronounce => $_[4],
       pos => '名詞',
       category1 => '接尾',
       category2 => '一般');
} # add_misc_suffix

sub add_adjectival_noun ($$$$$) {
  $_[0]->add_entry
      (surface => $_[1],
       yomi => $_[2],
       original => $_[3],
       pronounce => $_[4],
       pos => '名詞',
       category1 => '形容動詞語幹');
} # add_adjectival_noun

sub add_misc_noun ($$$$$) {
  $_[0]->add_entry
      (surface => $_[1],
       yomi => $_[2],
       original => $_[3],
       pronounce => $_[4],
       pos => '名詞',
       category1 => '一般');
} # add_misc_noun

sub add_symbol ($$) {
  $_[0]->add_entry (surface => $_[1], pos => '記号');
} # add_symbol

sub add_from_txt_f ($$) {
  my $self = $_[0];
  my $input = [map { [map { [split /\s+/, $_] } split /\n/, $_] } split /\n\n/, decode 'utf-8', $_[1]->slurp];
  $self->add_geographical_proper_noun (@$_) for @{$input->[0]};
  $self->add_geographical_suffix (@$_) for @{$input->[1]};
  $self->add_misc_proper_noun (@$_) for @{$input->[2]};
  $self->add_misc_noun (@$_) for @{$input->[3]};
  $self->add_misc_prefix (@$_) for @{$input->[4]};
  $self->add_misc_suffix (@$_) for @{$input->[5]};
  $self->add_adjectival_noun (@$_) for @{$input->[6]};
  $self->add_doable_noun (@$_) for @{$input->[6]};
  $self->add_symbol (@$_) for @{$input->[8]};
} # add_from_txt_f

sub add_from_dbreg ($$) {
  my ($self, $dbreg) = @_;
  my $db = $dbreg->load ('word');
  my $rows = {};
  $db->table ('dict_word')->find_all ({dwid => {'>', 0}})->each (sub {
    push @{$rows->{$_->get ('type')} ||= []}, $_;
  });
  for (
    [add_geographical_proper_noun => WORD_TYPE_GEO_PROPER_NOUN],
    [add_geographical_suffix      => WORD_TYPE_GEO_SUFFIX],
    [add_misc_proper_noun         => WORD_TYPE_MISC_PROPER_NOUN],
    [add_misc_noun                => WORD_TYPE_MISC_NOUN],
    [add_misc_prefix              => WORD_TYPE_MISC_PREFIX],
    [add_misc_suffix              => WORD_TYPE_MISC_SUFFIX],
    [add_adjectival_noun          => WORD_TYPE_ADJECTIVE_NOUN],
    [add_doable_noun              => WORD_TYPE_DOABLE_NOUN],
    [add_symbol                   => WORD_TYPE_SYMBOL],
    [add_person_name              => WORD_TYPE_PERSON_NAME],
    [add_organization             => WORD_TYPE_ORGANIZATION],
    [add_building                 => WORD_TYPE_BUILDING],
    [add_area                     => WORD_TYPE_AREA],
    [add_geo_natural              => WORD_TYPE_GEO_NATURAL],
    [add_person_name              => WORD_TYPE_PERSON_NAME],
    [add_organization             => WORD_TYPE_ORGANIZATION],
    [add_building                 => WORD_TYPE_BUILDING],
    [add_area                     => WORD_TYPE_AREA],
    [add_geo_natural              => WORD_TYPE_GEO_NATURAL],
  ) {
    my ($method, $key) = @$_;
    $self->$method (map { $_ eq '' ? undef : $_ }
                    $_->get ('text'), $_->get ('reading'),
                    $_->get ('canon_text'), $_->get ('canon_reading'))
        for @{$rows->{$key} or []};
  }
} # add_from_dbreg

sub add_predefined ($) {
  my $self = $_[0];
  for (
    [qw(1 一 イチ)],
    [qw(2 二 ニ)],
    [qw(3 三 サン)],
    [qw(4 四 ヨン)],
    [qw(5 五 ゴ)],
    [qw(6 六 ロク)],
    [qw(7 七 ナナ)],
    [qw(8 八 ハチ)],
    [qw(9 九 キュウ)],
  ) {
    $self->add_misc_prefix ("第$_->[1]", "ダイ$_->[2]");
    $self->add_misc_prefix
        ("第$_->[0]", "ダイ$_->[2]", "第$_->[1]");
  }
} # add_predefined

sub write_csv ($) {
  my $self = shift;
  my $csv_f = $self->csv_f;
  $csv_f->dir->mkpath;
  print { $csv_f->openw } encode 'utf-8',
      join '', map { "$_\n" } map {
        join ',',
            $_->{surface},
            $_->{left_id} || 0, $_->{right_id} || 0, $_->{cost} || 0,
            $_->{pos}, $_->{category1} // '*', $_->{category2} // '*',
            $_->{category3} // '*', $_->{inflect} // '*',
            $_->{inflect_type} // '*',
            $_->{original} // $_->{surface},
            $_->{yomi} // $_->{original} // $_->{surface},
            $_->{pronounce} // $_->{yomi} // $_->{original} // $_->{surface};
      } @{$self->{entries} or []};
} # write_csv

sub mecab_root_d ($;$) {
  if (@_ > 1) {
    $_[0]->{mecab_root_d} = $_[1];
  }
  return $_[0]->{mecab_root_d};
} # mecab_root_d

sub mecab_dict_index_f ($) {
  return $_[0]->mecab_root_d->file ('libexec', 'mecab', 'mecab-dict-index');
} # mecab_dict_index_f

sub mecab_dic_d ($) {
  return $_[0]->mecab_root_d->file ('lib', 'mecab', 'dic', 'ipadic');
} # mecab_dic_d

sub mecab_charset ($) {
  return 'utf-8';
} # mecab_charset

sub write_user_dict ($) {
  my $self = shift;

  $self->dic_f->dir->mkpath;
  my $result = system
      $self->mecab_dict_index_f->stringify,
      '-d' => $self->mecab_dic_d->stringify,
      '-u' => $self->dic_f->stringify,
      '-f' => $self->mecab_charset,
      '-t' => $self->mecab_charset,
      $self->csv_f->stringify;
  unless ($result == 0) {
    if ($? == -1) {
      die "failed to execute: $!\n";
    } elsif ($? & 127) {
      die sprintf "child died with signal %d, %s coredump\n",
          ($? & 127),  ($? & 128) ? 'with' : 'without';
    } else {
      die sprintf "child exited with value %d\n", $? >> 8;
    }
  }
} # write_user_dict

1;
