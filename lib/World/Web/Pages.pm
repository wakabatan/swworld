package World::Web::Pages;
use strict;
use warnings;
use AnyEvent;
use Web::URL::Canonicalize qw(url_to_canon_url);

sub process_pages ($$$$) {
  my ($class, $app, $path, $dbreg) = @_;

  if (not defined $path->[1]) {
    # /pages

    #
  } elsif ($path->[1] =~ /\A[0-9]+\z/) {
    require World::Loader::PageData;
    my $loader = World::Loader::PageData->new_from_dbreg ($dbreg);
    if (not defined $path->[2]) {
      # /pages/{page_id}
      $app->requires_editable;
      $loader->load_page ($path->[1]);
      my $page = $loader->page
          or $app->throw_error (404, reason_phrase => 'Page not found');
      $app->process_temma
          (['pages.item.html.tm'],
           {page => $page});
      return $app->throw;
    } elsif ($path->[2] eq 'edit' and not defined $path->[3]) {
      # /pages/{page_id}/edit
      $app->requires_editable;
      $loader->load_page ($path->[1], allow_hidden => 1);
      my $page = $loader->page
          or $app->throw_error (404, reason_phrase => 'Page not found');
      $loader->load_words;
      $loader->load_page_data;
      $loader->load_related_pages;
      $app->process_temma
          (['pages.edit.html.tm'],
           {page => $page, loader => $loader});
      return $app->throw;
    } # /pages/{page_id}
  } elsif ($path->[1] eq 'add' and not defined $path->[2]) {
    # /pages/add
    $app->requires_editable;
    if ($app->http->request_method eq 'POST') {
      my $word = $app->text_param ('wikipedia_page');
      my $action;
      my $cv;
      if (defined $word and length $word) {
        require World::Action::RegisterWikipediaPage;
        $action = World::Action::RegisterWikipediaPage->new_from_dbreg_and_config
            ($dbreg, $app->config);
        $action->user_dic_f ($app->user_dic_f);
        $cv = $action->register_by_word_as_cv ($word);
      } else {
        require World::Action::RegisterPage;
        $action = World::Action::RegisterPage->new_from_dbreg_and_config
            ($dbreg, $app->config);
        $action->user_dic_f ($app->user_dic_f);
        my $url = url_to_canon_url $app->text_param ('url');
        $cv = AE::cv;
        $action->register_page_by_url_as_cv ($url)->cb (sub {
          $cv->send ($_[0]->recv);
        });
      }
      $cv->cb (sub {
        if ($_[0]->recv) {
          $app->send_redirect ($action->page->edit_path);
        } else {
          $app->send_error
              (400, reason_phrase => "Can't read the page (@{[$action->error // '?']})");
        }
      });
      return $app->throw;
    } else { # GET
      $app->process_temma (['pages.add.html.tm'],
                           {url => $app->text_param ('url')});
      return $app->throw;
    }
  } elsif ($path->[1] eq 'ksj' and not defined $path->[2]) {
    # /pages/ksj
    $app->requires_editable;
    require World::Loader::KSJItems;
    my $loader = World::Loader::KSJItems->new_from_dbreg ($dbreg);
    my $q = $app->text_param ('q') // '';
    my $items = $loader->search_by_word ($q);
    $app->send_json
        ({q => $q, items => $items->map (sub { $_->as_jsonalizable })});
    return $app->throw;

  } elsif ($path->[1] eq 'crawling') {
    if (not defined $path->[2]) {
      # /pages/crawling
      $app->requires_editable;
      if ($app->http->request_method eq 'POST') {
        my $act = $app->bare_param ('action') || '';
        if ($act eq 'add') {
          require World::Action::ScheduleCrawling;
          my $action = World::Action::ScheduleCrawling->new_from_dbreg ($dbreg);
          $action->add_urls_as_cv ($app->text_param_list ('url'))->cb (sub {
            if ($_[0]->recv) {
              $app->throw_redirect ('/pages/crawling');
            } else {
              $app->send_error (400, reason_phrase => 'Bad |url|');
            }
          });
          return $app->throw;
        } else {
          return $app->throw_error (400, reason_phrase => 'Bad |action|');
        }
      } else { # GET
        require World::Loader::CrawlingPages;
        my $loader = World::Loader::CrawlingPages->new_from_dbreg ($dbreg);
        $loader->page ($app->bare_param ('page'));
        $loader->load_crawling_targets;
        $loader->load_pages;
        $app->process_temma
            (['pages.crawling.list.html.tm'], {loader => $loader});
        return $app->throw;
      }
    } elsif ($path->[2] eq 'admin' and not defined $path->[3]) {
      # /pages/crawling/admin
      if ($app->http->request_method eq 'POST') {
        require World::Action::EditCrawlingConfig;
        my $action = World::Action::EditCrawlingConfig->new_from_dbreg ($dbreg);
        $action->update_disallow_patterns_as_cv
            ($app->text_param ('disallow_patterns'))->cb (sub {
          if ($_[0]->recv) {
            $app->send_redirect ('/pages/crawling/admin?');
          } else {
            $app->send_error (400);
          } 
        });
        return $app->throw;
      } else {
        require World::Loader::CrawlingAdmin;
        my $loader = World::Loader::CrawlingAdmin->new_from_dbreg ($dbreg);
        $loader->load_config;
        $app->process_temma
            (['pages.crawling.admin.html.tm'], {loader => $loader});
        return $app->throw;
      }
    }
  } # /pages/...

  return $app->throw_error (404);
} # process_pages

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
