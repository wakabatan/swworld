package World::Web::Words;
use strict;
use warnings;
use URL::PercentEncode qw(percent_encode_c);

sub process_words ($$$$) {
  my ($class, $app, $path, $dbreg) = @_;

  if (not defined $path->[1]) {
    # /words
    $app->requires_editable;
    require World::Loader::Words;
    my $loader = World::Loader::Words->new_from_dbreg ($dbreg);
    my $words = $loader->get_all_words;
    $app->process_temma (['words.html.tm'], {words => $words});
    return $app->throw;
  } elsif ($path->[1] =~ /\A[0-9]+\z/ and not defined $path->[2]) {
    # /words/{word_id}
    $app->requires_editable;
    require World::Loader::PagesByWord;
    my $loader = World::Loader::PagesByWord->new_from_dbreg ($dbreg);
    my $pages = $loader->get_by_word_id ($path->[1]);
    $loader->load_word;
    require World::Loader::WordData;
    my $loader2 = World::Loader::WordData->new_from_dbreg ($dbreg);
    $loader2->set_word ($loader->word);
    $loader2->load_spots;
    $app->process_temma (['words.item.html.tm'],
                         {pages => $pages,
                          word => $loader2->word,
                          spots => $loader2->spots});
    return $app->throw;
  } elsif ($path->[1] eq 'dict') {
    $app->requires_editable;
    if (not defined $path->[2]) {
      # /words/dict
      if ($app->http->request_method eq 'POST') {
        my $act = $app->bare_param ('action') || '';
        if ($act eq 'add') {
          require World::Action::EditWordDict;
          my $loader = World::Action::EditWordDict->new_from_dbreg ($dbreg);
          my $text = $app->text_param ('text');
          $loader->add_word_as_cv
              ($text, $app->bare_param ('type'),
               canon_text => $app->text_param ('canon_text'),
               reading => $app->text_param ('reading'),
               canon_reading => $app->text_param ('canon_reading'))->cb (sub {
            if ($_[0]->recv) {
              $app->send_redirect ('/words/dict?q=' . percent_encode_c $text);
            } else {
              $app->send_error (400, reason_phrase => 'Failed');
            }
          });
          return $app->throw;
        } elsif ($act eq 'rebuild') {
          require World::Action::BuildDictionary;
          my $dict_builder = World::Action::BuildDictionary
              ->new_from_csv_f_and_dic_f
                  ($app->user_dic_csv_f, $app->user_dic_f);
          $dict_builder->mecab_root_d ($app->mecab_root_d);
          $dict_builder->add_from_dbreg ($dbreg);
          $dict_builder->add_predefined;
          $dict_builder->write_csv;
          $dict_builder->write_user_dict;
          return $app->throw_error (204, reason_phrase => 'Done');
        } else {
          return $app->throw_error (404, reason_phrase => 'Bad |action|');
        }
      } else {
        require World::Loader::DictWords;
        my $loader = World::Loader::DictWords->new_from_dbreg ($dbreg);
        $loader->search_word ($app->text_param ('q'));
        $loader->load_dict_words;
        $app->process_temma (['words.dict.html.tm'], {loader => $loader});
        return $app->throw;
      }
    } elsif ($path->[2] =~ /\A[0-9]+\z/ and not defined $path->[3]) {
      # /words/dict/{dwid}
      $app->requires_request_method ({POST => 1});
      require World::Action::EditWordDict;
      my $loader = World::Action::EditWordDict->new_from_dbreg ($dbreg);
      my $text = $app->text_param ('text');
      $loader->edit_word_as_cv
          ($path->[2],
           text => $text,
           canon_text => $app->text_param ('canon_text'),
           reading => $app->text_param ('reading'),
           canon_reading => $app->text_param ('canon_reading'),
           type => $app->bare_param ('type'))->cb (sub {
        if ($_[0]->recv) {
          $app->send_redirect ('/words/dict?q=' . percent_encode_c $text);
        } else {
          $app->send_error (400, reason_phrase => 'Failed');
        }
      });
      return $app->throw;
    }
  } # /words/...

  return $app->throw_error (404);
} # process_words

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
