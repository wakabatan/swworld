package World::Web::Spots;
use strict;
use warnings;
use World::Defs::GeoObjects;
use World::Loader::GeoObjects;

sub process_spots ($$$$) {
  my ($class, $app, $path, $dbreg) = @_;

  if (not defined $path->[1]) {

  } elsif ($path->[1] =~ /\A[0-9]+\z/) {
    require World::Loader::Spot;
    my $loader = World::Loader::Spot->new_from_dbreg_and_spot_id
        ($dbreg, $path->[1]);
    $loader->load_spot;
    my $spot = $loader->spot
        or $app->throw_error (404, reason_phrase => 'Spot not found');

    if (not defined $path->[2]) {
      # /spots/{spot_id}
      $loader->load_spot_names;
      $loader->load_words;
      $loader->load_parent_spots;
      $loader->load_child_spots;
      $loader->load_pages;
      $loader->load_related_spots;
      $loader->load_wikipedia;
      $loader->load_ksj_item;
      my $go_loader = World::Loader::GeoObjects->new_from_dbreg ($dbreg);
      $app->process_temma
          (['spots.item.html.tm'],
           {spot => $spot, loader => $loader,
            go_loader => $go_loader, is_public => 1});
      return $app->throw;
    } elsif ($path->[2] eq 'edit') {
      $app->requires_editable;
      if (not defined $path->[3]) {
        # /spots/{spot_id}/edit
        if ($app->http->request_method eq 'POST') {
          require World::Action::EditSpot;
          my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
              ($dbreg, $spot, $app->config);
          $action->edit_spot_data
              (name => $app->text_param ('name'),
               reading => $app->text_param ('reading'),
               roman => $app->text_param ('roman'),
               location => $app->text_param ('location'),
               desc => $app->text_param ('desc'),
               lat => $app->bare_param ('lat'),
               lon => $app->bare_param ('lon'));
          $action->touch_as_cv->cb (sub {
            $app->send_redirect ($spot->edit_path . '?');
          });
          return $app->throw;
        } else {
          $loader->load_spot_names;
          $loader->load_words;
          $loader->load_wikipedia;
          $loader->load_parent_spots;
          $loader->load_ksj_item;
          my $go_loader = World::Loader::GeoObjects->new_from_dbreg ($dbreg);
          $app->process_temma
              (['spots.edit.html.tm'], {spot => $spot, loader => $loader,
                                        go_loader => $go_loader});
          return $app->throw;
        }
      } elsif ($path->[3] eq 'names' and not defined $path->[4]) {
        # /spots/{spot_id}/edit/names
        $app->requires_request_method ({POST => 1});
        my $act = $app->bare_param ('action') || '';
        if ($act eq 'add') {
          require World::Action::EditSpot;
          my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
              ($dbreg, $spot, $app->config);
          $action->add_spot_name_as_cv
              ($app->text_param ('name'),
               $app->text_param ('reading'),
               $app->text_param ('roman'))->cb (sub {
            if ($_[0]->recv) {
              $action->touch_as_cv->cb (sub {
                $app->send_redirect ($spot->edit_path . '?');
              });
            } else {
              $app->send_error (400);
            }
          });
          return $app->throw;
        } elsif ($act eq 'delete') {
          require World::Action::EditSpot;
          my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
              ($dbreg, $spot, $app->config);
          $action->delete_spot_name_by_sha1_as_cv
              ($app->bare_param ('spot_name_sha1'))->cb (sub {
            if ($_[0]->recv) {
              $action->touch_as_cv->cb (sub {
                $app->send_redirect ($spot->edit_path . '?');
              });
            } else {
              $app->send_error (400);
            }
          });
          return $app->throw;
        } else {
          return $app->throw_error (400, reason_phrase => 'Bad |action|');
        }
      } elsif ($path->[3] eq 'pages' and not defined $path->[4]) {
        # /spots/{spot_id}/edit/pages
        if ($app->http->request_method eq 'POST') {
          require World::Action::EditSpot;
          my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
              ($dbreg, $spot, $app->config);
          $action->set_page_weight_by_page_id_as_cv
              ($app->bare_param ('page_id') =>
               $app->bare_param ('weight') || 0)->cb (sub {
            if ($_[0]->recv) {
              $action->touch_as_cv->cb (sub {
                $app->send_redirect ($spot->path);
              });
            } else {
              $app->send_error (400, reason_phrase => 'Bad |page_id|');
            }
          });
          return $app->throw;
        } else {
          $loader->load_spot_pages;
          $loader->load_ksj_item;
          $app->process_temma
              (['spots.edit.pages.html.tm'],
               {spot => $spot, loader => $loader});
          return $app->throw;
        }
      } elsif ($path->[3] eq 'gos' and not defined $path->[4]) {
        # /spots/{spot_id}/edit/gos
        my $go_type = $app->bare_param ('go_type') // '';
        my $loader = World::Loader::GeoObjects->new_from_dbreg ($dbreg);
        $loader->load_by_type_as_cv ($go_type)->cb (sub {
          return $app->send_error (404, reason_phrase => "Unknown |go_type| |$go_type|") unless $_[0]->recv;
          require World::Action::EditSpot;
          my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
              ($dbreg, $spot, $app->config);
          $action->set_go_link_as_cv ($go_type, $app->bare_param ('go_id'))->cb (sub {
            $action->touch_as_cv->cb (sub {
              $app->send_redirect ($spot->edit_path . '#gos');
            });
          });
        });
        return $app->throw;
      }
    } elsif ($path->[2] eq 'words' and not defined $path->[3]) {
      # /spots/{spot_id}/words
      $app->requires_editable;
      $app->requires_request_method ({POST => 1});
      require World::Action::EditSpot;
      my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
          ($dbreg, $spot, $app->config);
      $action->edit_words
          ([grep { length }
            map { s/\s+/ /g; s/^ //; s/ $//; $_ }
            split /[\x0D\x0A]+/, $app->text_param ('words')]);
      $action->touch_as_cv->cb (sub {
        $app->send_redirect ($spot->edit_path . '?');
      });
      return $app->throw;
    } elsif ($path->[2] eq 'parents' and not defined $path->[3]) {
      # /spots/{spot_id}/parents
      $app->requires_editable;
      $app->requires_request_method ({POST => 1});
      require World::Action::EditSpot;
      my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
          ($dbreg, $spot, $app->config);
      $action->add_parent_spot_by_id_as_cv
          ($app->bare_param ('spot_id'))->cb (sub {
        if ($_[0]->recv) {
          $action->touch_as_cv->cb (sub {
            $app->send_redirect ($spot->edit_path . '?');
          });
        } else {
          $app->send_error (400, reason_phrase => 'Bad |spot_id|');
        }
      });
      return $app->throw;
    } elsif ($path->[2] eq 'wikipedia.ja' and not defined $path->[3]) {
      # /spots/{spot_id}/wikipedia.ja
      $app->requires_editable;
      $app->requires_request_method ({POST => 1});
      require World::Action::EditSpot;
      my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
          ($dbreg, $spot, $app->config);
      $action->user_dic_f ($app->user_dic_f);
      $action->set_wikipedia_ja_as_cv ($app->text_param ('name'))->cb (sub {
        if ($_[0]->recv) {
          $action->touch_as_cv->cb (sub {
            $app->send_redirect ($spot->edit_path . '?');
          });
        } else {
          $app->send_error (400, reason_phrase => "Can't set Wikipedia page");
        }
      });
      return $app->throw;
    } elsif ($path->[2] eq 'ksj' and not defined $path->[3]) {
      # /spots/{spot_id}/ksj
      $app->requires_editable;
      $app->requires_request_method ({POST => 1});
      require World::Action::EditSpot;
      my $action = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
          ($dbreg, $spot, $app->config);
      $action->set_ksj_as_cv ($app->bare_param ('ksj_type'),
                              $app->bare_param ('ksj_code'))->cb (sub {
        if ($_[0]->recv) {
          $app->send_redirect ($spot->edit_path . '?');
        } else {
          $app->send_error (400, reason_phrase => "Can't set ksj");
        }
      });
      return $app->throw;
    } # /spots/{spot_id}
  } elsif ($path->[1] eq 'create' and not defined $path->[2]) {
    # /spots/create
    $app->requires_editable;
    if ($app->http->request_method eq 'POST') {
      require World::Action::CreateSpot;
      my $action = World::Action::CreateSpot->new_from_dbreg ($dbreg);
      $action->create_spot (name => $app->text_param ('name'));
      my $words = [grep { length }
                   map { s/\s+/ /g; s/^ //; s/ $//; $_ }
                   split /[\x0D\x0A]+/, $app->text_param ('words')];
      if (@$words) {
        require World::Action::EditSpot;
        my $act = World::Action::EditSpot->new_from_dbreg_and_spot_and_config
            ($dbreg, $action->spot, $app->config);
        $act->edit_words ($words);
      }
      $app->throw_redirect ($action->spot->edit_path . '?');
    } else {
      $app->process_temma (['spots.create.html.tm']);
      return $app->throw;
    }
  } elsif (($path->[1] eq 'search' or $path->[1] eq 'search.json') and
           not defined $path->[2]) {
    # /spots/search[.json]
    require World::Loader::SearchSpots;
    my $loader = World::Loader::SearchSpots->new_from_dbreg ($dbreg);
    my $q = $app->text_param ('q');
    my $spots = $loader->search_by_word ($q);
    if ($path->[1] eq 'search.json') {
      $app->send_json ({q => $q,
                        items => $spots->map (sub { $_->as_jsonalizable })});
      return $app->throw;
    } else {
      $app->process_temma (['spots.search.html.tm'],
                           {q => $q, spots => $spots, is_public => 1});
      return $app->throw;
    }

  } elsif ($path->[1] eq 'gos.json' and not defined $path->[2]) {
    ## /spots/gos.json
    my $defs = $World::Defs::GeoObjects::DefByID;
    $app->send_json ({gos => [map {
      +{
        id => $_,
        name => $defs->{$_}->{name},
        path => qq</spots/$_>,
        search_json_path => qq</spots/$_/search.json>,
      };
    } keys %$defs]});
    return $app->throw;

  } elsif ($path->[1] =~ /\A[a-z-]+\z/) {
    if (@$path == 2) {
      # /spots/{object_type}
      my $loader = World::Loader::GeoObjects->new_from_dbreg ($dbreg);
      $loader->load_by_type_as_cv ($path->[1])->cb (sub {
        return $app->send_error (404) unless $_[0]->recv;
        $app->process_temma
            (['spots.object.html.tm'],
             {loader => $loader,
              go_def => $loader->get_go_def_by_type ($path->[1]),
              is_public => 1});
      });
      return $app->throw;
    } elsif (@$path == 3 and $path->[2] eq 'search.json') {
      # /spots/{object_type}/search.json
      my $loader = World::Loader::GeoObjects->new_from_dbreg ($dbreg);
      $loader->load_by_type_as_cv ($path->[1])->cb (sub {
        return $app->send_error (404) unless $_[0]->recv;
        my $q = $app->text_param ('q') // '';
        $loader->search_by_type_and_word_as_cv ($path->[1], $q)->cb (sub {
          return $app->send_json ({q => $q, items => $_[0]->recv});
        });
      });
      return $app->throw;
    } elsif (@$path == 3 and $path->[2] =~ /\A[1-9][0-9]*\z/) {
      # /spots/{object_type}/{object_id}
      my $loader = World::Loader::GeoObjects->new_from_dbreg ($dbreg);
      $loader->load_by_type_as_cv ($path->[1])->cb (sub {
        return $app->send_error (404) unless $_[0]->recv;
        my $go = $loader->get_go_by_type_and_id ($path->[1], $path->[2])
            or return $app->send_error (404, reason_phrase => 'Object not found');
        $loader->load_spot_id_by_type_and_id_as_cv ($path->[1], $path->[2])->cb (sub {
          my $spot_id = $_[0]->recv;
          if (defined $spot_id) {
            require World::Loader::Spot;
            my $loader = World::Loader::Spot->new_from_dbreg_and_spot_id
                ($dbreg, $spot_id);
            $loader->load_spot;
            my $spot = $loader->spot
                or $app->throw_error (404, reason_phrase => 'Spot not found');
            $app->send_redirect ($spot->path);
          } else {
            $app->process_temma
                (['spots.object.item.html.tm'],
                 {loader => $loader,
                  go_def => $loader->get_go_def_by_type ($path->[1]),
                  go => $go,
                  is_public => 1});
          }
        });
      });
      return $app->throw;
    }
  } # /spots/...

  return $app->throw_error (404);
} # process_spots

1;

=head1 LICENSE

Copyright 2013-2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
