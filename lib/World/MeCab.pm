package World::MeCab;
use strict;
use Encode;
use Text::MeCab;
use Path::Class;
use warnings;

sub new ($) {
  return bless {}, $_[0];
} # new

sub user_dic_f ($;$) {
  if (@_ > 1) {
    $_[0]->{user_dic_f} = $_[1];
  }
  return $_[0]->{user_dic_f};
} # user_dic_f

sub mecab ($) {
  my $params = {};
  $params->{userdic} = $_[0]->user_dic_f->stringify;
  delete $params->{userdic} unless -f $params->{userdic};
  return $_[0]->{mecab} ||= Text::MeCab->new ($params);
} # mecab

sub mecab_charset ($) {
  return 'utf-8';
} # mecab_charset

sub parse_text ($$) {
  my $mecab = $_[0]->mecab;
  my $charset = $_[0]->mecab_charset;
  my @result;
  for (my $node = $mecab->parse ($_[1]); $node; $node = $node->next) {
    my $text = decode $charset, $node->surface;
    my $info = decode $charset, $node->feature;
    push @result, {text => $text, info => [split /,/, $info]};
  }
  return \@result;
} # parse_text

1;
