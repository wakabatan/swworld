package World::YouTube;
use strict;
use warnings;
use AnyEvent;
use Web::UserAgent::Functions qw(http_get);
use JSON::PS;
use Web::DateTime::Parser;

sub new_from_config_and_video_id ($$$) {
  return bless {config => $_[1], video_id => $_[2]}, $_[0];
} # new_from_config_and_video_id

sub get_video_data_as_cv ($) {
  my $self = $_[0];
  my $cv = AE::cv;
  
  http_get
      url => q<https://www.googleapis.com/youtube/v3/videos>,
      params => {
        id => $self->{video_id},
        part => 'id,snippet,contentDetails',
        key => $self->{config}->get_file_base64_text ('youtube_key'), # XXX blocking
      },
      anyevent => 1,
      cb => sub {
        my (undef, $res) = @_;
        if ($res->is_success) {
          my $json = json_bytes2perl $res->content;
          if (defined $json and ref $json eq 'HASH' and
              defined $json->{items} and ref $json->{items} eq 'ARRAY') {
            my %video = map { $_->{id} => $_  } grep { ref $_ eq 'HASH' and defined $_->{id} } @{$json->{items}};
            if (defined $video{$self->{video_id}} and
                ref $video{$self->{video_id}} eq 'HASH') {
              my $video = $video{$self->{video_id}};
              my $data = {id => $video{id}};
              if (defined $video->{snippet} and ref $video->{snippet} eq 'HASH') {
                $data->{title} = $video->{snippet}->{title};
                $data->{content} = $video->{snippet}->{description};
                my $dt = Web::DateTime::Parser->new->parse_w3c_dtf_string
                    ($video->{snippet}->{publishedAt});
                $data->{published} = $dt->to_unix_number if defined $dt;
              }
              if (defined $video->{contentDetails} and ref $video->{contentDetails} eq 'HASH') {
                my $dur = Web::DateTime::Parser->new->parse_iso8601_duration_string
                    ($video->{contentDetails}->{duration});
                $data->{seconds} = $dur->seconds if defined $dur;
              }
              return $cv->send ($data);
            }
          }
        }
        return $cv->send (undef);
      };
  return $cv;
} # get_video_data_as_cv

1;

=head1 LICENSE

Copyright 2013-2015 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
