package World::Wikimedia;
use strict;
use warnings;
use AnyEvent;
use URL::PercentEncode;
use Web::UserAgent::Functions qw(http_get);
use Web::DOM::Document;
use Web::URL::Canonicalize qw(url_to_canon_url);
use Web::HTML::Parser;
use Wanage::HTTP::MIMEType;

sub new ($) {
  return bless {}, $_[0];
} # new

sub get_wikipedia_ja_url ($$) {
  my $word = $_[1];
  return url_to_canon_url
      (q<https://ja.wikipedia.org/wiki/> . percent_encode_c $word);
} # get_wikipedia_ja_url

sub get_wikipedia_ja_text_by_word_as_cv ($$) {
  my $word = $_[1];
  my $cv = AE::cv;
  my $url = sprintf q<https://ja.wikipedia.org/w/index.php?title=%s&action=edit>,
      percent_encode_c $word;
  http_get
      url => $url,
      timeout => 100,
      anyevent => 1,
      cb => sub {
        my (undef, $res) = @_;
        unless ($res->is_success) {
          $cv->send (undef);
          return;
        }

        my $ct = Wanage::HTTP::MIMEType->new_from_content_type
            ($res->header ('Content-Type'));
        my $parser = Web::HTML::Parser->new;
        my $doc = new Web::DOM::Document;
        $parser->parse_byte_string ($ct->params->{charset}, $res->content => $doc);
        my $ta = $doc->query_selector ('textarea');
        $cv->send (defined $ta ? $ta->default_value : undef);
      };
  return $cv;
} # get_wikipedia_ja_text_as_cv

sub get_wikipedia_ja_image_data_by_as_cv ($$) {
  my $data = $_[1];
  my $cv = AE::cv;
  my $file_name = $data->{file_name};
  $file_name =~ tr/ /_/;
  my $url = q<https://ja.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB:>
      . percent_encode_c $file_name;
  http_get
      url => $url,
      timeout => 100,
      anyevent => 1,
      cb => sub {
        my (undef, $res) = @_;
        unless ($res->is_success) {
          $cv->send (undef);
          return;
        }

        my $ct = Wanage::HTTP::MIMEType->new_from_content_type
            ($res->header ('Content-Type'));
        my $parser = Web::HTML::Parser->new;
        my $doc = new Web::DOM::Document;
        $doc->manakai_set_url ($url);
        $parser->parse_byte_string ($ct->params->{charset}, $res->content => $doc);
        my $ta = $doc->query_selector ('#file > a');
        $cv->send ({url => $ta ? $ta->href : undef});
      };
  return $cv;
} # get_wikipedia_ja_image_data_by_as_cv

sub get_image_url_by_size ($$;$) {
  my (undef, $url, $size) = @_;
  return $url unless $size;

  return $url if $url =~ /\.svg$/;
  return $url unless $url =~ m{^https?://upload.wikimedia.org/wikipedia/commons/([^/]+)/([^/]+)/([^/]+)};

  return qq{https://upload.wikimedia.org/wikipedia/commons/thumb/$1/$2/$3/${size}px-$3};
} # get_image_url_by_size

1;

