package World::MySQL;
use strict;
use warnings;
use Path::Class;
use JSON::Functions::XS qw(file2perl);
use Dongry::Database;
use Dongry::Type::JSON;

sub load_by_env ($) {
  my $class = shift;
  my $file_name = $ENV{MYSQL_DSNS_JSON}
      or die "|MYSQL_DSNS_JSON| is not specified";
  return $class->create_dbreg_by_dsns_json_f (file ($file_name));
} # load_by_env

sub create_dbreg_by_dsns_json_f ($$) {
  my $json = file2perl $_[1];
  my $dbreg = Dongry::Database->create_registry;
  $dbreg->{Registry}->{page} = {
    sources => {
      master => {
        dsn => $json->{dsns}->{page},
        writable => 1,
      },
      default => {
        dsn => $json->{dsns}->{page},
      },
    },
    schema => {
      page => {
        primary_keys => ['page_id'],
        type => {
          url => 'text',
          title => 'text',
          summary => 'text',
        },
      },
      page_data => {
        primary_keys => ['page_id'],
        type => {
          parsed_data => 'json',
        },
      },
      page_word => {
        primary_keys => ['page_id', 'word_id'],
      },
      wikipedia => {
        primary_keys => ['page_id'],
        type => {
          name => 'text',
          reading => 'text',
          parsed_data => 'json',
        },
      },
      wikipedia_image => {
        primary_keys => ['file_name'],
        type => {
          file_name => 'text',
          url => 'text',
        },
      },
      external_data => {
        primary_keys => ['type', 'code'],
        type => {
          name => 'text',
          data => 'json',
        },
      },
      crawling_target => {
        primary_keys => ['ctid'],
        type => {
          url => 'text',
        },
      },
      crawling_config => {
        primary_keys => ['id'],
        type => {
          disallow_patterns => 'text',
        },
      },
    },
  }; # page
  $dbreg->{Registry}->{word} = {
    sources => {
      master => {
        dsn => $json->{dsns}->{word},
        writable => 1,
      },
      default => {
        dsn => $json->{dsns}->{word},
      },
    },
    schema => {
      word => {
        primary_keys => ['word_id'],
        type => {
          text => 'text',
        },
      },
      dict_word => {
        primary_keys => ['dwid'],
        type => {
          text => 'text',
          canon_text => 'text',
          reading => 'text',
          canon_reading => 'text',
        },
      },
    },
  }; # page
  $dbreg->{Registry}->{spot} = {
    sources => {
      master => {
        dsn => $json->{dsns}->{spot},
        writable => 1,
      },
      default => {
        dsn => $json->{dsns}->{spot},
      },
    },
    schema => {
      spot => {
        primary_keys => ['spot_id'],
        type => {
          name => 'text',
          reading => 'text',
          roman => 'text',
          location => 'text',
          wikipedia_ja => 'text',
        },
      },
      spot_name => {
        primary_keys => ['spot_name_sha1'],
        type => {
          name => 'text',
          reading => 'text',
          roman => 'text',
        },
      },
      spot_word => {
        primary_keys => ['spot_id', 'word_id'],
      },
      spot_link => {
        primary_keys => ['parent_spot_id', 'child_spot_id'],
      },
    },
  }; # spot
  return $dbreg;
} # create_dbreg_by_dsns_json_f

1;
