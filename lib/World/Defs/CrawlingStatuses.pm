package World::Defs::CrawlingStatuses;
use strict;
use warnings;
use Exporter::Lite;

our @EXPORT;

push @EXPORT, qw(
  CRAWLING_STATUS_INITIAL CRAWLING_STATUS_DONE CRAWLING_STATUS_FAILED
);

sub CRAWLING_STATUS_INITIAL () { 0 }
sub CRAWLING_STATUS_DONE () { 1 }
sub CRAWLING_STATUS_FAILED () { 2 }

1;
