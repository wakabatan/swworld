package World::Defs::GeoObjects;
use strict;
use warnings;
use utf8;

our $DefByID = {
  macroregions => {
    id => 'macroregions', file_name => 'macroregions.json',
    name => '地域',
    keys => {name => ['ja_name', 'en_name']},
    get_ids => sub { [keys %{$_[0]->{areas}}] },
    get_get_go_by_id => sub { my $data = $_[0]; sub { $data->{areas}->{$_[0]} }},
    map_zoom => 1,
    main_props => [
      {
        key => 'code',
        name => 'UN M.49 符号',
      },
    ],
  },
  countries => {
    id => 'countries', file_name => 'countries.json',
    name => '国',
    keys => {
      name => ['ja_name', 'en_name'],
      short_name => ['ja_short_name', 'en_short_name'],
    },
    get_ids => sub { [keys %{$_[0]->{areas}}] },
    get_get_go_by_id => sub { my $data = $_[0]; sub { $data->{areas}->{$_[0]} }},
    main_props => [
      {
        key => 'code',
        name => 'ISO 3166-1 2文字国符号',
      },
      {
        key => 'code3',
        name => 'ISO 3166-1 3文字国符号',
      },
      {
        key => 'iso3166_numeric',
        name => 'ISO 3166-1 3桁国符号',
      },
      {
        key => 'stanag',
        name => 'GEC 国符号',
      },
      {
        key => 'stanag',
        name => 'STANAG 国符号',
      },
    ],
    link_props => [
      {
        key => 'wref_ja',
        name => 'Wikipedia (日)',
        wikipedia => 'ja',
      },
      {
        key => 'wref_en',
        name => 'Wikipedia (英)',
        wikipedia => 'en',
      },
      {
        key => 'mofa_area_url',
        name => '外務省各国・地域情勢',
      },
      {
        key => 'mofa_anzen_url',
        name => '外務省海外安全情報',
      },
      {
        key => 'world_factbook_url',
        name => 'The World Factbook',
      },
    ],
  },
  'jp-regions' => {
    id => 'jp-regions', file_name => 'jp-regions-full-flatten.json',
    name => '日本の地方自治体',
    keys => {
      name => ['name'],
      kana_name => ['kana'],
      en_name => ['latin'],
      position => ['position', 'office'],
    },
    get_ids => sub { [keys %{$_[0]->{regions}}] },
    get_get_go_by_id => sub { my $data = $_[0]; sub { $data->{regions}->{$_[0]} }},
    main_props => [
      {
        key => 'code',
        name => '全国地方公共団体コード',
      },
    ],
    link_props => [
      {
        key => 'wref',
        name => 'Wikipedia',
        wikipedia => 'ja',
      },
      {
        key => 'url',
        name => 'Web サイト',
      },
    ],
  },
}; # $DefByID

1;
