package World::Defs::WordTypes;
use strict;
use warnings;
use utf8;
use Exporter::Lite;

our @EXPORT = qw(
  WORD_TYPE_GEO_PROPER_NOUN WORD_TYPE_GEO_SUFFIX WORD_TYPE_MISC_PROPER_NOUN
  WORD_TYPE_MISC_NOUN WORD_TYPE_MISC_PREFIX WORD_TYPE_MISC_SUFFIX
  WORD_TYPE_ADJECTIVE_NOUN WORD_TYPE_DOABLE_NOUN WORD_TYPE_SYMBOL
  WORD_TYPE_PERSON_NAME WORD_TYPE_ORGANIZATION WORD_TYPE_BUILDING
  WORD_TYPE_AREA WORD_TYPE_GEO_NATURAL
);

sub WORD_TYPE_GEO_PROPER_NOUN    () {  1 } # 地名
sub WORD_TYPE_GEO_SUFFIX         () {  2 } # 地名接尾辞
sub WORD_TYPE_AREA               () {  3 } # 地域名
sub WORD_TYPE_GEO_NATURAL        () {  4 } # 自然地名
sub WORD_TYPE_BUILDING           () {  5 } # 建造物名
sub WORD_TYPE_ORGANIZATION       () {  6 } # 組織名
sub WORD_TYPE_PERSON_NAME        () {  7 } # 人名
sub WORD_TYPE_MISC_PROPER_NOUN   () { 29 } # その他固有名詞
sub WORD_TYPE_ADJECTIVE_NOUN     () { 30 } # 形容動詞
sub WORD_TYPE_DOABLE_NOUN        () { 31 } # サ変動詞
sub WORD_TYPE_MISC_NOUN          () { 50 } # その他一般名詞
sub WORD_TYPE_MISC_PREFIX        () { 51 } # その他接頭辞
sub WORD_TYPE_MISC_SUFFIX        () { 52 } # その他接尾辞
sub WORD_TYPE_SYMBOL             () { 70 } # 記号

our $TypeToLabel = {
  WORD_TYPE_GEO_PROPER_NOUN , '地名',
  WORD_TYPE_GEO_SUFFIX      , '地名接尾辞',
  WORD_TYPE_MISC_PROPER_NOUN, 'その他固有名詞',
  WORD_TYPE_MISC_NOUN       , 'その他一般名詞',
  WORD_TYPE_MISC_PREFIX     , 'その他接頭辞',
  WORD_TYPE_MISC_SUFFIX     , 'その他接尾辞',
  WORD_TYPE_ADJECTIVE_NOUN  , '形容動詞',
  WORD_TYPE_DOABLE_NOUN     , 'サ変動詞',
  WORD_TYPE_SYMBOL          , '記号',
  WORD_TYPE_PERSON_NAME     , '人名',
  WORD_TYPE_ORGANIZATION    , '組織名',
  WORD_TYPE_BUILDING        , '建造物名',
  WORD_TYPE_AREA            , '地域名',
  WORD_TYPE_GEO_NATURAL     , '自然地名',
}; # $TypeToLabel

1;
