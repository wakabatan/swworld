package World::Defs::KSJ;
use strict;
use warnings;
use Exporter::Lite;

our @EXPORT;

# external_data.type
push our @EXPORT, qw(KSJ_TYPE_DAM);
sub KSJ_TYPE_DAM () { 1 }

our $TypeToString = {
  KSJ_TYPE_DAM, 'dam',
};

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
