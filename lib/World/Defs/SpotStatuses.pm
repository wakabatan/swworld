package World::Defs::SpotStatuses;
use strict;
use warnings;
use Exporter::Lite;

our @EXPORT = qw(
  SPOT_STATUS_DEFAULT
);

sub SPOT_STATUS_DEFAULT () { 0 }

1;
