package World::Object::Spot;
use strict;
use warnings;
use List::Rubyish;
use URL::PercentEncode;

sub new_from_row ($$) {
  return bless {spot_row => $_[1]}, $_[0];
} # new_form_row

sub new_from_spot_id ($$) {
  return bless {spot_id => $_[1]}, $_[0];
} # new_from_spot_id

sub spot_id ($) {
  return $_[0]->{spot_id} // $_[0]->{spot_row}->get ('spot_id');
} # spot_id

sub spot_row ($) {
  return $_[0]->{spot_row};
} # spot_row

sub name ($) {
  return $_[0]->{spot_row}->get ('name');
} # name

sub reading ($) {
  my $t = $_[0]->{spot_row}->get ('reading');
  return defined $t && length $t ? $t : undef;
} # reading

sub roman ($) {
  my $t = $_[0]->{spot_row}->get ('roman');
  return defined $t && length $t ? $t : undef;
} # roman

sub set_spot_name_rows ($$) {
  $_[0]->{spot_name_rows} = $_[1];
} # set_spot_name_rows

sub additional_names ($) {
  return List::Ish->new unless $_[0]->{spot_name_rows};
  return $_[0]->{additional_names} ||= $_[0]->{spot_name_rows}
      ->map (sub {
          my $reading = $_->get ('reading');
          my $roman = $_->get ('roman');
          return {name => $_->get ('name'),
                  reading => length $reading ? $reading : undef,
                  roman => length $roman ? $roman : undef,
                  spot_name_sha1 => $_->get ('spot_name_sha1')};
        });
} # additional_names

sub location ($) {
  return $_[0]->{spot_row}->get ('location');
} # location

sub desc ($) {
  return $_[0]->{spot_row}->get ('desc');
} # desc

sub has_latlon ($) {
  my $row = $_[0]->{spot_row};
  return !!($row->get ('lat') or $row->get ('lon'));
} # has_latlon

sub lat ($) {
  my $row = $_[0]->{spot_row};
  return undef unless $row->get ('lat') or $row->get ('lon');
  return $row->get ('lat');
} # lat

sub lon ($) {
  my $row = $_[0]->{spot_row};
  return undef unless $row->get ('lat') or $row->get ('lon');
  return $row->get ('lon');
} # lon

sub has_wikipedia ($) {
  return defined $_[0]->wikipedia_ja_page;
} # has_wikipedia

sub wikipedia_ja_page ($) {
  my $page = $_[0]->{spot_row}->get ('wikipedia_ja');
  return length $page ? $page : undef;
} # wikipedia_ja_page

sub wikipedia_url ($) {
  my $page = $_[0]->wikipedia_ja_page;
  return undef unless defined $page;
  require World::Wikimedia;
  my $wm = World::Wikimedia->new;
  return $wm->get_wikipedia_ja_url ($page);
} # wikipedia_url

sub get_wikipedia_section_url ($$) {
  my $base = $_[0]->wikipedia_url or return undef;
  my $fragment = percent_encode_c $_[1];
  $fragment =~ s/\./%2E/g;
  $fragment =~ s/_/%5F/g;
  $fragment =~ s/%20/_/g;
  $fragment =~ tr/%/./;
  return $base . '#' . $fragment;
} # get_wikipedia_section_url

sub ksj_type ($) {
  return $_[0]->{spot_row}->get ('ksj_type') || undef;
} # ksj_type

sub ksj_code ($) {
  return $_[0]->{spot_row}->get ('ksj_code') || undef;
} # ksj_code

sub set_ksj_item ($$) {
  $_[0]->{ksj_item} = $_[1];
} # set_ksj_item

sub ksj_item ($) {
  return $_[0]->{ksj_item}; # or undef
} # ksj_item

sub add_go_link ($$) {
  $_[0]->{go_links}->{$_[1]->{go_type}} = $_[1]->{go_id};
} # add_go_link

sub go_links ($) {
  return {%{$_[0]->{go_links} or {}}};
} # go_links

sub set_words ($$) {
  $_[0]->{words} = $_[1];
} # set_words

sub words ($) {
  return $_[0]->{words};
} # words

sub set_wikipedia_rows ($%) {
  my ($self, %args) = @_;
  $self->{wikipedia_row}->{ja} = $args{ja};
  $self->{wikipedia_row}->{image} = $args{image};
} # set_wikipedia_rows

sub wikipedia_summary ($) {
  my $row = $_[0]->{wikipedia_row}->{ja} or return undef;
  return $row->get ('parsed_data')->{summary}; # or undef
} # wikipedia_summary

sub wikipedia_headings ($) {
  my $row = $_[0]->{wikipedia_row}->{ja} or return undef;
  return $row->get ('parsed_data')->{headings}; # or undef
} # wikipedia_heading

sub wikipedia_image_url ($) {
  my $row = $_[0]->{wikipedia_row}->{image} or return undef;
  return $row->get ('url');
} # wikipedia_image_url

sub wikipedia_image_thumbnail_url ($) {
  return $_[0]->{wikipedia_image_thumbnail_url}
      if exists $_[0]->{wikipedia_image_thumbnail_url};
  my $url = $_[0]->wikipedia_image_url
      or return $_[0]->{wikipedia_image_thumbnail_url} = undef;
  my $row = $_[0]->{wikipedia_row}->{ja}
      or return $_[0]->{wikipedia_image_thumbnail_url} = $url;
  my $size = $row->get ('parsed_data')->{image}->{size};
  require World::Wikimedia;
  my $wm = World::Wikimedia->new;
  return $_[0]->{wikipedia_image_thumbnail_url}
      = $wm->get_image_url_by_size ($url, $size);
} # wikipedia_image_thumbnail_url

sub wikipedia_lat ($) {
  my $row = $_[0]->{wikipedia_row}->{ja} or return undef;
  return $row->get ('lat');
} # wikipedia_lat

sub wikipedia_lon ($) {
  my $row = $_[0]->{wikipedia_row}->{ja} or return undef;
  return $row->get ('lon');
} # wikipedia_lon

sub has_wikipedia_latlon ($) {
  return $_[0]->wikipedia_lat || $_[0]->wikipedia_lon;
} # has_wikipedia_latlon

sub google_word ($) {
  return $_[0]->name;
} # google_word

sub google_url ($) {
  return 'https://www.google.com/search?ie=UTF-8&q=' .
      percent_encode_c $_[0]->google_word;
} # google_url

sub google_image_url ($) {
  return 'https://www.google.com/search?ie=UTF-8&tbm=isch&q=' .
      percent_encode_c $_[0]->google_word;
} # google_image_url

sub path ($) {
  return '/spots/' . $_[0]->spot_id;
} # path

sub edit_path ($) {
  return '/spots/' . $_[0]->spot_id . '/edit';
} # edit_path

sub edit_names_path ($) {
  return '/spots/' . $_[0]->spot_id . '/edit/names';
} # edit_names_path

sub edit_words_path ($) {
  return '/spots/' . $_[0]->spot_id . '/words';
} # edit_words_path

sub edit_pages_path ($) {
  return '/spots/' . $_[0]->spot_id . '/edit/pages';
} # edit_pages_path

sub edit_parents_path ($) {
  return '/spots/' . $_[0]->spot_id . '/parents';
} # edit_parents_path

sub edit_wikipedia_ja_path ($) {
  return '/spots/' . $_[0]->spot_id . '/wikipedia.ja';
} # edit_wikipedia_ja_path

sub edit_ksj_path ($) {
  return '/spots/' . $_[0]->spot_id . '/ksj';
} # edit_ksj_path

sub edit_gos_path ($) {
  return '/spots/' . $_[0]->spot_id . '/edit/gos';
} # edit_gos_path

sub as_jsonalizable ($) {
  my $self = $_[0];
  return {spot_id => $self->spot_id,
          name => $self->name,
          lat => $self->lat,
          lon => $self->lon};
} # as_jsonalizable

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
