package World::Object::Word;
use strict;
use warnings;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub new_from_word_id ($$) {
  return bless {word_id => $_[1]}, $_[0];
} # new_from_word_id

sub word_id ($) {
  return $_[0]->{word_id} // $_[0]->{row}->get ('word_id');
} # word_id

sub text ($) {
  return $_[0]->{row}->get ('text');
} # text

sub path ($) {
  return '/words/' . $_[0]->word_id;
} # path

1;
