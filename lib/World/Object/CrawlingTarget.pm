package World::Object::CrawlingTarget;
use strict;
use warnings;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub ctid ($) {
  return $_[0]->{row}->get ('ctid');
} # ctid

sub url ($) {
  return $_[0]->{row}->get ('url');
} # url

sub status ($) {
  return $_[0]->{row}->get ('status');
} # status

sub created ($) {
  return $_[0]->{row}->get ('created');
} # created

sub updated ($) {
  return $_[0]->{row}->get ('updated');
} # updated

sub crawl_after ($) {
  return $_[0]->{row}->get ('crawl_after');
} # crawl_after

1;
