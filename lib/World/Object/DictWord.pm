package World::Object::DictWord;
use strict;
use warnings;
use World::Defs::WordTypes;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub dwid ($) {
  return $_[0]->{row}->get ('dwid');
} # dwid

sub text ($) {
  return $_[0]->{row}->get ('text');
} # text

sub reading ($) {
  return $_[0]->{row}->get ('reading');
} # reading

sub canon_text ($) {
  return $_[0]->{row}->get ('canon_text');
} # canon_text

sub canon_reading ($) {
  return $_[0]->{row}->get ('canon_reading');
} # canon_reading

sub type ($) {
  return $_[0]->{row}->get ('type');
} # type

sub type_as_label ($) {
  return $World::Defs::WordTypes::TypeToLabel->{$_[0]->type};
} # type_as_label

sub edit_path ($) {
  return '/words/dict/' . $_[0]->dwid;
} # edit_path

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
