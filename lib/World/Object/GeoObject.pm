package World::Object::GeoObject;
use strict;
use warnings;
use URL::PercentEncode;

sub new_from_def_and_id_and_data ($$$$) {
  return bless {go_def => $_[1], go_id => $_[2], data => $_[3]}, $_[0];
} # new_from_def_and_id_and_data

sub go_id ($) {
  return $_[0]->{go_id};
} # go_id

sub get_prop_value ($$) {
  my ($self, $prop_name) = @_;
  for (@{$self->{go_def}->{keys}->{$prop_name} or [$prop_name]}) {
    return $self->{data}->{$_} if defined $self->{data}->{$_};
  }
  return undef;
} # get_prop_value

sub _latlon ($) {
  return $_[0]->{_latlon} if exists $_[0]->{_latlon};
  my $latlon = $_[0]->get_prop_value ('position');
  return $_[0]->{_latlon} = undef unless defined $latlon;
  if (ref $latlon eq 'HASH') {
    return $_[0]->{_latlon} = $latlon->{position}; # or undef
  }
  return $_[0]->{_latlon} = $latlon;
} # _latlon

sub has_latlon ($) {
  return defined $_[0]->_latlon;
} # has_latlon

sub lat ($) {
  return $_[0]->_latlon->[0];
} # lat

sub lon ($) {
  return $_[0]->_latlon->[1];
} # lon

sub path ($) {
  my $self = $_[0];
  return sprintf q</spots/%s/%s>,
      $self->{go_def}->{id}, $self->{go_id};
} # path

sub sw_url ($) {
  return 'https://wiki.suikawiki.org/n/' . percent_encode_c $_[0]->get_prop_value ('name');
} # sw_url

1;
