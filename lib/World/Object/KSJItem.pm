package World::Object::KSJItem;
use strict;
use warnings;
use World::Defs::KSJ;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub type ($) {
  return $_[0]->{row}->get ('type');
} # type

sub type_as_string ($) {
  return $World::Defs::KSJ::TypeToString->{$_[0]->type}; # or undef
} # type_as_string

sub code ($) {
  return $_[0]->{row}->get ('code');
} # code

sub name ($) {
  return $_[0]->{row}->get ('name');
} # name

sub lat ($) {
  return $_[0]->{row}->get ('lat');
} # lat

sub lon ($) {
  return $_[0]->{row}->get ('lon');
} # lon

sub data ($) {
  return $_[0]->{row}->get ('data');
} # data

sub as_jsonalizable ($) {
  my $self = $_[0];
  return {type => $self->type, code => $self->code,
          name => $self->name};
} # as_jsonalizable

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
