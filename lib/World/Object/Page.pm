package World::Object::Page;
use strict;
use warnings;
use Web::URL::Canonicalize qw(parse_url);
use World::Defs::PageStatuses;

sub new_from_row ($$) {
  return bless {row => $_[1]}, $_[0];
} # new_from_row

sub new_from_page_id ($$) {
  return bless {page_id => $_[1]}, $_[0];
} # new_from_page_id

sub set_page_data_row ($$) {
  $_[0]->{page_data_row} = $_[1];
} # set_page_data_row

sub page_id ($) {
  return $_[0]->{page_id} // $_[0]->{row}->get ('page_id');
} # word_id

sub url ($) {
  return $_[0]->{row}->get ('url');
} # url

sub url_hostname ($) {
  return $_[0]->{url_hostname} ||= do {
    my $parsed = parse_url $_[0]->url;
    $parsed->{host};
  };
} # url_hostname

sub status ($) {
  return $_[0]->{row}->get ('status');
} # status

sub is_shown ($) {
  return $_[0]->status == PAGE_STATUS_NORMAL;
} # is_shown

sub title ($) {
  my $title = $_[0]->{row}->get ('title');
  return $title if defined $title and length $title;
  return undef;
} # title

sub any_title {
  return $_[0]->title // $_[0]->url;
} # any_title

sub summary ($) {
  my $summary = $_[0]->{row}->get ('summary');
  return $summary if defined $summary and length $summary;
  return undef;
} # summary

sub photo_count ($) {
  return $_[0]->{row}->get ('photo_count');
} # photo_count

sub map_count ($) {
  return $_[0]->{row}->get ('map_count');
} # map_count

sub video_count ($) {
  return $_[0]->{row}->get ('video_count');
} # video_count

sub timestamp ($) {
  return $_[0]->{row}->get ('timestamp') || undef;
} # timestamp

sub youtube_video_id ($) {
  my $self = $_[0];
  my $url = $self->url;
  if ($url =~ m{^https?://www\.youtube\.com/watch\?v=([^&%]+)(?:$|&)}) {
    return $1;
  } else {
    return undef;
  }
} # youtube_video_id

sub site_links ($) {
  my $row = $_[0]->{page_data_row} or return undef;
  return $row->get ('parsed_data')->{site_links}; # or undef
} # site_links

sub path ($) {
  return '/pages/' . $_[0]->page_id;
} # path

sub edit_path ($) {
  return '/pages/' . $_[0]->page_id . '/edit';
} # edit_path

1;
