package World::Web;
use strict;
use warnings;
use Wanage::HTTP;
use World::Warabe::App;
use Path::Class;
use Web::UserAgent::Functions qw(http_get);

my $css_d = file (__FILE__)->dir->parent->parent->resolve->subdir ('css');
my $js_d = file (__FILE__)->dir->parent->parent->resolve->subdir ('js');
my $fonts_d = file (__FILE__)->dir->parent->parent->resolve->subdir ('fonts');

$Wanage::HTTP::UseXForwardedScheme = 1;
$Wanage::HTTP::UseXForwardedHost = 1;

sub psgi_app ($$$) {
  my (undef, $dbreg, $config) = @_;
  return sub {
    my $http = Wanage::HTTP->new_from_psgi_env ($_[0]);
    my $app = World::Warabe::App->new_from_http ($http);
    $app->config ($config);

    # XXX accesslog
    warn sprintf "ACCESS: [%s] %s %s FROM %s %s\n",
        scalar gmtime,
        $app->http->request_method, $app->http->url->stringify,
        $app->http->client_ip_addr->as_text,
        $app->http->get_request_header ('User-Agent') // '';

    $app->http->set_response_header
        ('Strict-Transport-Security' => 'max-age=10886400; includeSubDomains; preload');

    return $http->send_response (onready => sub {
      $app->execute (sub {
        __PACKAGE__->process ($app, $dbreg);
      });
    });
  };
} # psgi_app

sub process ($$$$) {
  my ($class, $app, $dbreg) = @_;
  $app->requires_valid_content_length;
  $app->requires_mime_type;
  $app->requires_request_method;
  $app->requires_same_origin unless $app->http->request_method_is_safe;

  my $path = $app->path_segments;

  if ($path->[0] eq '' and not defined $path->[1]) {
    # /
    require World::Loader::RecentSpots;
    my $loader = World::Loader::RecentSpots->new_from_dbreg ($dbreg);
    $loader->load_spots;
    $app->process_temma (['index.html.tm'],
                         {loader => $loader, is_public => 1});
    return $app->throw;
  } elsif ($path->[0] eq 'spots') {
    require World::Web::Spots;
    World::Web::Spots->process_spots ($app, $path, $dbreg);
  } elsif ($path->[0] eq 'pages') {
    require World::Web::Pages;
    World::Web::Pages->process_pages ($app, $path, $dbreg);
  } elsif ($path->[0] eq 'words') {
    require World::Web::Words;
    World::Web::Words->process_words ($app, $path, $dbreg);
  } elsif ($path->[0] eq 'edit' and not defined $path->[1]) {
    $app->requires_editable;
    $app->process_temma (['edit.html.tm']);
    return $app->throw;
  } elsif ($path->[0] eq 'about' and not defined $path->[1]) {
    $app->process_temma (['about.html.tm'], {is_public => 1});
    return $app->throw;
  } elsif (@$path == 2 and $path->[0] eq 'wp-abstract') {
    # /wp-abstract/{word}
    my $host = $app->config->get_text ('wpserver_host');
    http_get
        url => qq{http://$host/ja/abstract},
        params => {name => $path->[1]},
        anyevent => 1,
        cb => sub {
          if ($_[1]->code == 200) {
            $app->http->set_response_header ('Content-Type' => 'text/plain; charset=utf-8');
            $app->http->send_response_body_as_ref (\($_[1]->content));
            $app->http->close_response_body;
          } else {
            $app->send_error (404);
          }
        };
    return $app->throw;
  }

  if ($path->[0] eq 'css' and
      defined $path->[1] and $path->[1] =~ /\A[0-9A-Za-z_-]+\.css\z/) {
    # /css/{file_name}.css
    my $f = $css_d->file ($path->[1]);
    if (-f $f) {
      my $http = $app->http;
      $http->response_mime_type->set_value ('text/css');
      $http->response_mime_type->set_param (charset => 'utf-8');
      $http->set_response_last_modified ($f->stat->mtime);
      $http->send_response_body_as_ref (\scalar $f->slurp);
      $http->close_response_body;
      $app->throw;
    }
  }

  if ($path->[0] eq 'js' and
      defined $path->[1] and $path->[1] =~ /\A[0-9A-Za-z_-]+\.js\z/) {
    # /js/{file_name}.js
    my $f = $js_d->file ($path->[1]);
    if (-f $f) {
      my $http = $app->http;
      $http->response_mime_type->set_value ('text/javascript');
      $http->response_mime_type->set_param (charset => 'utf-8');
      $http->set_response_last_modified ($f->stat->mtime);
      $http->send_response_body_as_ref (\scalar $f->slurp);
      $http->close_response_body;
      $app->throw;
    }
  }

  if ($path->[0] eq 'fonts' and
      defined $path->[1] and $path->[1] =~ /\A[0-9A-Za-z_-]+\.[a-z]{3,4}\z/) {
    # /fonts/{file_name}.{ext}
    my $f = $fonts_d->file ($path->[1]);
    if (-f $f) {
      my $http = $app->http;
      $http->response_mime_type->set_value ('application/octet-stream');
      $http->set_response_last_modified ($f->stat->mtime);
      $http->set_response_header ('Access-Control-Allow-Origin' => '*');
      $http->send_response_body_as_ref (\scalar $f->slurp);
      $http->close_response_body;
      $app->throw;
    }
  }

  return $app->throw_error (404);
} # process

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
