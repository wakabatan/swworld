package World::Process::ParseWikipedia;
use strict;
use warnings;
use utf8;
use Char::Normalize::FullwidthHalfwidth qw(normalize_width);

sub new_from_text {
  return bless {text => $_[1]}, $_[0];
} # new_from_text

sub extract_sections ($) {
  my $self = $_[0];
  my $summary = '';
  my @heading;
  for (split /\x0D?\x0A/, $self->{text}) {
    if (/^==(?!=)\s*(.+?)\s*(?!>=)==$/) {
      push @heading, $1;
    } elsif (not @heading) {
      $summary .= "\n" . $_;
    }
  }
  $self->{headings} = \@heading;
  $summary =~ s{\{\{(\w+)\n((?:\|.+\n)+)\}\}}{}g;
  $summary =~ s{\s+}{ }g;
  $summary =~ s{^ }{};
  $summary =~ s{ $}{};
  $self->{summary} = $summary;
} # extract_sections

sub headings ($) {
  return $_[0]->{headings};
} # headings

sub summary ($) {
  return $_[0]->{summary};
} # summary

sub extract_reading ($) {
  my $self = $_[0];
  if ($self->{text} =~ /\{\{DEFAULTSORT:(.+?)\}\}/) {
    $self->{reading} = $1;
  }
} # extract_reading

sub reading ($) {
  return $_[0]->{reading};
} # reading

sub extract_table_templates ($) {
  my $self = $_[0];
  my $text = $self->{text};
  while ($text =~ m{\{\{(?:Infobox\s*)?(\w+)\|?\n((?:[ \t]*(?>\|.+\n|.+?\|\n))+)}g) {
    my $name = $1;
    my $data = $2;
    my $d = {};
    while ($data =~ /^[ \t]*\|?[ \t]*(\S+?)[ \t]*=[ \t]*(.*?)\|?$/mg) {
      $d->{$1} = $2;
    }
    $self->{table_templates}->{$name} = $d;
  }
} # extract_table_templates

sub table_templates ($) {
  return $_[0]->{table_templates} || {};
} # table_templates

sub template_names_as_list ($) {
  return (qw(ダム 橋 駅情報 湖 河川), keys %{$_[0]->{table_templates}});
} # template_names_as_list

sub extract_latlon ($) {
  my $self = $_[0];
  for ($self->template_names_as_list) {
    my $tbl = $self->{table_templates}->{$_};
    if ($tbl and defined $tbl->{'経度'}) {
      $self->{lat} = $tbl->{'緯度'} + ($tbl->{'緯分'} || 0)/60 + ($tbl->{'緯秒'} || 0)/3600;
      $self->{lon} = $tbl->{'経度'} + ($tbl->{'経分'} || 0)/60 + ($tbl->{'経秒'} || 0)/3600;
      return;
    } elsif (defined $tbl->{'位置情報'} and
             $tbl->{'位置情報'} =~ m{\{(?:ウィキ座標2段度分秒|ウィキ座標度分秒)\|(\d+)\|(\d+)\|([\d.]+)\|([NS])\|(\d+)\|(\d+)\|([\d.]+)\|([EW])}) {
      $self->{lat} = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      $self->{lon} = ($8 eq 'W' ? -1 : 1) * ($5 + $6/60 + $7/3600);
      return;
    } elsif (defined $tbl->{'位置'} and
             $tbl->{'位置'} =~ m{\{ウィキ座標2段度分秒\|(\d+)\|(\d+)\|([\d.]+)\|([NS])\|(\d+)\|(\d+)\|([\d.]+)\|([EW])}) {
      $self->{lat} = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      $self->{lon} = ($8 eq 'W' ? -1 : 1) * ($5 + $6/60 + $7/3600);
      return;
    } elsif (defined $tbl->{'座標'} and
             $tbl->{'座標'} =~ m{\{(?:ウィキ座標2段度分秒|coor dms)\|(\d+)\|(\d+)\|([\d.]+)\|([NS])\|(\d+)\|(\d+)\|([\d.]+)\|([EW])}) {
      $self->{lat} = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      $self->{lon} = ($8 eq 'W' ? -1 : 1) * ($5 + $6/60 + $7/3600);
      return;
    } elsif (defined $tbl->{lat_deg} or defined $tbl->{latd}) {
      if (($tbl->{lat_deg} || $tbl->{latd}) =~ /^\s*(\d+)\s*\|\s*lat_?mi?n?\s*=\s*(\d+)\s*\|\s*lat_?se?c?\s*=\s*([\d.]+)\s*\|\s*lat(?:_dir|NS)\s*=\s*([NS])/) {
        $self->{lat} = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      }
      if (($tbl->{lon_deg} || $tbl->{longd}) =~ /^\s*(\d+)\s*\|\s*long?_?mi?n?\s*=\s*(\d+)\s*\|\s*long?_?se?c?\s*=\s*([\d.]+)\s*\|\s*long?(?:_dir|EW)\s*=\s*([EW])/) {
        $self->{lon} = ($4 eq 'W' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      }
      return;
    } elsif (defined $tbl->{'緯度度'}) {
      if ($tbl->{'緯度度'} =~ /^\s*(\d+)\s*\|\s*緯度分\s*=\s*(\d+)\s*\|\s*緯度秒\s*=\s*([\d.]+)\s*/) {
        $self->{lat} = ('' eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      }
      if ($tbl->{'経度度'} =~ /^\s*(\d+)\s*\|\s*経度分\s*=\s*(\d+)\s*\|\s*経度秒\s*=\s*([\d.]+)\s*/) {
        $self->{lon} = ('' eq 'W' ? -1 : 1) * ($1 + $2/60 + $3/3600);
      }
      return;
    }
  }
  if ($self->{text} =~ m{\{\{[Cc]oord\|(\d+)\|(\d+)\|([\d.]+)\|([NS])\|(\d+)\|(\d+)\|([\d.]+)\|([EW])\|.*?\}\}}) {
    $self->{lat} = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
    $self->{lon} = ($8 eq 'W' ? -1 : 1) * ($5 + $6/60 + $7/3600);
    return;
  } elsif ($self->{text} =~ m{\{\{日本の位置情報\|(\d+)\|(\d+)\|([\d.]+)\|(\d+)\|(\d+)\|([\d.]+)}) {
    $self->{lat} = ($1 + $2/60 + $3/3600);
    $self->{lon} = ($4 + $5/60 + $6/3600);
    return;
  } elsif ($self->{text} =~ m{\{\{[Cc]oord\|([\d.]+)\|([NS])\|([\d.]+)\|([EW])}) {
    $self->{lat} = ($2 eq 'S' ? -1 : 1) * $1;
    $self->{lon} = ($4 eq 'W' ? -1 : 1) * $3;
    return;
  } elsif ($self->{text} =~ m{\{\{[Cc]oord\|([-+\d.]+)\|([-+\d.]+)}) {
    $self->{lat} = $1;
    $self->{lon} = $2;
    return;
  } elsif ($self->{text} =~ m{\{\{mapplot\|([\d.]+)\|([\d.]+)\|.*?\}\}}) {
    $self->{lat} = $2;
    $self->{lon} = $1;
    return;
  } elsif ($self->{text} =~ m{\{\{ウィキ座標日本世界\|(\d+)\|(\d+)\|([\d.]+)\|([NS])\|(\d+)\|(\d+)\|([\d.]+)\|([EW]).*?\}\}}) {
    my $lat = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
    my $lon = ($8 eq 'W' ? -1 : 1) * ($5 + $6/60 + $7/3600);

    # <https://ja.wikipedia.org/wiki/Template:%E6%B8%AC%E5%9C%B0%E7%B3%BB%E5%A4%89%E6%8F%9B/%E6%97%A5%E6%9C%ACto%E4%B8%96%E7%95%8C/%E7%B7%AF%E5%BA%A6>
    $self->{lat} = ($lat * 0.99989305) + ($lon * 0.000017464) + 0.0046017;

    # <https://ja.wikipedia.org/wiki/Template:%E6%B8%AC%E5%9C%B0%E7%B3%BB%E5%A4%89%E6%8F%9B/%E4%B8%96%E7%95%8Cto%E6%97%A5%E6%9C%AC/%E7%B5%8C%E5%BA%A6>
    $self->{lon} = ($lon * 0.999916957) - ($lat * 0.000046038) + 0.010041;
    return;
  } elsif ($self->{text} =~ m{\{ウィキ座標2段度分秒\|(\d+)\|(\d+)\|([\d.]+)\|([NS])\|(\d+)\|(\d+)\|([\d.]+)\|([EW])}) {
    $self->{lat} = ($4 eq 'S' ? -1 : 1) * ($1 + $2/60 + $3/3600);
    $self->{lon} = ($8 eq 'W' ? -1 : 1) * ($5 + $6/60 + $7/3600);
    return;
  }
} # extract_latlon

sub lat ($) {
  return $_[0]->{lat};
} # lat

sub lon ($) {
  return $_[0]->{lon};
} # lon

sub remove_inline_markup ($$) {
  my $s = $_[1];
  $s =~ s{<!--.*?-->}{}gs;
  $s =~ s{'''(.*?)'''}{$1}gs;
  $s =~ s{\[\[([^:|]+?)\]\]}{$1}gs;
  $s =~ s{\[\[[^:|]*?\|([^|]+?)\]\]}{$1}gs;
  $s =~ s{\[\[\w+:.*?\]\]}{}gs;
  1 while $s =~ s{\{\{[^{}]*?\}\}}{}gs;
  1 while $s =~ s{\{\|[^{}|]*?\|\}}{}gs;
  $s =~ s{\[https?:\S+\s+([^\[\]]+)\]}{$1}g;
  $s =~ s{<ref[^<>]*>.*?</ref>}{}g;
  $s =~ s{<[a-z]+\s[^<>]+>}{}g;
  $s =~ s{^\s*\|\}\s*}{}g;
  $s =~ s{\s+}{ }g;
  $s =~ s{^ }{};
  $s =~ s{ $}{};
  return ${normalize_width \$s};
} # remove_inline_markup

sub extract_image ($) {
  my $self = $_[0];
  for ($self->template_names_as_list) {
    my $tbl = $self->{table_templates}->{$_};
    if (defined $tbl->{'画像'} and
        $tbl->{'画像'} =~ /\[\[(?:ファイル|画像|Image|File):(.+?)\]\]/) {
      $self->{image} = $self->parse_image_attrs ($1);
      return;
    } elsif (defined $tbl->{'画像'} and length $tbl->{'画像'}) {
      my $size;
      if (defined $tbl->{'pxl'} and $tbl->{'pxl'} =~ /(\d+)px/) {
        $size = $1;
      }
      $self->{image} = {type => 'wikimedia',
                        file_name => $tbl->{'画像'},
                        size => $size,
                        title => $tbl->{'画像説明'}};
      return;
    } elsif (defined $tbl->{'所在地'} and
             $tbl->{'所在地'} =~ /\[\[(?:ファイル|画像|Image|File):(.+)\]\]/) {
      $self->{image} = $self->parse_image_attrs ($1);
      return;
    }
  }

  if ($self->{text} =~ /\[\[(?:ファイル|画像|Image|File):(.+?)\]\]/) {
    $self->{image} = $self->parse_image_attrs ($1);
    return;
  }
} # extract_image

sub parse_image_attrs ($$) {
  my @data = split /\|/, $_[1];
  my $size = [map { /(\d+)px/ ? $1 : () } @data]->[0];
  @data = grep { not /^\d+px$/ } grep { not { map { $_ => 1 } qw(
    left center right
  ) }->{$_} } @data;
  return {type => 'wikimedia',
          file_name => $data[0],
          size => $size,
          title => @data > 1 ? $data[-1] : undef};
} # parse_image_attrs

sub image ($) {
  return $_[0]->{image};
} # image

1;
