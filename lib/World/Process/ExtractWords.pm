package World::Process::ExtractWords;
use strict;
use warnings;
use Encode;
use Char::Normalize::FullwidthHalfwidth qw(normalize_width);
use Web::DOM::Document;
use Web::DOM::Node;
use Web::HTML::Parser;

sub new ($) {
  return bless {}, $_[0];
} # new

sub mecab ($) {
  require World::MeCab;
  return $_[0]->{mecab} ||= World::MeCab->new;
} # mecab

sub parse_html ($$;$) {
  $_[0]->{document} = my $doc = new Web::DOM::Document;
  my $parser = Web::HTML::Parser->new;
  $parser->parse_byte_string ($_[2], $_[1] => $doc);
  my $scripts = [];
  for (@{$doc->query_selector_all ('style, script, noscript, noembed, noframes')}) {
    $_->parent_node->remove_child ($_);
    push @$scripts, $_;
  }
  $_[0]->{scripts} = $scripts;
  for (@{$_[0]->extract_paragraphs ($doc)}) {
    $_[0]->parse_text (join '', map { $_->node_value } @$_);
  }
} # parse_html

sub document ($) {
  return $_[0]->{document};
} # document

my $Blocks = {
  map { $_ => 1 } qw(
    title
    body section article aside nav header footer div main hgroup h1 h2 h3 h4 h5 h6
    p dt dd li td th blockquote menu hr marquee pre xmp textarea option datalist
    object iframe applet br noscript noembed noframes
  )
};

my $Skip = {
  map { $_ => 1 } qw(
    style script
  )
};

sub extract_paragraphs ($$) {
  my @node = ($_[1]->child_nodes->to_list);
  my @para;
  my $new_para = 1;
  while (@node) {
    my $node = shift @node;
    if (not ref $node) {
      $new_para = 1 if $Blocks->{$node};
      next;
    }
    my $nt = $node->node_type;
    if ($nt == TEXT_NODE) {
      if ($new_para) {
        push @para, [$node];
        $new_para = 0;
      } else {
        push @{$para[-1]}, $node;
      }
    } elsif ($nt == ELEMENT_NODE) {
      my $ln = $node->local_name; ## ignore namespaces
      next if $Skip->{$ln};
      $new_para = 1 if $Blocks->{$ln};
      unshift @node, $node->child_nodes->to_list, $ln;
    } else {
      unshift @node, $node->child_nodes->to_list;
    }
  } # while
  return \@para;
} # extract_paragraphs

sub parse_text ($$) {
  push @{$_[0]->{parsed} ||= []},
      @{$_[0]->mecab->parse_text (${normalize_width \($_[1])})};
} # parse_text

sub parsed ($) {
  return $_[0]->{parsed} || [];
} # parsed

sub extract_words ($) {
  my $parsed = $_[0]->parsed;

  my @found;
  my $prev;
  for my $data (reverse @$parsed) {
    unless (defined $data->{text}) {
      undef $prev;
      next;
    }
    use utf8;
    if ($prev) {
      if ((($data->{info}->[0] eq '名詞' and
            not {'形容動詞語幹' => 1, 'サ変接続' => 1,
                 '副詞可能' => 1}->{$data->{info}->[1]}) or
           $data->{info}->[0] eq '接頭辞' or
           {
             map { $_ => 1 } qw(溜め 旧 々 現 新)
           }->{$data->{text}}) and
          not ({map { $_ => 1 } qw(手前 回 旧道)}->{$data->{text}})) {
        $found[-1] = $data->{text} . $found[-1];
      } else {
        undef $prev;
      }
    } else {
      if (($data->{info}->[1] eq '固有名詞') or
          ($data->{info}->[1] eq '接尾' and $data->{info}->[2] eq '地域' and
           not {map { $_ => 1 } qw(方面)}->{$data->{text}}) or
          ($data->{info}->[1] eq '接尾' and $data->{info}->[2] eq '一般' and
           {
             map { $_ => 1 } qw(線 地)
           }->{$data->{text}}) or
          ($data->{info}->[0] eq '名詞' and {
             map { $_ => 1 } qw(橋 大橋 歩道 遊歩道 池 島 湿原 草原 高原 原
                                鉄道 航路 橋梁 吊り橋 林道 地区 酷道 谷 道路
                                国道 県道 市道 道 集落 軌道
                                ダム 号 号線 トンネル センター 公園
                                スカイライン 古墳 丁目 区間)
           }->{$data->{text}})) {
        push @found, $data->{text};
        $prev = $data;
      } else {
        undef $prev;
      }
    }
  }

  for (@found) {
    $_[0]->{words}->{$_}++;
  }
} # extract_words

sub words ($) {
  return $_[0]->{words} || {};
} # words

sub merge_words ($) {
  my $words1 = $_[0]->words;
  my $words2 = {%$words1};
  my $delete_words = [];
  for my $word1 (keys %$words1) {
    for my $word2 (keys %$words1) {
      next if $word2 eq $word1;
      if ($word2 =~ /\Q$word1\E/) {
        $words2->{$word2} += $words1->{$word1};
        push @$delete_words, $word1;
      }
    }
  }
  for (@$delete_words) {
    delete $words2->{$_};
  }
  $_[0]->{merged_words} = $words2;
} # merge_words

sub merged_words ($) {
  return $_[0]->{merged_words};
} # merged_words

1;
