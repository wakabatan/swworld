package World::Loader::Spot;
use strict;
use warnings;
use List::Ish;
use AnyEvent;
use World::Defs::PageStatuses;
use World::Object::Spot;
use World::Object::Word;
use World::Object::Page;
use World::Object::KSJItem;

sub new_from_dbreg_and_spot_id ($$$) {
  return bless {dbreg => $_[1], spot_id => $_[2]}, $_[0];
} # new_from_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub db ($) {
  return $_[0]->{db} ||= $_[0]->{dbreg}->load ('spot');
} # db

sub load_spot ($) {
  my $self = shift;
  my $db = $self->db;
  $self->{spot_row} = $db->table ('spot')->find ({spot_id => $self->{spot_id}});
} # load_spot

sub spot ($) {
  my $self = shift;
  return undef unless $self->{spot_row};
  return $self->{spot} ||= World::Object::Spot->new_from_row ($self->{spot_row});
} # spot

sub load_spot_names ($) {
  my $self = $_[0];
  my $db = $self->db;
  my $spot = $self->spot;
  my $rows = $db->table ('spot_name')->find_all
      ({spot_id => $spot->spot_id},
       fields => ['name', 'reading', 'roman', 'spot_name_sha1']);
  $spot->set_spot_name_rows ($rows);
} # load_spot_names

sub load_words ($) {
  my $self = shift;
  my $db = $self->db;
  my $word_ids = $db->select ('spot_word', {spot_id => $self->{spot_id}}, fields => 'word_id')->all->map (sub { $_->{word_id} });
  my $list;
  if (@$word_ids) {
    my $word_db = $self->dbreg->load ('word');
    $list = $word_db->table ('word')->find_all ({word_id => {-in => $word_ids}})
        ->map (sub { World::Object::Word->new_from_row ($_) });
  } else {
    $list = List::Ish->new;
  }
  $self->spot->set_words ($list);
} # load_words

sub load_pages ($) {
  my $spot_db = $_[0]->db;
  my $found_spot_ids = {};
  my $spot_ids = [grep { not $found_spot_ids->{$_}++ } map { $_->spot_id } $_[0]->spot, @{$_[0]->child_spots}];
  return unless @$spot_ids;

  my $spot_word_ids = {};
  my $word_ids = $spot_db->execute
      ('SELECT spot_id,word_id FROM spot_word WHERE spot_id IN (:spot_ids)',
       {spot_ids => $spot_ids})
      ->all->map (sub {
        push @{$spot_word_ids->{$_->{spot_id}} ||= []}, $_->{word_id};
        return $_->{word_id};
      });

  my $page_ids = List::Ish->new;
  my $page_db = $_[0]->dbreg->load ('page');
  my $page_word_id_to_frequency = {};
  my $word_page_ids = {};
  if (@$word_ids) {
    $page_ids = $page_db->execute
        ('SELECT word_id,page_id,frequency FROM page_word WHERE word_id IN (:word_id)',
         {word_id => [@$word_ids]})
            ->all->map (sub {
          $page_word_id_to_frequency->{$_->{page_id}, $_->{word_id}} += $_->{frequency}; 
          push @{$word_page_ids->{$_->{word_id}} ||= []}, $_->{page_id};
          $_->{page_id};
        });
  }

  my $spot_id_to_page_id_to_weight = {};
  $spot_db->select ('spot_page',
                    {spot_id => {-in => $spot_ids}},
                    fields => ['page_id', 'spot_id', 'weight'])
      ->all->each (sub {
    push @$page_ids, $_->{page_id};
    $spot_id_to_page_id_to_weight->{$_->{spot_id}}->{$_->{page_id}}
        = $_->{weight};
  });

  return unless @$page_ids;
  
  my $pages = $page_db->table ('page')->find_all
      ({page_id => {-in => $page_ids},
        status => PAGE_STATUS_NORMAL})
          ->map (sub { World::Object::Page->new_from_row ($_) })
          ->as_hashref_by_key (sub { $_->page_id });

  my $spot_id_to_pages = {};
  for my $spot_id (@$spot_ids) {
    $spot_id_to_pages->{$spot_id} = [map {
      $pages->{$_->[0]} || ();
    } sort { 
      $b->[2] <=> $a->[2];
    } map {
      my $word_id = $_;
      map {
        [$_, $word_id, $page_word_id_to_frequency->{$_, $word_id} || 0];
      } @{$word_page_ids->{$word_id} or []};
    } @{$spot_word_ids->{$spot_id} or []}];

    push @{$spot_id_to_pages->{$spot_id}}, map {
      $pages->{$_} || ();
    } keys %{$spot_id_to_page_id_to_weight->{$spot_id} or {}};

    my $seq_pages = {};
    $spot_id_to_pages->{$spot_id} = [sort {
      $b->[1] <=> $a->[1];
    } map {
      my $url = $_->url;
      $url =~ s{([0-9]+)([^0-9]*)$}{$2};
      $seq_pages->{$url}->{$1 || 0} = $_;
      [$_,
       $spot_id_to_page_id_to_weight->{$spot_id}->{$_->page_id} || 0,
       $seq_pages->{$url},
       $url];
    } grep {
      $spot_id_to_page_id_to_weight->{$spot_id}->{$_->page_id} >= PAGE_WEIGHT_SHOW_MIN;
    } @{$spot_id_to_pages->{$spot_id}}];
  } # $spot_id
  my $found_page_ids = {};
  for my $spot_id (@$spot_ids[1..$#$spot_ids], $spot_ids->[0]) {
    my $spot_found_page_ids = {};
    $spot_id_to_pages->{$spot_id} = List::Ish->new
        ([grep {
          (not $found_page_ids->{$_->[3]}++ or
           $spot_id_to_page_id_to_weight->{$spot_id}->{$_->[0]} >= PAGE_WEIGHT_SHOW_MIN) and
           not $spot_found_page_ids->{$_->[3]}++;
        } @{$spot_id_to_pages->{$spot_id}}]);
  }
  $_[0]->{pages_by_spot_id} = $spot_id_to_pages;
} # load_pages

sub get_items_by_spot_and_type ($$$) {
  my ($self, $spot, $type) = @_;

  my $category = 'all';
  if ($type =~ s/^([^-]+)-(.+)$/$1/) {
    $category = $2;
  }

  my $min = $type eq 'main'       ? PAGE_WEIGHT_NORMAL:
            $type eq 'additional' ? PAGE_WEIGHT_ADDITIONAL:
                                    PAGE_WEIGHT_ADDITIONAL;
  my $max = $type eq 'main'       ? PAGE_WEIGHT_IMPORTANT:
            $type eq 'additional' ? PAGE_WEIGHT_ADDITIONAL:
                                    PAGE_WEIGHT_IMPORTANT;
  my @item = grep {
    $min <= $_->[1] and $_->[1] <= $max;
  } @{$_[0]->{pages_by_spot_id}->{$_[1]->spot_id} || []};

  @item = map {
    my $p = $_;
    +{page => $p->[0], weight => $p->[1],
      seq_pages => [map { [$_ => $p->[2]->{$_}] }
                    sort { $a <=> $b }
                    keys %{$p->[2]}]};
  } @item;

  if ($category eq 'youtube') {
    @item = grep {
      defined $_->{page}->youtube_video_id;
    } @item;
  } elsif ($category eq 'other') {
    @item = grep {
      not defined $_->{page}->youtube_video_id;
    } @item;
  }

  return \@item;
} # get_items_by_spot_and_type

sub load_wikipedia ($) {
  my $self = $_[0];
  my $name = $_[0]->{spot_row}->get ('wikipedia_ja');
  return unless length $name;
  
  my $page_db = $_[0]->dbreg->load ('page');
  my $wp_row = $page_db->table ('wikipedia')->find ({lang => 'ja', name => $name})
      or return;

  my $image_row;
  my $image = $wp_row->get ('parsed_data')->{image};
  if ($image and $image->{file_name}) {
    $image_row = $page_db->table ('wikipedia_image')->find
        ({file_name => $image->{file_name}});
  }

  $self->spot->set_wikipedia_rows (ja => $wp_row, image => $image_row);
} # load_wikipedia

sub load_parent_spots ($) {
  my $spot_db = $_[0]->db;
  my $spot_ids = $spot_db->execute
      ('SELECT parent_spot_id FROM spot_link WHERE child_spot_id = ? ORDER BY weight DESC',
       {child_spot_id => $_[0]->spot->spot_id})
          ->all->map (sub { $_->{parent_spot_id} });
  return unless @$spot_ids;

  $_[0]->{parent_spots} = $spot_db->table ('spot')
      ->find_all ({spot_id => {-in => $spot_ids}})
      ->map (sub { World::Object::Spot->new_from_row ($_) });
} # load_parent_spots

sub parent_spots ($) {
  return $_[0]->{parent_spots} || List::Ish->new;
} # parent_spots

sub load_child_spots ($) {
  my $spot_db = $_[0]->db;
  my $spot_ids = $spot_db->execute
      ('SELECT child_spot_id FROM spot_link WHERE parent_spot_id = ? ORDER BY weight DESC',
       {parent_spot_id => $_[0]->spot->spot_id})
          ->all->map (sub { $_->{child_spot_id} });
  return unless @$spot_ids;

  $_[0]->{child_spots} = $spot_db->table ('spot')
      ->find_all ({spot_id => {-in => $spot_ids}})
      ->map (sub { World::Object::Spot->new_from_row ($_) });
} # load_child_spots

sub load_related_spots ($) {
  my $self = $_[0];
  return unless $self->{pages_by_spot_id};
  my $page_ids = [map { $_->[0]->page_id }
                  grep { $_->[1] >= PAGE_WEIGHT_NORMAL }
                  map { @{$_} }
                  values %{$self->{pages_by_spot_id}}];
  return unless @$page_ids;

  my $page_db = $self->dbreg->load ('page');
  my $word_ids = {};
  $page_db->select
      ('page_word',
       {page_id => {-in => $page_ids}},
       fields => 'word_id',
       distinct => 1)->all->each (sub { $word_ids->{$_->{word_id}}++ });
  return unless keys %$word_ids;

  my $spot_db = $self->db;
  my $spot_ids = {};
  $spot_db->select
      ('spot_word',
       {word_id => {-in => [keys %$word_ids]},
        spot_id => {-not => $self->spot->spot_id}},
       fields => ['spot_id', 'word_id'])->all->each (sub {
    $spot_ids->{$_->{spot_id}} += $word_ids->{$_->{word_id}};
  });
  return unless keys %$spot_ids;

  my $found_ids = {};
  $self->parent_spots->each (sub { $found_ids->{$_->spot_id} = 1 });
  $self->child_spots->each (sub { $found_ids->{$_->spot_id} = 1 });
  $spot_ids = [sort { $spot_ids->{$b} <=> $spot_ids->{$a} }
               grep { not $found_ids->{$_}++ }
               keys %$spot_ids];
  return unless @$spot_ids;

  $self->{related_spots} = $spot_db->table ('spot')
      ->find_all ({spot_id => {-in => $spot_ids}})
      ->map (sub { World::Object::Spot->new_from_row ($_) });
} # load_related_spots

sub load_spot_pages ($) {
  my $self = $_[0];

  my $spot_db = $self->db;
  my $page_ids = [];
  $spot_db->select ('spot_page', {spot_id => $self->spot->spot_id},
                    fields => ['page_id', 'weight'])->all->each (sub {
    push @$page_ids, $_->{page_id};
    $self->{page_id_to_weight}->{$_->{page_id}} = $_->{weight};
  });
  return unless @$page_ids;

  my $page_db = $self->dbreg->load ('page');
  $self->{spot_pages} = $page_db->table ('page')->find_all ({page_id => {-in => $page_ids}})->map (sub { World::Object::Page->new_from_row ($_) });
} # load_spot_pages

sub spot_pages ($) {
  return $_[0]->{spot_pages} || List::Ish->new;
} # spot_pages

sub load_ksj_item ($) {
  my $self = $_[0];
  my $spot = $self->spot;
  return unless $spot->ksj_type and $spot->ksj_code;
  my $page_db = $_[0]->dbreg->load ('page');
  my $row = $page_db->select
      ('external_data', {type => $spot->ksj_type,
                         code => $spot->ksj_code},
       limit => 1)->first_as_row or return;
  my $item = World::Object::KSJItem->new_from_row ($row);
  $spot->set_ksj_item ($item);
} # load_ksj_item

sub get_spot_page_weight ($$) {
  return $_[0]->{page_id_to_weight}->{$_[1]->page_id} || 0;
} # get_spot_page_weight

sub child_spots ($) {
  return $_[0]->{child_spots} || List::Ish->new;
} # child_spots

sub related_spots ($) {
  return $_[0]->{related_spots} || List::Ish->new;
} # related_spots

sub load_linked_go_data_as_cv ($) {
  my $self = shift;
  my $cv = AE::cv;

  my $spot = $self->spot;
  my $spot_db = $self->db;
  $spot_db->select ('spot_go_link', {spot_id => $spot->spot_id})->each (sub {
    $spot->add_go_link ($_);
  });

  AE::postpone { $cv->send (1) };
  return $cv;
} # load_linked_go_data_as_cv

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
