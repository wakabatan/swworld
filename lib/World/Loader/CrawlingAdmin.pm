package World::Loader::CrawlingAdmin;
use strict;
use warnings;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub load_config ($) {
  my $self = $_[0];
  my $db = $self->db;
  $self->{row} = $db->table ('crawling_config')->find ({id => 1});
} # load_config

sub disallow_patterns ($) {
  return $_[0]->{row} ? $_[0]->{row}->get ('disallow_patterns') : '';
} # disallow_patterns

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
