package World::Loader::DictWords;
use strict;
use warnings;
use List::Ish;
use World::Defs::WordTypes;
use World::Object::DictWord;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('word');
} # db

sub search_word ($$) {
  if (@_ > 1) {
    $_[0]->{search_word} = $_[1];
  }
  return $_[0]->{search_word};
} # search_word

sub load_dict_words ($) {
  my $self = $_[0];
  my $db = $self->db;

  my %where;
  if (defined $self->{search_word} and length $self->{search_word}) {
    $where{text} = {-infix => $self->{search_word}};
  }

  return unless keys %where;

  $self->{dict_words} = $db->table ('dict_word')->find_all (\%where)
      ->map (sub { World::Object::DictWord->new_from_row ($_) });
} # load_dict_words

sub dict_words ($) {
  return $_[0]->{dict_words} || List::Ish->new;
} # dict_words

sub avail_word_types ($) {
  my $defs = $World::Defs::WordTypes::TypeToLabel;
  return [map { {label => $defs->{$_}, value => $_} } sort { $a <=> $b } keys %$defs];
} # avail_word_types

1;
