package World::Loader::KSJItems;
use strict;
use warnings;
use List::Ish;
use World::Object::KSJItem;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub search_by_word ($$) {
  my ($self, $q) = @_;
  return List::Ish->new unless length $q;
  return $self->db->table ('external_data')
      ->find_all ({name => {-infix => $q}})
      ->map (sub { World::Object::KSJItem->new_from_row ($_) });
} # search_by_word

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
