package World::Loader::PageData;
use strict;
use warnings;
use List::Ish;
use World::Defs::PageStatuses;
use World::Object::Page;
use World::Object::Word;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub load_page ($$;%) {
  my ($self, $page_id, %args) = @_;
  my $page_db = $self->dbreg->load ('page');

  my %status;
  unless ($args{allow_hidden}) {
    $status{status} = PAGE_STATUS_NORMAL;
  }

  my $page_row = $page_db->table ('page')->find
      ({page_id => $page_id, %status}) or return;
  $self->{page} = World::Object::Page->new_from_row ($page_row);
} # load_page

sub load_page_data ($) {
  my $page_db = $_[0]->dbreg->load ('page');
  my $page = $_[0]->{page};
  my $row = $page_db->table ('page_data')->find ({page_id => $page->page_id});
  $page->set_page_data_row ($row) if $row;
} # load_page_data

sub load_related_pages ($) {
  my $self = $_[0];
  my $page = $self->{page};
  my $urls = [keys %{$page->site_links || {}}];
  return unless @$urls;
  my $page_db = $self->dbreg->load ('page');
  $page_db->table ('page')->find_all
      ({url => {-in => $urls}})
      ->each (sub {
        $self->{page_by_url}->{$_->get ('url')} = World::Object::Page->new_from_row ($_);
      });
} # load_related_pages

sub page ($) {
  return $_[0]->{page}; # or undef
} # page

sub get_page_by_url ($$) {
  return $_[0]->{page_by_url}->{$_[1]};
} # get_page_by_url

sub load_words ($) {
  my $page_db = $_[0]->dbreg->load ('page');
  my $wids = $page_db->select ('page_word', {page_id => $_[0]->page->page_id},
                               fields => ['word_id', 'frequency'])->all;
  return unless @$wids;

  my $word_db = $_[0]->dbreg->load ('word');
  my $words = $word_db->table ('word')
      ->find_all ({word_id => {-in => $wids->map (sub { $_->{word_id} })}})
      ->map (sub { World::Object::Word->new_from_row ($_) });
  my $wid_to_count = {map { $_->{word_id} => $_->{frequency} } @$wids};
  $_[0]->{page_words} = $words->map (sub {
    return {word => $_, count => $wid_to_count->{$_->word_id}};
  });
} # load_words

sub page_words ($) {
  return $_[0]->{page_words} || List::Ish->new;
} # page_words

1;
