package World::Loader::RecentSpots;
use strict;
use warnings;
use World::Defs::SpotStatuses;
use World::Object::Spot;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('spot');
} # db

sub page_db ($) {
  return $_[0]->{dbreg}->load ('page');
} # page_db

sub load_spots ($) {
  my $self = $_[0];
  my $db = $self->db;
  $self->{spots} = $db->table ('spot')->find_all
      ({status => SPOT_STATUS_DEFAULT},
       order => ['updated', 'DESC'],
       limit => 30)
          ->map (sub { World::Object::Spot->new_from_row ($_) });
  return unless @{$self->{spots}};

  my $wp_names = $self->{spots}->map (sub { $_->wikipedia_ja_page // () });
  return unless @$wp_names;

  my $page_db = $self->page_db;
  my $wp_rows = $page_db->table ('wikipedia')->find_all
      ({lang => 'ja', name => {-in => $wp_names}});
  my $img_file_names = $wp_rows->map
      (sub { $_->get ('parsed_data')->{image}->{file_name} // () });
  my $wp_img_rows = {};
  if (@$img_file_names) {
    $wp_img_rows = $page_db->table ('wikipedia_image')->find_all
        ({file_name => {-in => $img_file_names}})
            ->as_hashref_by_key (sub { $_->get ('file_name') });
  }
  $wp_rows = $wp_rows->as_hashref_by_key (sub { $_->get ('name') });

  $self->{spots}->each (sub {
    my $wp_row = $wp_rows->{$_->wikipedia_ja_page} or return;
    my $wp_img_row = $wp_img_rows->{$wp_row->get ('parsed_data')->{image}->{file_name}}; # or undef
    $_->set_wikipedia_rows (ja => $wp_row, image => $wp_img_row);
  });
} # load_spots

sub spots ($) {
  return $_[0]->{spots};
} # spots

1;
