package World::Loader::PagesByWord;
use strict;
use warnings;
use List::Ish;
use World::Defs::PageStatuses;
use World::Object::Page;
use World::Object::Word;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub get_by_word ($$) {
  my $word_db = $_[0]->dbreg->load ('word');
  delete $_[0]->{word_id};
  $_[0]->{word_row} = my $word_row = $word_db->table ('word')->find
      ({text => $_[1]}, fields => 'word_id')
      or return List::Ish->new;
  $_[0]->{word_id} = my $word_id = $word_row->get ('word_id');

  my $page_db = $_[0]->dbreg->load ('page');
  my $page_ids = $page_db->table ('page_word')->find_all
      ({word_id => $word_id}, fields => 'page_id')
      ->map (sub { $_->get ('page_id') });
  return List::Ish->new unless @$page_ids;

  return $page_db->table ('page')->find_all ({page_id => {-in => $page_ids}})
      ->map (sub { World::Object::Page->new_from_row ($_) });
} # get_by_word

sub get_by_word_id ($$) {
  my $word_id = $_[1];
  my $page_db = $_[0]->dbreg->load ('page');
  my $page_ids = $page_db->table ('page_word')->find_all
      ({word_id => $word_id}, fields => 'page_id')
      ->map (sub { $_->get ('page_id') });
  $_[0]->{word_id} = $word_id;
  delete $_[0]->{word_row};
  return List::Ish->new unless @$page_ids;

  return $page_db->table ('page')->find_all
      ({page_id => {-in => $page_ids}, status => PAGE_STATUS_NORMAL})
      ->map (sub { World::Object::Page->new_from_row ($_) });
} # get_by_word_id

sub load_word ($) {
  return unless $_[0]->{word_id};
  $_[0]->{word_row} ||= $_[0]->dbreg->load ('word')->table ('word')->find
      ({word_id => $_[0]->{word_id}});
} # load_word

sub word ($) {
  my $word_id = $_[0]->{word_id} or return undef;
  return $_[0]->{word} ||=
      $_[0]->{word_row}
          ? World::Object::Word->new_from_row ($_[0]->{word_row})
          : World::Object::Word->new_from_word_id ($word_id);
} # word

1;
