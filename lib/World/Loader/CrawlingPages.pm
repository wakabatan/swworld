package World::Loader::CrawlingPages;
use strict;
use warnings;
use World::Defs::PageStatuses;
use World::Object::CrawlingTarget;
use World::Object::Page;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('page');
} # db

sub page ($;$) {
  if (@_ > 1) {
    $_[0]->{page} = $_[1];
  }
  return $_[0]->{page} || 1;
} # page

sub per_page ($;$) {
  if (@_ > 1) {
    $_[0]->{per_page} = $_[1];
  }
  return $_[0]->{per_page} || 200;
} # per_page

sub load_crawling_targets ($) {
  my $db = $_[0]->db;
  $_[0]->{crawling_targets} = $db->table ('crawling_target')->find_all
      ({ctid => {'!=', 0}},
       order => ['crawl_after' => 'DESC'],
       offset => ($_[0]->page - 1) * $_[0]->per_page,
       limit => $_[0]->per_page)
          ->map (sub { World::Object::CrawlingTarget->new_from_row ($_) });
} # load_crawling_targets

sub crawling_targets ($) {
  return $_[0]->{crawling_targets};
} # crawling_targets

sub load_pages ($) {
  my $self = $_[0];
  my $db = $self->db;
  my $targets = $self->crawling_targets;
  return unless @$targets;
  $db->table ('page')->find_all
      ({url => {-in => $targets->map (sub { $_->url })},
        status => PAGE_STATUS_NORMAL})
      ->each (sub {
        my $page = World::Object::Page->new_from_row ($_);
        $self->{pages_by_url}->{$page->url} = $page;
      });
} # load_pages

sub get_page_by_url ($$) {
  return $_[0]->{pages_by_url}->{$_[1]}; # or undef
} # get_page_by_url

1;
