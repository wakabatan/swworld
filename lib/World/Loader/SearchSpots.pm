package World::Loader::SearchSpots;
use strict;
use warnings;
use List::Ish;
use World::Object::Spot;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub db ($) {
  return $_[0]->{dbreg}->load ('spot');
} # db

sub max_items ($) {
  return 100;
} # max_items

sub search_by_word ($$) {
  my $word = $_[1];
  return List::Ish->new unless defined $word and length $word;

  my $db = $_[0]->db;
  return $db->table ('spot')->find_all ({name => {-infix => $word}}, limit => $_[0]->max_items)->map (sub { World::Object::Spot->new_from_row ($_) });
} # search_by_word

1;
