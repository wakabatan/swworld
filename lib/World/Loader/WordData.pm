package World::Loader::WordData;
use strict;
use warnings;
use List::Ish;
use World::Object::Spot;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_form_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub set_word ($$) {
  $_[0]->{word} = $_[1];
} # set_word

sub word ($) {
  return $_[0]->{word};
} # word

sub load_spots ($) {
  my $spot_db = $_[0]->dbreg->load ('spot');
  my $ids = $spot_db->execute ('SELECT spot_id FROM spot_word WHERE word_id = ?',
                               {word_id => $_[0]->word->word_id})->all
      ->map (sub { $_->{spot_id} });
  return unless @$ids;
  $_[0]->{spots} = $spot_db->table ('spot')->find_all ({spot_id => {-in => $ids}})
      ->map (sub { World::Object::Spot->new_from_row ($_) });
} # load_spots

sub spots ($) {
  return $_[0]->{spots} || List::Ish->new;
} # spots

1;
