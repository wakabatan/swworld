package World::Loader::GeoObjects;
use strict;
use warnings;
use Path::Tiny;
use JSON::PS;
use AnyEvent;
use World::Defs::GeoObjects;
use World::Object::GeoObject;

my $data_path = path (__FILE__)->parent->parent->parent->parent->child
    ('local/data');

my $JSONData = {};

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1], json_data => $JSONData}, $_[0];
} # new_from_dbreg

sub spot_db ($) {
  return $_[0]->{dbreg}->load ('spot');
} # spot_db

sub load_by_type_as_cv ($$) {
  my ($self, $go_type) = @_;
  my $cv = AE::cv;
  if (defined $self->{json_data}->{$go_type}) {
    AE::postpone { $cv->send (1) };
    return $cv;
  }
  my $go_def = $World::Defs::GeoObjects::DefByID->{$go_type};
  if (defined $go_def) {
    my $json_path = $data_path->child ($go_def->{file_name});
    my $json = json_bytes2perl $json_path->slurp; # blocking!
    $self->{json_data}->{$go_type} = $json;
  }
  AE::postpone {
    $cv->send (defined $go_def);
  };
  return $cv;
} # load_by_type_as_cv

sub get_go_def_by_type ($$) {
  my ($self, $go_type) = @_;
  return $World::Defs::GeoObjects::DefByID->{$go_type}
      || die "GO type |$go_type| is not defined";
} # get_go_def_by_type

sub get_member_go_ids_by_type ($$) {
  my ($self, $go_type) = @_;
  my $go_def = $World::Defs::GeoObjects::DefByID->{$go_type}
      or die "GO type |$go_type| is not defined";
  return $go_def->{get_ids}->($self->{json_data}->{$go_type});
} # get_member_go_ids_by_type

sub get_go_by_type_and_id ($$$) {
  my ($self, $go_type, $go_id) = @_;
  my $go_def = $World::Defs::GeoObjects::DefByID->{$go_type}
      or die "GO type |$go_type| is not defined";
  my $data = $go_def->{get_get_go_by_id}->($self->{json_data}->{$go_type})->($go_id)
      or return undef;
  return World::Object::GeoObject->new_from_def_and_id_and_data
      ($go_def, $go_id, $data);
} # get_go_by_type_and_id

sub search_by_type_and_word_as_cv ($$$) {
  my ($self, $go_type, $q) = @_;
  my $cv = AE::cv;
  my $go_def = $World::Defs::GeoObjects::DefByID->{$go_type}
      or die "GO type |$go_type| is not defined";
  $q = lc $q;

  AE::postpone {
    my $result = [];
    my $go_getter = $go_def->{get_get_go_by_id}->($self->{json_data}->{$go_type});
    GO: for my $id (@{$go_def->{get_ids}->($self->{json_data}->{$go_type})}) {
      my $data = $go_getter->($id);
      for my $key (@{$go_def->{keys}->{name} or []},
                   @{$go_def->{keys}->{short_name} or []}) {
        if (defined $data->{$key} and
            (index lc ($data->{$key}), $q) > -1) {
          my $obj = World::Object::GeoObject->new_from_def_and_id_and_data
              ($go_def, $id, $data);
          push @$result, {id => $id,
                          name => $obj->get_prop_value ('name'),
                          matched => $data->{$key},
                          lat => $obj->lat,
                          lon => $obj->lon,
                          path => $obj->path};
          next GO;
        }
      }
    } # GO
    $cv->send ($result);
  };

  return $cv;
} # search_by_type_and_word_as_cv

sub load_spot_id_by_type_and_id_as_cv ($$$) {
  my ($self, $type, $id) = @_;
  my $cv = AE::cv;

  my $db = $self->spot_db;
  my $link = $db->select ('spot_go_link', {go_type => $type, go_id => $id},
                          fields => ['spot_id'])->first;
  AE::postpone { $cv->send (defined $link ? $link->{spot_id} : undef) };

  return $cv;
} # load_spot_id_by_type_and_id_as_cv

1;

=head1 LICENSE

Copyright 2014 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
