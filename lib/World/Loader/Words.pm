package World::Loader::Words;
use strict;
use warnings;
use World::Object::Word;

sub new_from_dbreg ($$) {
  return bless {dbreg => $_[1]}, $_[0];
} # new_from_dbreg

sub dbreg ($) {
  return $_[0]->{dbreg};
} # dbreg

sub get_all_words ($) {
  my $word_db = $_[0]->dbreg->load ('word');
  return $word_db->table ('word')->find_all ({word_id => {'>', 0}})->map (sub {
    return World::Object::Word->new_from_row ($_);
  });
} # get_all_words

1;
