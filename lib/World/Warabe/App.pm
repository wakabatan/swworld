package World::Warabe::App;
use strict;
use warnings;
use Path::Class;
use Warabe::App;
use Warabe::App::Role::JSON;
push our @ISA, qw(Warabe::App Warabe::App::Role::JSON);
use Digest::SHA ();
use Temma;

my $templates_d = file (__FILE__)->dir->parent->parent->parent->resolve
    ->subdir ('templates');

my $dict_d = file (__FILE__)->dir->parent->parent->parent
    ->subdir ('local', 'dict');

my $mecab_root_d = dir (glob file (__FILE__)->dir->parent->parent->parent
    ->subdir ('local', 'mecab-*-utf-8'));

sub config ($;$) {
  if (@_ > 1) {
    $_[0]->{config} = $_[1];
  }
  return $_[0]->{config};
} # config

sub mecab_root_d ($) {
  return $mecab_root_d;
} # mecab_root_d

sub user_dic_csv_f ($) {
  return $dict_d->file ('words.csv');
} # user_dic_csv_f

sub user_dic_f ($) {
  return $dict_d->file ('words.dic');
} # user_dic_f

sub requires_editable ($) {
  my $app = $_[0];
  my $allowed = $app->config->get_file_json ('edit_basic_auth');
  my $http = $app->http;
  my $auth = $http->request_auth;
  if ($auth->{auth_scheme} and $auth->{auth_scheme} eq 'basic') {
    if (defined $allowed->{$auth->{userid}} and
        defined $auth->{password}) {
      my $pwd = Digest::SHA::sha512_hex ($auth->{password});
      if ($pwd eq $allowed->{$auth->{userid}}) {
        return;
      }
    }
  }

  $http->set_status (401);
  $http->set_response_auth ('basic', realm => 'Edit');
  $http->set_response_header
      ('Content-Type' => 'text/plain; charset=us-ascii');
  $http->send_response_body_as_ref (\'401 Authorization required');
  $http->close_response_body;
  $app->throw;
} # requires_editable

sub process_temma ($$$$) {
  my ($app, $template_path, $args) = @_;
  my $http = $app->http;
  $http->response_mime_type->set_value ('text/html');
  $http->response_mime_type->set_param (charset => 'utf-8');
  my $fh = Temma::Warabe::App::Printer->new_from_http ($http);
  $args->{app} = $app;
  Temma->process_html
      ($templates_d->file (@$template_path), $args => $fh,
       sub { undef $fh; $http->close_response_body });
} # process_temma

package Temma::Warabe::App::Printer;

sub new_from_http ($$) {
  return bless {http => $_[1], value => ''}, $_[0];
} # new_from_http

sub print ($$) {
  $_[0]->{value} .= $_[1];
  if (length $_[0]->{value} > 1024*10 or length $_[1] == 0) {
    $_[0]->{http}->send_response_body_as_text ($_[0]->{value});
    $_[0]->{value} = '';
  }
} # print

sub DESTROY {
  $_[0]->{http}->send_response_body_as_text ($_[0]->{value})
      if length $_[0]->{value};
} # DESTROY

1;

=head1 LICENSE

Copyright 2013 Wakaba <wakaba@suikawiki.org>.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
