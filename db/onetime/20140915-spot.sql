CREATE TABLE spot_go_link (
  spot_id BIGINT UNSIGNED NOT NULL,
  go_type VARBINARY(31) NOT NULL,
  go_id BIGINT UNSIGNED NOT NULL,
  created DOUBLE,
  PRIMARY KEY (spot_id, go_type),
  UNIQUE KEY (go_type, go_id),
  KEY (created)
) DEFAULT CHARSET=BINARY Engine=InnoDB;
