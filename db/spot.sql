CREATE TABLE spot (
  spot_id BIGINT UNSIGNED NOT NULL,
  name VARBINARY(1023) NOT NULL,
  reading VARBINARY(1023) NOT NULL,
  roman VARBINARY(1023) NOT NULL,
  lat DOUBLE NOT NULL,
  lon DOUBLE NOT NULL,
  location VARBINARY(1023) NOT NULL,
  `desc` BLOB NOT NULL,
  wikipedia_ja VARBINARY(1023) NOT NULL,
  ksj_type SMALLINT UNSIGNED NOT NULL,
  ksj_code INT UNSIGNED NOT NULL,
  status TINYINT UNSIGNED NOT NULL DEFAULT 0,
  created DOUBLE NOT NULL,
  updated DOUBLE NOT NULL,
  PRIMARY KEY (spot_id),
  KEY (lat, lon),
  KEY (lon, lat),
  KEY (created),
  KEY (updated)
) DEFAULT CHARSET=BINARY;

CREATE TABLE spot_name (
  spot_name_sha1 BINARY(40) NOT NULL,
  spot_id BIGINT UNSIGNED NOT NULL,
  name VARBINARY(1023) NOT NULL,
  reading VARBINARY(1023) NOT NULL,
  roman VARBINARY(1023) NOT NULL,
  created DOUBLE NOT NULL,
  PRIMARY KEY (spot_name_sha1),
  KEY (created)
) DEFAULT CHARSET=BINARY;

CREATE TABLE spot_word (
  spot_id BIGINT UNSIGNED NOT NULL,
  word_id BIGINT UNSIGNED NOT NULL,
  weight INT UNSIGNED NOT NULL,
  created DOUBLE NOT NULL,
  PRIMARY KEY (spot_id, word_id),
  KEY (created)
) DEFAULT CHARSET=BINARY;

CREATE TABLE spot_page (
  spot_id BIGINT UNSIGNED NOT NULL,
  page_id BIGINT UNSIGNED NOT NULL,
  weight SMALLINT NOT NULL,
  created DOUBLE NOT NULL,
  updated DOUBLE NOT NULL,
  PRIMARY KEY (spot_id, page_id),
  KEY (created),
  KEY (updated)
) DEFAULT CHARSET=BINARY;

CREATE TABLE spot_link (
  parent_spot_id BIGINT UNSIGNED NOT NULL,
  child_spot_id BIGINT UNSIGNED NOT NULL,
  weight INT UNSIGNED NOT NULL DEFAULT 0,
  created DOUBLE NOT NULL,
  PRIMARY KEY (parent_spot_id, child_spot_id),
   KEY (created)
) DEFAULT CHARSET=BINARY;

CREATE TABLE spot_go_link (
  spot_id BIGINT UNSIGNED NOT NULL,
  go_type VARBINARY(31) NOT NULL,
  go_id BIGINT UNSIGNED NOT NULL,
  created DOUBLE,
  PRIMARY KEY (spot_id, go_type),
  UNIQUE KEY (go_type, go_id),
  KEY (created)
) DEFAULT CHARSET=BINARY Engine=InnoDB;