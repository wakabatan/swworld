all: data-files

clean: clean-data-files

WGET = wget
CURL = curl
GIT = git
PMBP_OPTIONS =

updatenightly: local/bin/pmbp.pl
	$(CURL) https://gist.githubusercontent.com/motemen/667573/raw/git-submodule-track | sh
	$(GIT) add modules
	perl local/bin/pmbp.pl --update
	$(GIT) add config

## ------ Setup ------

deps: git-submodules pmbp-install data-files

git-submodules:
	$(GIT) submodule update --init

local/bin/pmbp.pl:
	mkdir -p local/bin
	$(WGET) -O $@ https://raw.github.com/wakaba/perl-setupenv/master/bin/pmbp.pl
pmbp-upgrade: local/bin/pmbp.pl
	perl local/bin/pmbp.pl $(PMBP_OPTIONS) --update-pmbp-pl
pmbp-update: pmbp-upgrade
	perl local/bin/pmbp.pl $(PMBP_OPTIONS) --update
pmbp-install: pmbp-upgrade
	perl local/bin/pmbp.pl $(PMBP_OPTIONS) --install \
	    --install-module Text::MeCab \
            --create-perl-command-shortcut perl \
            --create-perl-command-shortcut prove \
            --create-perl-command-shortcut plackup

## ------ Data ------

data-files: local/data local/data/countries.json local/data/macroregions.json \
    local/data/jp-regions-full-flatten.json
clean-data-files:
	rm -fr local/data/*.json

local/data:
	mkdir -p local/data

local/data/countries.json:
	$(WGET) -O $@ https://geocol.github.io/data/geocol/data-countries/countries.json
local/data/macroregions.json:
	$(WGET) -O $@ https://geocol.github.io/data/geocol/data-countries/macroregions.json
local/data/jp-regions-full-flatten.json:
	$(WGET) -O $@ https://geocol.github.io/data/geocol/data-jp-areas/jp-regions-full-flatten.json

## ------ Tests ------

PROVE = ./prove

test: test-deps test-main

test-deps: deps

test-main:
	$(PROVE) t/local-http/*.t

## License: Public Domain.
